--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: londiste; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA londiste;


ALTER SCHEMA londiste OWNER TO postgres;

--
-- Name: pgq; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA pgq;


ALTER SCHEMA pgq OWNER TO postgres;

--
-- Name: pgq_ext; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA pgq_ext;


ALTER SCHEMA pgq_ext OWNER TO postgres;

--
-- Name: pgq_node; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA pgq_node;


ALTER SCHEMA pgq_node OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: avito_status; Type: TYPE; Schema: public; Owner: avito
--

CREATE TYPE avito_status AS ENUM (
    '1',
    '2',
    '3',
    '4th',
    '5th'
);


ALTER TYPE avito_status OWNER TO avito;

SET search_path = londiste, pg_catalog;

--
-- Name: _coordinate_copy(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION _coordinate_copy(i_queue_name text, i_table_name text, OUT copy_role text, OUT copy_pos integer) RETURNS record
    LANGUAGE plpgsql STABLE STRICT
    AS $$
-- if the table is in middle of copy from multiple partitions,
-- the copy processes need coordination.
declare
    q_part1     text;
    q_part_ddl  text;
    n_parts     int4;
    n_done      int4;
    _table_name text;
    n_combined_queue text;
    merge_state text;
    dest_table  text;
    dropped_ddl text;
begin
    copy_pos := 0;
    copy_role := null;

    select t.merge_state, t.dest_table, t.dropped_ddl,
           min(case when t2.local then t2.queue_name else null end) as _queue1,
           min(case when t2.local and t2.dropped_ddl is not null then t2.queue_name else null end) as _queue1ddl,
           count(case when t2.local then t2.table_name else null end) as _total,
           count(case when t2.local then nullif(t2.merge_state, 'in-copy') else null end) as _done,
           min(n.combined_queue) as _combined_queue,
           count(nullif(t2.queue_name < i_queue_name and t.merge_state = 'in-copy' and t2.merge_state = 'in-copy', false)) as _copy_pos
        from londiste.table_info t
        join pgq_node.node_info n on (n.queue_name = t.queue_name)
        left join pgq_node.node_info n2 on (n2.combined_queue = n.combined_queue or
            (n2.combined_queue is null and n.combined_queue is null))
        left join londiste.table_info t2 on
           (coalesce(t2.dest_table, t2.table_name) = coalesce(t.dest_table, t.table_name) and
            t2.queue_name = n2.queue_name and
            (t2.merge_state is null or t2.merge_state != 'ok'))
        where t.queue_name = i_queue_name and t.table_name = i_table_name
        group by t.nr, t.table_name, t.local, t.merge_state, t.custom_snapshot, t.table_attrs, t.dropped_ddl, t.dest_table
        into merge_state, dest_table, dropped_ddl, q_part1, q_part_ddl, n_parts, n_done, n_combined_queue, copy_pos;

    -- q_part1, q_part_ddl, n_parts, n_done, n_combined_queue, copy_pos, dest_table

    -- be more robust against late joiners
    q_part1 := coalesce(q_part_ddl, q_part1);

    -- turn the logic off if no merge is happening
    if n_parts = 1 then
        q_part1 := null;
    end if;

    if q_part1 is not null then
        if i_queue_name = q_part1 then
            -- lead
            if merge_state = 'in-copy' then
                if dropped_ddl is null and n_done > 0 then
                    -- seems late addition, let it copy with indexes
                    copy_role := 'wait-replay';
                elsif n_done < n_parts then
                    -- show copy_role only if need to drop ddl or already did drop ddl
                    copy_role := 'lead';
                end if;

                -- make sure it cannot be made to wait
                copy_pos := 0;
            end if;
            if merge_state = 'catching-up' and dropped_ddl is not null then
                -- show copy_role only if need to wait for others
                if n_done < n_parts then
                    copy_role := 'wait-replay';
                end if;
            end if;
        else
            -- follow
            if merge_state = 'in-copy' then
                if q_part_ddl is not null then
                    -- can copy, wait in replay until lead has applied ddl
                    copy_role := 'wait-replay';
                elsif n_done > 0 then
                    -- ddl is not dropped, others are active, copy without touching ddl
                    copy_role := 'wait-replay';
                else
                    -- wait for lead to drop ddl
                    copy_role := 'wait-copy';
                end if;
            elsif merge_state = 'catching-up' then
                -- show copy_role only if need to wait for lead
                if q_part_ddl is not null then
                    copy_role := 'wait-replay';
                end if;
            end if;
        end if;
    end if;

    return;
end;
$$;


ALTER FUNCTION londiste._coordinate_copy(i_queue_name text, i_table_name text, OUT copy_role text, OUT copy_pos integer) OWNER TO postgres;

--
-- Name: create_partition(text, text, text, text, timestamp with time zone, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION create_partition(i_table text, i_part text, i_pkeys text, i_part_field text, i_part_time timestamp with time zone, i_part_period text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
------------------------------------------------------------------------
-- Function: londiste.create_partition
--
--      Creates inherited child table if it does not exist by copying parent table's structure.
--      Locks londiste.table_info table to avoid parallel creation of any partitions.
--
-- Elements that are copied over by "LIKE x INCLUDING ALL":
--      * Defaults
--      * Constraints
--      * Indexes
--      * Storage options (9.0+)
--      * Comments (9.0+)
--
-- Elements that are copied over manually because LIKE ALL does not support them:
--      * Grants
--      * Triggers
--      * Rules
--
-- Parameters:
--      i_table - name of parent table
--      i_part - name of partition table to create
--      i_pkeys - primary key fields (comma separated, used to create constraint).
--      i_part_field - field used to partition table (when not partitioned by field, value is NULL)
--      i_part_time - partition time
--      i_part_period -  period of partitioned data, current possible values are 'hour', 'day', 'month' and 'year'
--
-- Example:
--      select londiste.create_partition('aggregate.user_call_monthly', 'aggregate.user_call_monthly_2010_01', 'key_user', 'period_start', '2010-01-10 11:00'::timestamptz, 'month');
--
------------------------------------------------------------------------
declare
    chk_start       text;
    chk_end         text;
    part_start      timestamptz;
    part_end        timestamptz;
    parent_schema   text;
    parent_name     text;
    parent_oid      oid;
    part_schema     text;
    part_name       text;
    owner           name;
    pos             int4;
    fq_table        text;
    fq_part         text;
    q_grantee       text;
    g               record;
    r               record;
    tg              record;
    sql             text;
    pgver           integer;
    r_oldtbl        text;
    r_extra         text;
    r_sql           text;
begin
    if i_table is null or i_part is null then
        raise exception 'need table and part';
    end if;

    -- load postgres version (XYYZZ).
    show server_version_num into pgver;

    -- parent table schema and name + quoted name
    pos := position('.' in i_table);
    if pos > 0 then
        parent_schema := substring(i_table for pos - 1);
        parent_name := substring(i_table from pos + 1);
    else
        parent_schema := 'public';
        parent_name := i_table;
    end if;
    fq_table := quote_ident(parent_schema) || '.' || quote_ident(parent_name);

    -- part table schema and name + quoted name
    pos := position('.' in i_part);
    if pos > 0 then
        part_schema := substring(i_part for pos - 1);
        part_name := substring(i_part from pos + 1);
    else
        part_schema := 'public';
        part_name := i_part;
    end if;
    fq_part := quote_ident(part_schema) || '.' || quote_ident(part_name);

    -- allow only single creation at a time, without affecting DML operations
    -- (changed from locking parent table to avoid deadlocks from concurrent workers)
    execute 'lock table londiste.table_info in share update exclusive mode';
    parent_oid := fq_table::regclass::oid;

    -- check if part table exists
    perform 1 from pg_class t, pg_namespace s
        where t.relnamespace = s.oid
          and s.nspname = part_schema
          and t.relname = part_name;
    if found then
        return 0;
    end if;

    -- need to use 'like' to get indexes
    sql := 'create table ' || fq_part || ' (like ' || fq_table;
    if pgver >= 90000 then
        sql := sql || ' including all';
    else
        sql := sql || ' including indexes including constraints including defaults';
    end if;
    sql := sql || ') inherits (' || fq_table || ')';
    execute sql;

    -- find out parent table owner
    select o.rolname into owner
      from pg_class t, pg_namespace s, pg_roles o
     where t.relnamespace = s.oid
       and s.nspname = parent_schema
       and t.relname = parent_name
       and t.relowner = o.oid;

    -- set proper part table ownership
    if owner != user then
        sql = 'alter table ' || fq_part || ' owner to ' || quote_ident(owner);
        execute sql;
    end if;

    -- extra check constraint
    if i_part_field != '' then
        part_start := date_trunc(i_part_period, i_part_time);
        part_end := part_start + ('1 ' || i_part_period)::interval;
        chk_start := quote_literal(to_char(part_start, 'YYYY-MM-DD HH24:MI:SS'));
        chk_end := quote_literal(to_char(part_end, 'YYYY-MM-DD HH24:MI:SS'));
        sql := 'alter table '|| fq_part || ' add check ('
            || quote_ident(i_part_field) || ' >= ' || chk_start || ' and '
            || quote_ident(i_part_field) || ' < ' || chk_end || ')';
        execute sql;
    end if;

    -- load grants from parent table
    for g in
        select grantor, grantee, privilege_type, is_grantable
            from information_schema.table_privileges
            where table_schema = parent_schema
                and table_name = parent_name
    loop
        if g.grantee = 'PUBLIC' then
            q_grantee = 'public';
        else
            q_grantee := quote_ident(g.grantee);
        end if;
        sql := 'grant ' || g.privilege_type || ' on ' || fq_part || ' to ' || q_grantee;
        if g.is_grantable = 'YES' then
            sql := sql || ' with grant option';
        end if;
        execute sql;
    end loop;

    -- generate triggers info query
    sql := 'SELECT tgname, tgenabled,'
        || '   pg_catalog.pg_get_triggerdef(oid) as tgdef'
        || ' FROM pg_catalog.pg_trigger '
        || ' WHERE tgrelid = ' || parent_oid::text
        || ' AND ';
    if pgver >= 90000 then
        sql := sql || ' NOT tgisinternal';
    else
        sql := sql || ' NOT tgisconstraint';
    end if;

    -- copy triggers
    for tg in execute sql
    loop
        sql := regexp_replace(tg.tgdef, E' ON ([[:alnum:]_.]+|"([^"]|"")+")+ ', ' ON ' || fq_part || ' ');
        if sql = tg.tgdef then
            raise exception 'Failed to reconstruct the trigger: %', sql;
        end if;
        execute sql;
        if tg.tgenabled = 'O' then
            -- standard mode
            r_extra := NULL;
        elsif tg.tgenabled = 'D' then
            r_extra := ' DISABLE TRIGGER ';
        elsif tg.tgenabled = 'A' then
            r_extra := ' ENABLE ALWAYS TRIGGER ';
        elsif tg.tgenabled = 'R' then
            r_extra := ' ENABLE REPLICA TRIGGER ';
        else
            raise exception 'Unknown trigger mode: %', tg.tgenabled;
        end if;
        if r_extra is not null then
            sql := 'ALTER TABLE ' || fq_part || r_extra || quote_ident(tg.tgname);
            execute sql;
        end if;
    end loop;

    -- copy rules
    for r in
        select rw.rulename, rw.ev_enabled, pg_catalog.pg_get_ruledef(rw.oid) as definition
          from pg_catalog.pg_rewrite rw
         where rw.ev_class = parent_oid
           and rw.rulename <> '_RETURN'::name
    loop
        -- try to skip rule name
        r_extra := 'CREATE RULE ' || quote_ident(r.rulename) || ' AS';
        r_sql := substr(r.definition, 1, char_length(r_extra));
        if r_sql = r_extra then
            r_sql := substr(r.definition, char_length(r_extra));
        else
            raise exception 'failed to match rule name';
        end if;

        -- no clue what name was used in defn, so find it from sql
        r_oldtbl := substring(r_sql from ' TO (([[:alnum:]_.]+|"([^"]+|"")+")+)[[:space:]]');
        if char_length(r_oldtbl) > 0 then
            sql := replace(r.definition, r_oldtbl, fq_part);
        else
            raise exception 'failed to find original table name';
        end if;
        execute sql;

        -- rule flags
        r_extra := NULL;
        if r.ev_enabled = 'R' then
            r_extra = ' ENABLE REPLICA RULE ';
        elsif r.ev_enabled = 'A' then
            r_extra = ' ENABLE ALWAYS RULE ';
        elsif r.ev_enabled = 'D' then
            r_extra = ' DISABLE RULE ';
        elsif r.ev_enabled <> 'O' then
            raise exception 'unknown rule option: %', r.ev_enabled;
        end if;
        if r_extra is not null then
            sql := 'ALTER TABLE ' || fq_part || r_extra
                || quote_ident(r.rulename);
            execute sql;
        end if;
    end loop;

    return 1;
end;
$$;


ALTER FUNCTION londiste.create_partition(i_table text, i_part text, i_pkeys text, i_part_field text, i_part_time timestamp with time zone, i_part_period text) OWNER TO postgres;

--
-- Name: create_trigger(text, text, text[], text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION create_trigger(i_queue_name text, i_table_name text, i_trg_args text[], i_dest_table text, i_node_type text, OUT ret_code integer, OUT ret_note text, OUT trigger_name text) RETURNS record
    LANGUAGE plpgsql
    AS $$
------------------------------------------------------------------------
-- Function: londiste.create_trigger(5)
--
--     Create or replace londiste trigger(s)
--
-- Parameters:
--      i_queue_name - queue name
--      i_table_name - table name
--      i_trg_args   - args to trigger
--      i_dest_table - actual name of destination table (NULL if same as src)
--      i_node_type  - l3 node type
--
-- Trigger args:
--      See documentation for pgq triggers.
--
-- Trigger creation flags (default: AIUDL):
--      I - ON INSERT
--      U - ON UPDATE
--      D - ON DELETE
--      Q - use pgq.sqltriga() as trigger function
--      L - use pgq.logutriga() as trigger function
--      B - BEFORE
--      A - AFTER
--      S - SKIP
--
-- Returns:
--      200 - Ok
--      201 - Trigger not created
--      405 - Multiple SKIP triggers
--
------------------------------------------------------------------------
declare
    trigger_name text;
    lg_func text;
    lg_pos text;
    lg_event text;
    lg_args text[];
    _old_tgargs bytea;
    _new_tgargs bytea;
    trunctrg_name text;
    pgversion int;
    sql text;
    arg text;
    i integer;
    _extra_args text[] := '{}';
    -- skip trigger
    _skip_prefix text := 'zzz_';
    _skip_trg_count integer;
    _skip_trg_name text;
    -- given tgflags array
    _tgflags char[];
    -- ordinary argument array
    _args text[];
    -- array with all valid tgflags values
    _valid_flags char[] := array['B','A','Q','L','I','U','D','S'];
    -- argument flags
    _skip boolean := false;
    _no_triggers boolean := false;
    _got_extra1 boolean := false;
begin
    -- parse trigger args
    if array_lower(i_trg_args, 1) is not null then
        for i in array_lower(i_trg_args, 1) .. array_upper(i_trg_args, 1) loop
            arg := i_trg_args[i];
            if arg like 'tgflags=%' then
                -- special flag handling
                arg := upper(substr(arg, 9));
                for j in array_lower(_valid_flags, 1) .. array_upper(_valid_flags, 1) loop
                    if position(_valid_flags[j] in arg) > 0 then
                        _tgflags := array_append(_tgflags, _valid_flags[j]);
                    end if;
                end loop;
            elsif arg = 'no_triggers' then
                _no_triggers := true;
            elsif lower(arg) = 'skip' then
                _skip := true;
            elsif arg = 'virtual_table' then
                _no_triggers := true;   -- do not create triggers
            elsif arg not in ('expect_sync', 'skip_truncate', 'merge_all', 'no_merge') then -- ignore add-table args
                if arg like 'ev_extra1=%' then
                    _got_extra1 := true;
                end if;
                -- ordinary arg
                _args = array_append(_args, quote_literal(arg));
            end if;
        end loop;
    end if;

    if i_dest_table <> i_table_name and not _got_extra1 then
        -- if renamed table, enforce trigger to put
        -- global table name into extra1
        arg := 'ev_extra1=' || quote_literal(i_table_name);
        _args := array_append(_args, quote_literal(arg));
    end if;

    trigger_name := '_londiste_' || i_queue_name;
    lg_func := 'pgq.logutriga';
    lg_event := '';
    lg_args := array[quote_literal(i_queue_name)];
    lg_pos := 'after';

    if array_lower(_args, 1) is not null then
        lg_args := lg_args || _args;
    end if;

    if 'B' = any(_tgflags) then
        lg_pos := 'before';
    end if;
    if 'A' = any(_tgflags)  then
        lg_pos := 'after';
    end if;
    if 'Q' = any(_tgflags) then
        lg_func := 'pgq.sqltriga';
    end if;
    if 'L' = any(_tgflags) then
        lg_func := 'pgq.logutriga';
    end if;
    if 'I' = any(_tgflags) then
        lg_event := lg_event || ' or insert';
    end if;
    if 'U' = any(_tgflags) then
        lg_event := lg_event || ' or update';
    end if;
    if 'D' = any(_tgflags) then
        lg_event := lg_event || ' or delete';
    end if;
    if 'S' = any(_tgflags) then
        _skip := true;
    end if;

    if i_node_type = 'leaf' then
        -- on weird leafs the trigger funcs may not exist
        perform 1 from pg_proc p join pg_namespace n on (n.oid = p.pronamespace)
            where n.nspname = 'pgq' and p.proname in ('logutriga', 'sqltriga');
        if not found then
            select 201, 'Trigger not created' into ret_code, ret_note;
            return;
        end if;
        -- on regular leaf, install deny trigger
        _extra_args := array_append(_extra_args, quote_literal('deny'));
    end if;

    if _skip or lg_pos = 'after' then
        -- get count and name of existing skip triggers
        select count(*), min(t.tgname)
            into _skip_trg_count, _skip_trg_name
            from pg_catalog.pg_trigger t
            where t.tgrelid = londiste.find_table_oid(i_dest_table)
                and position(E'\\000SKIP\\000'::bytea in tgargs) > 0;
    end if;

    -- make sure new trigger won't be effectively inactive
    if lg_pos = 'after' and _skip_trg_count > 0 then
        select 403, 'AFTER trigger cannot work with SKIP trigger(s)'
        into ret_code, ret_note;
        return;
    end if;

    -- if skip param given, rename previous skip triggers and prefix current
    if _skip then
        -- if no previous skip triggers, prefix name and add SKIP to args
        if _skip_trg_count = 0 then
            trigger_name := _skip_prefix || trigger_name;
            lg_args := array_append(lg_args, quote_literal('SKIP'));
        -- if one previous skip trigger, check it's prefix and
        -- do not use SKIP on current trigger
        elsif _skip_trg_count = 1 then
            -- if not prefixed then rename
            if position(_skip_prefix in _skip_trg_name) != 1 then
                sql := 'alter trigger ' || _skip_trg_name
                    || ' on ' || londiste.quote_fqname(i_dest_table)
                    || ' rename to ' || _skip_prefix || _skip_trg_name;
                execute sql;
            end if;
        else
            select 405, 'Multiple SKIP triggers'
            into ret_code, ret_note;
            return;
        end if;
    end if;

    -- create Ins/Upd/Del trigger if it does not exists already
    select t.tgargs
        from pg_catalog.pg_trigger t
        where t.tgrelid = londiste.find_table_oid(i_dest_table)
            and t.tgname = trigger_name
        into _old_tgargs;

    if found then
        _new_tgargs := decode(lg_args[1], 'escape');
        for i in 2 .. array_upper(lg_args, 1) loop
            _new_tgargs := _new_tgargs || E'\\000'::bytea || decode(lg_args[i], 'escape');
        end loop;

        if _old_tgargs is distinct from _new_tgargs then
            sql := 'drop trigger if exists ' || quote_ident(trigger_name)
                || ' on ' || londiste.quote_fqname(i_dest_table);
            execute sql;
        end if;
    end if;

    if not found or _old_tgargs is distinct from _new_tgargs then
        if _no_triggers then
            select 201, 'Trigger not created'
            into ret_code, ret_note;
            return;
        end if;

        -- finalize event
        lg_event := substr(lg_event, 4); -- remove ' or '
        if lg_event = '' then
            lg_event := 'insert or update or delete';
        end if;

        -- create trigger
        lg_args := lg_args || _extra_args;
        sql := 'create trigger ' || quote_ident(trigger_name)
            || ' ' || lg_pos || ' ' || lg_event
            || ' on ' || londiste.quote_fqname(i_dest_table)
            || ' for each row execute procedure '
            || lg_func || '(' || array_to_string(lg_args, ', ') || ')';
        execute sql;
    end if;

    -- create truncate trigger if it does not exists already
    show server_version_num into pgversion;
    if pgversion >= 80400 then
        trunctrg_name  := '_londiste_' || i_queue_name || '_truncate';
        perform 1 from pg_catalog.pg_trigger
          where tgrelid = londiste.find_table_oid(i_dest_table)
            and tgname = trunctrg_name;
        if not found then
            _extra_args := quote_literal(i_queue_name) || _extra_args;
            sql := 'create trigger ' || quote_ident(trunctrg_name)
                || ' after truncate on ' || londiste.quote_fqname(i_dest_table)
                || ' for each statement execute procedure pgq.sqltriga('
                || array_to_string(_extra_args, ', ') || ')';
            execute sql;
        end if;
    end if;

    select 200, 'OK'
    into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.create_trigger(i_queue_name text, i_table_name text, i_trg_args text[], i_dest_table text, i_node_type text, OUT ret_code integer, OUT ret_note text, OUT trigger_name text) OWNER TO postgres;

--
-- Name: drop_obsolete_partitions(text, interval, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION drop_obsolete_partitions(i_parent_table text, i_retention_period interval, i_partition_period text) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
-------------------------------------------------------------------------------
--  Function: londiste.drop_obsolete_partitions(3)
--
--    Drop obsolete partitions of partition-by-date parent table.
--
--  Parameters:
--    i_parent_table        Master table from which partitions are inherited
--    i_retention_period    How long to keep partitions around
--    i_partition_period    One of: year, month, day, hour
--
--  Returns:
--    Names of partitions dropped
-------------------------------------------------------------------------------
declare
    _part text;
begin
    for _part in
        select londiste.list_obsolete_partitions (i_parent_table, i_retention_period, i_partition_period)
    loop
        execute 'drop table '|| _part;
        return next _part;
    end loop;
end;
$$;


ALTER FUNCTION londiste.drop_obsolete_partitions(i_parent_table text, i_retention_period interval, i_partition_period text) OWNER TO postgres;

--
-- Name: drop_table_fkey(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION drop_table_fkey(i_from_table text, i_fkey_name text) RETURNS integer
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.drop_table_fkey(2)
--
--      Drop one fkey, save in pending table.
-- ----------------------------------------------------------------------
declare
    fkey       record;
begin        
    select * into fkey
    from londiste.find_table_fkeys(i_from_table) 
    where fkey_name = i_fkey_name and from_table = i_from_table;
    
    if not found then
        return 0;
    end if;
            
    insert into londiste.pending_fkeys values (fkey.from_table, fkey.to_table, i_fkey_name, fkey.fkey_def);
        
    execute 'alter table only ' || londiste.quote_fqname(fkey.from_table)
            || ' drop constraint ' || quote_ident(i_fkey_name);
    
    return 1;
end;
$$;


ALTER FUNCTION londiste.drop_table_fkey(i_from_table text, i_fkey_name text) OWNER TO postgres;

--
-- Name: drop_table_triggers(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION drop_table_triggers(i_queue_name text, i_table_name text) RETURNS void
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.drop_table_triggers(2)
--
--      Remove Londiste triggers from table.
--
-- Parameters:
--      i_queue_name      - set name
--      i_table_name      - table name
--
-- Returns:
--      200 - OK
--      404 - Table not found
-- ----------------------------------------------------------------------
declare
    logtrg_name     text;
    b_queue_name    bytea;
    _dest_table     text;
begin
    select coalesce(dest_table, table_name)
        from londiste.table_info t
        where t.queue_name = i_queue_name
          and t.table_name = i_table_name
        into _dest_table;
    if not found then
        return;
    end if;

    -- skip if no triggers found on that table
    perform 1 from pg_catalog.pg_trigger where tgrelid = londiste.find_table_oid(_dest_table);
    if not found then
        return;
    end if;

    -- cast to bytea
    b_queue_name := decode(replace(i_queue_name, E'\\', E'\\\\'), 'escape');

    -- drop all replication triggers that target our queue.
    -- by checking trigger func and queue name there is not
    -- dependency on naming standard or side-storage.
    for logtrg_name in
        select tgname from pg_catalog.pg_trigger
         where tgrelid = londiste.find_table_oid(_dest_table)
           and londiste.is_replica_func(tgfoid)
           and octet_length(tgargs) > 0
           and substring(tgargs for (position(E'\\000'::bytea in tgargs) - 1)) = b_queue_name
    loop
        execute 'drop trigger ' || quote_ident(logtrg_name)
                || ' on ' || londiste.quote_fqname(_dest_table);
    end loop;
end;
$$;


ALTER FUNCTION londiste.drop_table_triggers(i_queue_name text, i_table_name text) OWNER TO postgres;

--
-- Name: execute_finish(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION execute_finish(i_queue_name text, i_file_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.execute_finish(2)
--
--      Finish execution of DDL.  Should be called at the
--      end of the transaction that does the SQL execution.
--
-- Called-by:
--      Londiste setup tool on root, replay on branches/leafs.
--
-- Returns:
--      200 - Proceed.
--      404 - Current entry not found, execute_start() was not called?
-- ----------------------------------------------------------------------
declare
    is_root boolean;
    sql text;
    attrs text;
begin
    is_root := pgq_node.is_root_node(i_queue_name);

    select execute_sql, execute_attrs
        into sql, attrs
        from londiste.applied_execute
        where execute_file = i_file_name;
    if not found then
        select 404, 'execute_file called without execute_start'
            into ret_code, ret_note;
        return;
    end if;

    if is_root then
        perform pgq.insert_event(i_queue_name, 'EXECUTE', sql, i_file_name, attrs, null, null);
    end if;

    select 200, 'Execute finished: ' || i_file_name into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.execute_finish(i_queue_name text, i_file_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: execute_start(text, text, text, boolean); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION execute_start(i_queue_name text, i_file_name text, i_sql text, i_expect_root boolean, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.execute_start(4)
--
--      Start execution of DDL.  Should be called at the
--      start of the transaction that does the SQL execution.
--
-- Called-by:
--      Londiste setup tool on root, replay on branches/leafs.
--
-- Parameters:
--      i_queue_name    - cascaded queue name
--      i_file_name     - Unique ID for SQL
--      i_sql           - Actual script (informative, not used here)
--      i_expect_root   - Is this on root?  Setup tool sets this to avoid
--                        execution on branches.
--
-- Returns:
--      200 - Proceed.
--      301 - Already applied
--      401 - Not root.
--      404 - No such queue
-- ----------------------------------------------------------------------
begin
    select f.ret_code, f.ret_note
      from londiste.execute_start(i_queue_name, i_file_name, i_sql, i_expect_root, null) f
      into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.execute_start(i_queue_name text, i_file_name text, i_sql text, i_expect_root boolean, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: execute_start(text, text, text, boolean, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION execute_start(i_queue_name text, i_file_name text, i_sql text, i_expect_root boolean, i_attrs text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.execute_start(5)
--
--      Start execution of DDL.  Should be called at the
--      start of the transaction that does the SQL execution.
--
-- Called-by:
--      Londiste setup tool on root, replay on branches/leafs.
--
-- Parameters:
--      i_queue_name    - cascaded queue name
--      i_file_name     - Unique ID for SQL
--      i_sql           - Actual script (informative, not used here)
--      i_expect_root   - Is this on root?  Setup tool sets this to avoid
--                        execution on branches.
--      i_attrs         - urlencoded dict of extra attributes.
--                        The value will be put into ev_extra2
--                        field of outgoing event.
--
-- Returns:
--      200 - Proceed.
--      201 - Already applied
--      401 - Not root.
--      404 - No such queue
-- ----------------------------------------------------------------------
declare
    is_root boolean;
begin
    is_root := pgq_node.is_root_node(i_queue_name);
    if i_expect_root then
        if not is_root then
            select 401, 'Node is not root node: ' || i_queue_name
                into ret_code, ret_note;
            return;
        end if;
    end if;

    perform 1 from londiste.applied_execute
        where execute_file = i_file_name;
    if found then
        select 201, 'EXECUTE: "' || i_file_name || '" already applied, skipping'
            into ret_code, ret_note;
        return;
    end if;

    -- this also lock against potetial parallel execute
    insert into londiste.applied_execute (queue_name, execute_file, execute_sql, execute_attrs)
        values (i_queue_name, i_file_name, i_sql, i_attrs);

    select 200, 'Executing: ' || i_file_name into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.execute_start(i_queue_name text, i_file_name text, i_sql text, i_expect_root boolean, i_attrs text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: find_column_types(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION find_column_types(tbl text) RETURNS text
    LANGUAGE plpgsql STABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.find_column_types(1)
--
--      Returns columnt type string for logtriga().
--
-- Parameters:
--      tbl - fqname
--
-- Returns:
--      String of 'kv'.
-- ----------------------------------------------------------------------
declare
    res      text;
    col      record;
    tbl_oid  oid;
begin
    tbl_oid := londiste.find_table_oid(tbl);
    res := '';
    for col in 
        SELECT CASE WHEN k.attname IS NOT NULL THEN 'k' ELSE 'v' END AS type
            FROM pg_attribute a LEFT JOIN (
                SELECT k.attname FROM pg_index i, pg_attribute k
                 WHERE i.indrelid = tbl_oid AND k.attrelid = i.indexrelid
                   AND i.indisprimary AND k.attnum > 0 AND NOT k.attisdropped
                ) k ON (k.attname = a.attname)
            WHERE a.attrelid = tbl_oid AND a.attnum > 0 AND NOT a.attisdropped
            ORDER BY a.attnum
    loop
        res := res || col.type;
    end loop;

    return res;
end;
$$;


ALTER FUNCTION londiste.find_column_types(tbl text) OWNER TO postgres;

--
-- Name: find_rel_oid(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION find_rel_oid(i_fqname text, i_kind text) RETURNS oid
    LANGUAGE plpgsql STABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.find_rel_oid(2)
--
--      Find pg_class row oid.
--
-- Parameters:
--      i_fqname    - fq object name
--      i_kind      - relkind value
--
-- Returns:
--      oid or exception of not found
-- ----------------------------------------------------------------------
declare
    res      oid;
    pos      integer;
    schema   text;
    name     text;
begin
    pos := position('.' in i_fqname);
    if pos > 0 then
        schema := substring(i_fqname for pos - 1);
        name := substring(i_fqname from pos + 1);
    else
        schema := 'public';
        name := i_fqname;
    end if;
    select c.oid into res
      from pg_namespace n, pg_class c
     where c.relnamespace = n.oid
       and c.relkind = i_kind
       and n.nspname = schema and c.relname = name;
    if not found then
        res := NULL;
    end if;

    return res;
end;
$$;


ALTER FUNCTION londiste.find_rel_oid(i_fqname text, i_kind text) OWNER TO postgres;

--
-- Name: find_seq_oid(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION find_seq_oid(seq text) RETURNS oid
    LANGUAGE plpgsql STABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.find_seq_oid(1)
--
--      Find sequence oid based on fqname.
--
-- Parameters:
--      seq - fqname
--
-- Returns:
--      oid
-- ----------------------------------------------------------------------
begin
    return londiste.find_rel_oid(seq, 'S');
end;
$$;


ALTER FUNCTION londiste.find_seq_oid(seq text) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: pending_fkeys; Type: TABLE; Schema: londiste; Owner: postgres; Tablespace: 
--

CREATE TABLE pending_fkeys (
    from_table text NOT NULL,
    to_table text NOT NULL,
    fkey_name text NOT NULL,
    fkey_def text NOT NULL
);


ALTER TABLE pending_fkeys OWNER TO postgres;

--
-- Name: find_table_fkeys(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION find_table_fkeys(i_table_name text) RETURNS SETOF pending_fkeys
    LANGUAGE plpgsql STABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.find_table_fkeys(1)
--
--      Return all active fkeys.
--
-- Parameters:
--      i_table_name    - fqname
--
-- Returns:
--      from_table      - fqname
--      to_table        - fqname
--      fkey_name       - name
--      fkey_def        - full def
-- ----------------------------------------------------------------------
declare
    fkey      record;
    tbl_oid   oid;
begin
    select londiste.find_table_oid(i_table_name) into tbl_oid;
        
    for fkey in
        select n1.nspname || '.' || t1.relname as from_table, n2.nspname || '.' || t2.relname as to_table,
            conname::text as fkey_name, 
            'alter table only ' || quote_ident(n1.nspname) || '.' || quote_ident(t1.relname)
            || ' add constraint ' || quote_ident(conname::text) || ' ' || pg_get_constraintdef(c.oid)
            as fkey_def
        from pg_constraint c, pg_namespace n1, pg_class t1, pg_namespace n2, pg_class t2
        where c.contype = 'f' and (c.conrelid = tbl_oid or c.confrelid = tbl_oid)
            and t1.oid = c.conrelid and n1.oid = t1.relnamespace
            and t2.oid = c.confrelid and n2.oid = t2.relnamespace
        order by 1,2,3
    loop
        return next fkey;
    end loop;
    
    return;
end;
$$;


ALTER FUNCTION londiste.find_table_fkeys(i_table_name text) OWNER TO postgres;

--
-- Name: find_table_oid(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION find_table_oid(tbl text) RETURNS oid
    LANGUAGE plpgsql STABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.find_table_oid(1)
--
--      Find table oid based on fqname.
--
-- Parameters:
--      tbl - fqname
--
-- Returns:
--      oid
-- ----------------------------------------------------------------------
begin
    return londiste.find_rel_oid(tbl, 'r');
end;
$$;


ALTER FUNCTION londiste.find_table_oid(tbl text) OWNER TO postgres;

--
-- Name: get_seq_list(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION get_seq_list(i_queue_name text, OUT seq_name text, OUT last_value bigint, OUT local boolean) RETURNS SETOF record
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.get_seq_list(1)
--
--      Returns registered seqs on this Londiste node.
--
-- Result fiels:
--      seq_name    - fully qualified name of sequence
--      last_value  - last globally published value
--      local       - is locally registered
-- ----------------------------------------------------------------------
declare
    rec record;
begin
    for seq_name, last_value, local in
        select s.seq_name, s.last_value, s.local from londiste.seq_info s
            where s.queue_name = i_queue_name
            order by s.nr, s.seq_name
    loop
        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION londiste.get_seq_list(i_queue_name text, OUT seq_name text, OUT last_value bigint, OUT local boolean) OWNER TO postgres;

--
-- Name: get_table_list(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION get_table_list(i_queue_name text, OUT table_name text, OUT local boolean, OUT merge_state text, OUT custom_snapshot text, OUT table_attrs text, OUT dropped_ddl text, OUT copy_role text, OUT copy_pos integer, OUT dest_table text) RETURNS SETOF record
    LANGUAGE plpgsql STABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.get_table_list(1)
--
--      Return info about registered tables.
--
-- Parameters:
--      i_queue_name - cascaded queue name
--
-- Returns:
--      table_name      - fully-quelified table name
--      local           - does events needs to be applied to local table
--      merge_state     - show phase of initial copy
--      custom_snapshot - remote snapshot of COPY transaction
--      table_attrs     - urlencoded dict of table attributes
--      dropped_ddl     - partition combining: temp place to put DDL
--      copy_role       - partition combining: how to handle copy
--      copy_pos        - position in parallel copy working order
--
-- copy_role = lead:
--      on copy start, drop indexes and store in dropped_ddl
--      on copy finish change state to catching-up, then wait until copy_role turns to NULL
--      catching-up: if dropped_ddl not NULL, restore them
-- copy_role = wait-copy:
--      on copy start wait, until role changes (to wait-replay)
-- copy_role = wait-replay:
--      on copy finish, tag as 'catching-up'
--      wait until copy_role is NULL, then proceed
-- ----------------------------------------------------------------------
begin
    for table_name, local, merge_state, custom_snapshot, table_attrs,
        dropped_ddl, dest_table
    in
        select t.table_name, t.local, t.merge_state, t.custom_snapshot, t.table_attrs,
               t.dropped_ddl, t.dest_table
            from londiste.table_info t
            where t.queue_name = i_queue_name
            order by t.nr, t.table_name
    loop
        copy_role := null;
        copy_pos := 0;

        if merge_state in ('in-copy', 'catching-up') then
            select f.copy_role, f.copy_pos
                from londiste._coordinate_copy(i_queue_name, table_name) f
                into copy_role, copy_pos;
        end if;

        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION londiste.get_table_list(i_queue_name text, OUT table_name text, OUT local boolean, OUT merge_state text, OUT custom_snapshot text, OUT table_attrs text, OUT dropped_ddl text, OUT copy_role text, OUT copy_pos integer, OUT dest_table text) OWNER TO postgres;

--
-- Name: get_table_pending_fkeys(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION get_table_pending_fkeys(i_table_name text) RETURNS SETOF pending_fkeys
    LANGUAGE plpgsql STABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.get_table_pending_fkeys(1)
--
--      Return dropped fkeys for table.
--
-- Parameters:
--      i_table_name - fqname
--
-- Returns:
--      desc
-- ----------------------------------------------------------------------
declare
    fkeys   record;
begin
    for fkeys in
        select *
        from londiste.pending_fkeys
        where from_table = i_table_name or to_table = i_table_name
        order by 1,2,3
    loop
        return next fkeys;
    end loop;
    return;
end;
$$;


ALTER FUNCTION londiste.get_table_pending_fkeys(i_table_name text) OWNER TO postgres;

--
-- Name: get_valid_pending_fkeys(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION get_valid_pending_fkeys(i_queue_name text) RETURNS SETOF pending_fkeys
    LANGUAGE plpgsql STABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.get_valid_pending_fkeys(1)
--
--      Returns dropped fkeys where both sides are in sync now.
--
-- Parameters:
--      i_queue_name - cascaded queue name
--
-- Returns:
--      desc
-- ----------------------------------------------------------------------
declare
    fkeys   record;
begin
    for fkeys in
        select pf.*
        from londiste.pending_fkeys pf
        order by 1, 2, 3
    loop
        perform 1
           from londiste.table_info st_from
          where coalesce(st_from.dest_table, st_from.table_name) = fkeys.from_table
            and st_from.merge_state = 'ok'
            and st_from.custom_snapshot is null
            and st_from.queue_name = i_queue_name;
        if not found then
            continue;
        end if;
        perform 1
           from londiste.table_info st_to
          where coalesce(st_to.dest_table, st_to.table_name) = fkeys.to_table
            and st_to.merge_state = 'ok'
            and st_to.custom_snapshot is null
            and st_to.queue_name = i_queue_name;
        if not found then
            continue;
        end if;
        return next fkeys;
    end loop;
    
    return;
end;
$$;


ALTER FUNCTION londiste.get_valid_pending_fkeys(i_queue_name text) OWNER TO postgres;

--
-- Name: global_add_table(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION global_add_table(i_queue_name text, i_table_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.global_add_table(2)
--
--      Register table on Londiste set.
--
--      This means its available from root, events for it appear
--      in queue and nodes can attach to it.
--
-- Called by:
--      on root - londiste.local_add_table()
--      elsewhere - londiste consumer when receives new table event
--
-- Returns:
--      200 - Ok
--      400 - No such set
-- ----------------------------------------------------------------------
declare
    fq_table_name text;
    _cqueue text;
begin
    fq_table_name := londiste.make_fqname(i_table_name);

    select combined_queue into _cqueue
        from pgq_node.node_info
        where queue_name = i_queue_name
        for update;
    if not found then
        select 400, 'No such queue: ' || i_queue_name into ret_code, ret_note;
        return;
    end if;

    perform 1 from londiste.table_info where queue_name = i_queue_name and table_name = fq_table_name;
    if found then
        select 200, 'Table already added: ' || fq_table_name into ret_code, ret_note;
        return;
    end if;

    insert into londiste.table_info (queue_name, table_name)
        values (i_queue_name, fq_table_name);
    select 200, 'Table added: ' || i_table_name
        into ret_code, ret_note;

    -- let the combined node know about it too
    if _cqueue is not null then
        perform londiste.global_add_table(_cqueue, i_table_name);
    end if;

    return;
exception
    -- seems the row was added from parallel connection (setup vs. replay)
    when unique_violation then
        select 200, 'Table already added: ' || i_table_name
            into ret_code, ret_note;
        return;
end;
$$;


ALTER FUNCTION londiste.global_add_table(i_queue_name text, i_table_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: global_remove_seq(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION global_remove_seq(i_queue_name text, i_seq_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.global_remove_seq(2)
--
--      Removes sequence registration in set.
--
-- Called by:
--      - On root by londiste.local_remove_seq()
--      - Elsewhere by consumer receiving seq remove event
--
-- Returns:
--      200 - OK
--      400 - not found
-- ----------------------------------------------------------------------
declare
    fq_name text;
begin
    fq_name := londiste.make_fqname(i_seq_name);
    delete from londiste.seq_info
        where queue_name = i_queue_name
          and seq_name = fq_name;
    if not found then
        select 400, 'Sequence not found: '||fq_name into ret_code, ret_note;
        return;
    end if;
    if pgq_node.is_root_node(i_queue_name) then
        perform londiste.root_notify_change(i_queue_name, 'londiste.remove-seq', fq_name);
    end if;
    select 200, 'Sequence removed: '||fq_name into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.global_remove_seq(i_queue_name text, i_seq_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: global_remove_table(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION global_remove_table(i_queue_name text, i_table_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.global_remove_table(2)
--
--      Removes tables registration in set.
--
--      Means that nodes cannot attach to this table anymore.
--
-- Called by:
--      - On root by londiste.local_remove_table()
--      - Elsewhere by consumer receiving table remove event
--
-- Returns:
--      200 - OK
--      400 - not found
-- ----------------------------------------------------------------------
declare
    fq_table_name text;
begin
    fq_table_name := londiste.make_fqname(i_table_name);
    if not pgq_node.is_root_node(i_queue_name) then
        perform londiste.local_remove_table(i_queue_name, fq_table_name);
    end if;
    delete from londiste.table_info
        where queue_name = i_queue_name
          and table_name = fq_table_name;
    if not found then
        select 400, 'Table not found: ' || fq_table_name
            into ret_code, ret_note;
        return;
    end if;
    select 200, 'Table removed: ' || i_table_name
        into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.global_remove_table(i_queue_name text, i_table_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: global_update_seq(text, text, bigint); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION global_update_seq(i_queue_name text, i_seq_name text, i_value bigint, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.global_update_seq(3)
--
--      Update seq.
--
-- Parameters:
--      i_queue_name  - set name
--      i_seq_name  - seq name
--      i_value     - new published value
--
-- Returns:
--      200 - OK
-- ----------------------------------------------------------------------
declare
    n record;
    fqname text;
    seq record;
begin
    select node_type, node_name into n
        from pgq_node.node_info
        where queue_name = i_queue_name;
    if not found then
        select 404, 'Set not found: ' || i_queue_name into ret_code, ret_note;
        return;
    end if;
    if n.node_type = 'root' then
        select 402, 'Must not run on root node' into ret_code, ret_note;
        return;
    end if;

    fqname := londiste.make_fqname(i_seq_name);
    select last_value, local from londiste.seq_info
        into seq
        where queue_name = i_queue_name and seq_name = fqname
        for update;
    if not found then
        insert into londiste.seq_info
            (queue_name, seq_name, last_value)
        values (i_queue_name, fqname, i_value);
    else
        update londiste.seq_info
            set last_value = i_value
            where queue_name = i_queue_name and seq_name = fqname;
        if seq.local then
            perform pgq.seq_setval(fqname, i_value);
        end if;
    end if;
    select 200, 'Sequence updated' into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.global_update_seq(i_queue_name text, i_seq_name text, i_value bigint, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: is_obsolete_partition(text, interval, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION is_obsolete_partition(i_partition_table text, i_retention_period interval, i_partition_period text) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
-------------------------------------------------------------------------------
--  Function: londiste.is_obsolete_partition(3)
--
--    Test partition name of partition-by-date parent table.
--
--  Parameters:
--    i_partition_table     Partition table name we want to check
--    i_retention_period    How long to keep partitions around
--    i_partition_period    One of: year, month, day, hour
--
--  Returns:
--    True if partition is too old, false if it is not,
--    null if its name does not match expected pattern.
-------------------------------------------------------------------------------
declare
    _expr text;
    _dfmt text;
    _base text;
begin
    if i_partition_period in ('year', 'yearly') then
        _expr := '_[0-9]{4}';
        _dfmt := '_YYYY';
    elsif i_partition_period in ('month', 'monthly') then
        _expr := '_[0-9]{4}_[0-9]{2}';
        _dfmt := '_YYYY_MM';
    elsif i_partition_period in ('day', 'daily') then
        _expr := '_[0-9]{4}_[0-9]{2}_[0-9]{2}';
        _dfmt := '_YYYY_MM_DD';
    elsif i_partition_period in ('hour', 'hourly') then
        _expr := '_[0-9]{4}_[0-9]{2}_[0-9]{2}_[0-9]{2}';
        _dfmt := '_YYYY_MM_DD_HH24';
    else
        raise exception 'not supported i_partition_period: %', i_partition_period;
    end if;

    _expr = '^(.+)' || _expr || '$';
    _base = substring (i_partition_table from _expr);

    if _base is null then
        return null;
    elsif i_partition_table < _base || to_char (now() - i_retention_period, _dfmt) then
        return true;
    else
        return false;
    end if;
end;
$_$;


ALTER FUNCTION londiste.is_obsolete_partition(i_partition_table text, i_retention_period interval, i_partition_period text) OWNER TO postgres;

--
-- Name: is_replica_func(oid); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION is_replica_func(func_oid oid) RETURNS boolean
    LANGUAGE sql STABLE STRICT
    AS $_$
-- ----------------------------------------------------------------------
-- Function: londiste.is_replica_func(1)
--
--      Returns true if function is a PgQ-based replication functions.
--      This also means it takes queue name as first argument.
-- ----------------------------------------------------------------------
select count(1) > 0
  from pg_proc f join pg_namespace n on (n.oid = f.pronamespace)
  where f.oid = $1 and n.nspname = 'pgq' and f.proname in ('sqltriga', 'logutriga');
$_$;


ALTER FUNCTION londiste.is_replica_func(func_oid oid) OWNER TO postgres;

--
-- Name: list_obsolete_partitions(text, interval, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION list_obsolete_partitions(i_parent_table text, i_retention_period interval, i_partition_period text) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
-------------------------------------------------------------------------------
--  Function: londiste.list_obsolete_partitions(3)
--
--    List obsolete partitions of partition-by-date parent table.
--
--  Parameters:
--    i_parent_table        Master table from which partitions are inherited
--    i_retention_period    How long to keep partitions around
--    i_partition_period    One of: year, month, day, hour
--
--  Returns:
--    Names of partitions to be dropped
-------------------------------------------------------------------------------
declare
    _schema text not null := split_part (i_parent_table, '.', 1);
    _table  text not null := split_part (i_parent_table, '.', 2);
    _part   text;
    _expr   text;
    _dfmt   text;
begin
    if i_partition_period in ('year', 'yearly') then
        _expr := '_[0-9]{4}';
        _dfmt := '_YYYY';
    elsif i_partition_period in ('month', 'monthly') then
        _expr := '_[0-9]{4}_[0-9]{2}';
        _dfmt := '_YYYY_MM';
    elsif i_partition_period in ('day', 'daily') then
        _expr := '_[0-9]{4}_[0-9]{2}_[0-9]{2}';
        _dfmt := '_YYYY_MM_DD';
    elsif i_partition_period in ('hour', 'hourly') then
        _expr := '_[0-9]{4}_[0-9]{2}_[0-9]{2}_[0-9]{2}';
        _dfmt := '_YYYY_MM_DD_HH24';
    else
        raise exception 'not supported i_partition_period: %', i_partition_period;
    end if;

    if length (_table) = 0 then
        _table := _schema;
        _schema := 'public';
    end if;

    for _part in
        select quote_ident (t.schemaname) ||'.'|| quote_ident (t.tablename)
          from pg_catalog.pg_tables t
         where t.schemaname = _schema
           and t.tablename ~ ('^'|| _table || _expr ||'$')
           and t.tablename < _table || to_char (now() - i_retention_period, _dfmt)
         order by 1
    loop
        return next _part;
    end loop;
end;
$_$;


ALTER FUNCTION londiste.list_obsolete_partitions(i_parent_table text, i_retention_period interval, i_partition_period text) OWNER TO postgres;

--
-- Name: local_add_seq(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_add_seq(i_queue_name text, i_seq_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.local_add_seq(2)
--
--      Register sequence.
--
-- Parameters:
--      i_queue_name    - cascaded queue name
--      i_seq_name      - seq name
--
-- Returns:
--      200 - OK
--      400 - Not found
-- ----------------------------------------------------------------------
declare
    fq_seq_name text;
    lastval int8;
    seq record;
begin
    fq_seq_name := londiste.make_fqname(i_seq_name);

    perform 1 from pg_class
        where oid = londiste.find_seq_oid(fq_seq_name);
    if not found then
        select 400, 'Sequence not found: ' || fq_seq_name into ret_code, ret_note;
        return;
    end if;

    if pgq_node.is_root_node(i_queue_name) then
        select local, last_value into seq
            from londiste.seq_info
            where queue_name = i_queue_name
                and seq_name = fq_seq_name
            for update;
        if found and seq.local then
            select 201, 'Sequence already added: ' || fq_seq_name
                into ret_code, ret_note;
            return;
        end if;
        if not seq.local then
            update londiste.seq_info set local = true
                where queue_name = i_queue_name and seq_name = fq_seq_name;
        else
            insert into londiste.seq_info (queue_name, seq_name, local, last_value)
                values (i_queue_name, fq_seq_name, true, 0);
        end if;
        perform * from londiste.root_check_seqs(i_queue_name);
    else
        select local, last_value into seq
            from londiste.seq_info
            where queue_name = i_queue_name
                and seq_name = fq_seq_name
            for update;
        if not found then
            select 404, 'Unknown sequence: ' || fq_seq_name
                into ret_code, ret_note;
            return;
        end if;
        if seq.local then
            select 201, 'Sequence already added: ' || fq_seq_name
                into ret_code, ret_note;
            return;
        end if;
        update londiste.seq_info set local = true
            where queue_name = i_queue_name and seq_name = fq_seq_name;
        perform pgq.seq_setval(fq_seq_name, seq.last_value);
    end if;

    select 200, 'Sequence added: ' || fq_seq_name into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.local_add_seq(i_queue_name text, i_seq_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: local_add_table(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_add_table(i_queue_name text, i_table_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.local_add_table(2)
--
--      Register table on Londiste node.
-- ----------------------------------------------------------------------
begin
    select f.ret_code, f.ret_note into ret_code, ret_note
      from londiste.local_add_table(i_queue_name, i_table_name, null) f;
    return;
end;
$$;


ALTER FUNCTION londiste.local_add_table(i_queue_name text, i_table_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: local_add_table(text, text, text[]); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_add_table(i_queue_name text, i_table_name text, i_trg_args text[], OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.local_add_table(3)
--
--      Register table on Londiste node.
-- ----------------------------------------------------------------------
begin
    select f.ret_code, f.ret_note into ret_code, ret_note
      from londiste.local_add_table(i_queue_name, i_table_name, i_trg_args, null) f;
    return;
end;
$$;


ALTER FUNCTION londiste.local_add_table(i_queue_name text, i_table_name text, i_trg_args text[], OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: local_add_table(text, text, text[], text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_add_table(i_queue_name text, i_table_name text, i_trg_args text[], i_table_attrs text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.local_add_table(4)
--
--      Register table on Londiste node.
-- ----------------------------------------------------------------------
begin
    select f.ret_code, f.ret_note into ret_code, ret_note
      from londiste.local_add_table(i_queue_name, i_table_name, i_trg_args, i_table_attrs, null) f;
    return;
end;
$$;


ALTER FUNCTION londiste.local_add_table(i_queue_name text, i_table_name text, i_trg_args text[], i_table_attrs text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: local_add_table(text, text, text[], text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_add_table(i_queue_name text, i_table_name text, i_trg_args text[], i_table_attrs text, i_dest_table text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.local_add_table(5)
--
--      Register table on Londiste node, with customizable trigger args.
--
-- Parameters:
--      i_queue_name    - queue name
--      i_table_name    - table name
--      i_trg_args      - args to trigger, or magic parameters.
--      i_table_attrs   - args to python handler
--      i_dest_table    - actual name of destination table (NULL if same)
--
-- Trigger args:
--      See documentation for pgq triggers.
--
-- Magic parameters:
--      no_triggers     - skip trigger creation
--      skip_truncate   - set 'skip_truncate' table attribute
--      expect_sync     - set table state to 'ok'
--      tgflags=X       - trigger creation flags
--      merge_all       - merge table from all sources. required for
--                        multi-source table
--      no_merge        - do not merge tables from different sources
--      skip            - create skip trigger. same as S flag
--      virtual_table   - skips structure check and trigger creation
--
-- Trigger creation flags (default: AIUDL):
--      I - ON INSERT
--      U - ON UPDATE
--      D - ON DELETE
--      Q - use pgq.sqltriga() as trigger function
--      L - use pgq.logutriga() as trigger function
--      B - BEFORE
--      A - AFTER
--      S - SKIP
--
-- Example:
--      > londiste.local_add_table('q', 'tbl', array['tgflags=BI', 'SKIP', 'pkey=col1,col2'])
--
-- Returns:
--      200 - Ok
--      301 - Warning, trigger exists that will fire before londiste one
--      400 - No such set
--      410 - Table already exists but with different table_attrs
------------------------------------------------------------------------
declare
    col_types text;
    fq_table_name text;
    new_state text;
    pgversion int;
    logtrg_previous text;
    trigger_name text;
    tbl record;
    i integer;
    j integer;
    arg text;
    _node record;
    _tbloid oid;
    _combined_queue text;
    _combined_table text;
    _table_attrs text := i_table_attrs;
    -- check local tables from all sources
    _queue_name text;
    _local boolean;
    -- argument flags
    _expect_sync boolean := false;
    _merge_all boolean := false;
    _no_merge boolean := false;
    _virtual_table boolean := false;
    _dest_table text;
    _table_name2 text;
    _desc text;
begin

    -------- i_trg_args ARGUMENTS PARSING (TODO: use different input param for passing extra options that have nothing to do with trigger)

    if array_lower(i_trg_args, 1) is not null then
        for i in array_lower(i_trg_args, 1) .. array_upper(i_trg_args, 1) loop
            arg := i_trg_args[i];
            if arg = 'expect_sync' then
                _expect_sync := true;
            elsif arg = 'skip_truncate' then
                _table_attrs := coalesce(_table_attrs || '&skip_truncate=1', 'skip_truncate=1');
            elsif arg = 'merge_all' then
                _merge_all = true;
            elsif arg = 'no_merge' then
                _no_merge = true;
            elsif arg = 'virtual_table' then
                _virtual_table := true;
                _expect_sync := true;   -- do not copy
            end if;
        end loop;
    end if;

    if _merge_all and _no_merge then
        select 405, 'Cannot use merge-all and no-merge together'
        into ret_code, ret_note;
        return;
    end if;

    fq_table_name := londiste.make_fqname(i_table_name);
    _dest_table := londiste.make_fqname(coalesce(i_dest_table, i_table_name));

    if _dest_table = fq_table_name then
        _desc := fq_table_name;
    else
        _desc := fq_table_name || '(' || _dest_table || ')';
    end if;

    -------- TABLE STRUCTURE CHECK

    if not _virtual_table then
        _tbloid := londiste.find_table_oid(_dest_table);
        if _tbloid is null then
            select 404, 'Table does not exist: ' || _desc into ret_code, ret_note;
            return;
        end if;
        col_types := londiste.find_column_types(_dest_table);
        if position('k' in col_types) < 1 then
            -- allow missing primary key in case of combined table where
            -- pkey was removed by londiste
            perform 1 from londiste.table_info t,
                pgq_node.node_info n_this,
                pgq_node.node_info n_other
              where n_this.queue_name = i_queue_name
                and n_other.combined_queue = n_this.combined_queue
                and n_other.queue_name <> n_this.queue_name
                and t.queue_name = n_other.queue_name
                and coalesce(t.dest_table, t.table_name) = _dest_table
                and t.dropped_ddl is not null;
            if not found then
                select 400, 'Primary key missing on table: ' || _desc into ret_code, ret_note;
                return;
            end if;
        end if;
    end if;

    -------- TABLE REGISTRATION LOGIC

    select * from pgq_node.get_node_info(i_queue_name) into _node;
    if not found or _node.ret_code >= 400 then
        select 400, 'No such set: ' || i_queue_name into ret_code, ret_note;
        return;
    end if;

    select merge_state, local, table_attrs into tbl
        from londiste.table_info
        where queue_name = i_queue_name and table_name = fq_table_name;
    if not found then
        -- add to set on root
        if _node.node_type = 'root' then
            select f.ret_code, f.ret_note into ret_code, ret_note
                from londiste.global_add_table(i_queue_name, i_table_name) f;
            if ret_code <> 200 then
                return;
            end if;
        else
            select 404, 'Table not available on queue: ' || _desc
                into ret_code, ret_note;
            return;
        end if;

        -- reload info
        select merge_state, local, table_attrs into tbl
            from londiste.table_info
            where queue_name = i_queue_name and table_name = fq_table_name;
    end if;

    if tbl.local then
        if tbl.table_attrs is distinct from _table_attrs then
            select 410, 'Table ' || _desc || ' already added, but with different args: ' || coalesce(tbl.table_attrs, '') into ret_code, ret_note;
        else
            select 200, 'Table already added: ' || _desc into ret_code, ret_note;
        end if;
        return;
    end if;

    if _node.node_type = 'root' then
        new_state := 'ok';
        perform londiste.root_notify_change(i_queue_name, 'londiste.add-table', fq_table_name);
    elsif _node.node_type = 'leaf' and _node.combined_type = 'branch' then
        new_state := 'ok';
    elsif _expect_sync then
        new_state := 'ok';
    else
        new_state := NULL;
    end if;

    update londiste.table_info
        set local = true,
            merge_state = new_state,
            table_attrs = coalesce(_table_attrs, table_attrs),
            dest_table = nullif(_dest_table, fq_table_name)
        where queue_name = i_queue_name and table_name = fq_table_name;
    if not found then
        raise exception 'lost table: %', fq_table_name;
    end if;

    -- merge all table sources on leaf
    if _node.node_type = 'leaf' and not _no_merge then
        for _queue_name, _table_name2, _local in
            select t2.queue_name, t2.table_name, t2.local
            from londiste.table_info t
            join pgq_node.node_info n on (n.queue_name = t.queue_name)
            left join pgq_node.node_info n2 on (n2.combined_queue = n.combined_queue or
                    (n2.combined_queue is null and n.combined_queue is null))
            left join londiste.table_info t2
              on (t2.queue_name = n2.queue_name and
                  coalesce(t2.dest_table, t2.table_name) = coalesce(t.dest_table, t.table_name))
            where t.queue_name = i_queue_name
              and t.table_name = fq_table_name
              and t2.queue_name != i_queue_name -- skip self
        loop
            -- if table from some other source is already marked as local,
            -- raise error
            if _local and coalesce(new_state, 'x') <> 'ok' then
                select 405, 'Found local table '|| _desc
                        || ' in queue ' || _queue_name
                        || ', use remove-table first to remove all previous '
                        || 'table subscriptions'
                into ret_code, ret_note;
                return;
            end if;

           -- when table comes from multiple sources, merge_all switch is
           -- required
           if not _merge_all and coalesce(new_state, 'x') <> 'ok' then
               select 405, 'Found multiple sources for table '|| _desc
                       || ', use merge-all or no-merge to continue'
               into ret_code, ret_note;
               return;
           end if;

            update londiste.table_info
               set local = true,
                   merge_state = new_state,
                   table_attrs = coalesce(_table_attrs, table_attrs)
               where queue_name = _queue_name and table_name = _table_name2;
            if not found then
                raise exception 'lost table: % on queue %', _table_name2, _queue_name;
            end if;
        end loop;

        -- if this node has combined_queue, add table there too
        -- note: we need to keep both table_name/dest_table values
        select n2.queue_name, t.table_name
            from pgq_node.node_info n1
            join pgq_node.node_info n2
                on (n2.queue_name = n1.combined_queue)
            left join londiste.table_info t
                on (t.queue_name = n2.queue_name and t.table_name = fq_table_name and t.local)
            where n1.queue_name = i_queue_name and n2.node_type = 'root'
            into _combined_queue, _combined_table;
        if found and _combined_table is null then
            select f.ret_code, f.ret_note
                from londiste.local_add_table(_combined_queue, fq_table_name, i_trg_args, _table_attrs, _dest_table) f
                into ret_code, ret_note;
            if ret_code >= 300 then
                return;
            end if;
        end if;
    end if;

    -- create trigger
    select f.ret_code, f.ret_note, f.trigger_name
        from londiste.create_trigger(i_queue_name, fq_table_name, i_trg_args, _dest_table, _node.node_type) f
        into ret_code, ret_note, trigger_name;

    if ret_code > 299 then
        ret_note := 'Trigger creation failed for table ' || _desc || ': ' || ret_note;
        return;
    elsif ret_code = 201 then
        select 200, 'Table added with no triggers: ' || _desc
            into ret_code, ret_note;
        return;
    end if;

    -- Check that no trigger exists on the target table that will get fired
    -- before londiste one (this could have londiste replicate data out-of-order)
    --
    -- Don't report all the trigger names, 8.3 does not have array_accum available.

    show server_version_num into pgversion;
    if pgversion >= 90000 then
        select tg.tgname into logtrg_previous
        from pg_class r join pg_trigger tg on (tg.tgrelid = r.oid)
        where r.oid = londiste.find_table_oid(_dest_table)
          and not tg.tgisinternal
          and tg.tgname < trigger_name::name
          -- per-row AFTER trigger
          and (tg.tgtype & 3) = 1   -- bits: 0:ROW, 1:BEFORE
          -- current londiste
          and not londiste.is_replica_func(tg.tgfoid)
          -- old londiste
          and substring(tg.tgname from 1 for 10) != '_londiste_'
          and substring(tg.tgname from char_length(tg.tgname) - 6) != '_logger'
        order by 1 limit 1;
    else
        select tg.tgname into logtrg_previous
        from pg_class r join pg_trigger tg on (tg.tgrelid = r.oid)
        where r.oid = londiste.find_table_oid(_dest_table)
          and not tg.tgisconstraint
          and tg.tgname < trigger_name::name
          -- per-row AFTER trigger
          and (tg.tgtype & 3) = 1   -- bits: 0:ROW, 1:BEFORE
          -- current londiste
          and not londiste.is_replica_func(tg.tgfoid)
          -- old londiste
          and substring(tg.tgname from 1 for 10) != '_londiste_'
          and substring(tg.tgname from char_length(tg.tgname) - 6) != '_logger'
        order by 1 limit 1;
    end if;

    if logtrg_previous is not null then
       select 301,
              'Table added: ' || _desc
                              || ', but londiste trigger is not first: '
                              || logtrg_previous
         into ret_code, ret_note;
        return;
    end if;

    select 200, 'Table added: ' || _desc into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.local_add_table(i_queue_name text, i_table_name text, i_trg_args text[], i_table_attrs text, i_dest_table text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: local_change_handler(text, text, text[], text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_change_handler(i_queue_name text, i_table_name text, i_trg_args text[], i_table_attrs text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
----------------------------------------------------------------------------------------------------
-- Function: londiste.local_change_handler(4)
--
--     Change handler and rebuild trigger if needed
--
-- Parameters:
--      i_queue_name  - set name
--      i_table_name  - table name
--      i_trg_args    - args to trigger
--      i_table_attrs - args to python handler
--
-- Returns:
--      200 - OK
--      400 - No such set
--      404 - Table not found
--
----------------------------------------------------------------------------------------------------
declare
    _dest_table text;
    _desc text;
    _node record;
begin
    -- get node info
    select * from pgq_node.get_node_info(i_queue_name) into _node;
    if not found or _node.ret_code >= 400 then
        select 400, 'No such set: ' || i_queue_name into ret_code, ret_note;
        return;
    end if;

    -- update table_attrs with new handler info
    select f.ret_code, f.ret_note
        from londiste.local_set_table_attrs(i_queue_name, i_table_name, i_table_attrs) f
        into ret_code, ret_note;
    if ret_code <> 200 then
        return;
    end if;

    -- get destination table name for use in trigger creation
    select coalesce(ti.dest_table, i_table_name)
        from londiste.table_info ti
        where queue_name = i_queue_name
        and table_name = i_table_name
        and local
        into _dest_table;

    -- replace the trigger if needed
    select f.ret_code, f.ret_note
        from londiste.create_trigger(i_queue_name, i_table_name, i_trg_args, _dest_table, _node.node_type) f
        into ret_code, ret_note;

    if _dest_table = i_table_name then
        _desc := i_table_name;
    else
        _desc := i_table_name || '(' || _dest_table || ')';
    end if;

    if ret_code > 299 then
        ret_note := 'Trigger creation failed for table ' || _desc || ': ' || ret_note;
        return;
    elsif ret_code = 201 then
        select 200, 'Table handler updated with no triggers: ' || _desc
            into ret_code, ret_note;
        return;
    end if;

    select 200, 'Handler changed for table: ' || _desc
        into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.local_change_handler(i_queue_name text, i_table_name text, i_trg_args text[], i_table_attrs text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: local_remove_seq(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_remove_seq(i_queue_name text, i_seq_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.local_remove_seq(2)
--
--      Remove sequence.
--
-- Parameters:
--      i_queue_name      - set name
--      i_seq_name      - sequence name
--
-- Returns:
--      200 - OK
--      404 - Sequence not found
-- ----------------------------------------------------------------------
declare
    fqname text;
begin
    fqname := londiste.make_fqname(i_seq_name);
    if pgq_node.is_root_node(i_queue_name) then
        select f.ret_code, f.ret_note
            into ret_code, ret_note
            from londiste.global_remove_seq(i_queue_name, fqname) f;
        return;
    end if;
    update londiste.seq_info
        set local = false
        where queue_name = i_queue_name
          and seq_name = fqname
          and local;
    if not found then
        select 404, 'Sequence not found: '||fqname into ret_code, ret_note;
        return;
    end if;

    select 200, 'Sequence removed: '||fqname into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.local_remove_seq(i_queue_name text, i_seq_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: local_remove_table(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_remove_table(i_queue_name text, i_table_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.local_remove_table(2)
--
--      Remove table.
--
-- Parameters:
--      i_queue_name      - set name
--      i_table_name      - table name
--
-- Returns:
--      200 - OK
--      404 - Table not found
-- ----------------------------------------------------------------------
declare
    fq_table_name   text;
    qtbl            text;
    seqname         text;
    tbl             record;
    tbl_oid         oid;
    pgver           integer;
begin
    fq_table_name := londiste.make_fqname(i_table_name);
    qtbl := londiste.quote_fqname(fq_table_name);
    tbl_oid := londiste.find_table_oid(i_table_name);
    show server_version_num into pgver;

    select local, dropped_ddl, merge_state into tbl
        from londiste.table_info
        where queue_name = i_queue_name
          and table_name = fq_table_name
        for update;
    if not found then
        select 400, 'Table not found: ' || fq_table_name into ret_code, ret_note;
        return;
    end if;

    if tbl.local then
        perform londiste.drop_table_triggers(i_queue_name, fq_table_name);

        -- restore dropped ddl
        if tbl.dropped_ddl is not null then
            -- table is not synced, drop data to make restore faster
            if pgver >= 80400 then
                execute 'TRUNCATE ONLY ' || qtbl;
            else
                execute 'TRUNCATE ' || qtbl;
            end if;
            execute tbl.dropped_ddl;
        end if;

        -- reset data
        update londiste.table_info
            set local = false,
                custom_snapshot = null,
                table_attrs = null,
                dropped_ddl = null,
                merge_state = null,
                dest_table = null
            where queue_name = i_queue_name
                and table_name = fq_table_name;

        -- drop dependent sequence
        for seqname in
            select n.nspname || '.' || s.relname
                from pg_catalog.pg_class s,
                     pg_catalog.pg_namespace n,
                     pg_catalog.pg_attribute a
                where a.attrelid = tbl_oid
                    and a.atthasdef
                    and a.atttypid::regtype::text in ('integer', 'bigint')
                    and s.oid = pg_get_serial_sequence(qtbl, a.attname)::regclass::oid
                    and n.oid = s.relnamespace
        loop
            perform londiste.local_remove_seq(i_queue_name, seqname);
        end loop;
    else
        if not pgq_node.is_root_node(i_queue_name) then
            select 400, 'Table not registered locally: ' || fq_table_name into ret_code, ret_note;
            return;
        end if;
    end if;

    if pgq_node.is_root_node(i_queue_name) then
        perform londiste.global_remove_table(i_queue_name, fq_table_name);
        perform londiste.root_notify_change(i_queue_name, 'londiste.remove-table', fq_table_name);
    end if;

    select 200, 'Table removed: ' || fq_table_name into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.local_remove_table(i_queue_name text, i_table_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: local_set_table_attrs(text, text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_set_table_attrs(i_queue_name text, i_table_name text, i_table_attrs text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.local_set_table_attrs(3)
--
--      Store urlencoded table attributes.
--
-- Parameters:
--      i_queue_name    - cascaded queue name
--      i_table         - table name
--      i_table_attrs   - urlencoded attributes
-- ----------------------------------------------------------------------
begin
    update londiste.table_info
        set table_attrs = i_table_attrs
      where queue_name = i_queue_name
        and table_name = i_table_name
        and local;
    if found then
        select 200, i_table_name || ': Table attributes stored'
            into ret_code, ret_note;
    else
        select 404, 'no such local table: ' || i_table_name
            into ret_code, ret_note;
    end if;
    return;
end;
$$;


ALTER FUNCTION londiste.local_set_table_attrs(i_queue_name text, i_table_name text, i_table_attrs text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: local_set_table_state(text, text, text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_set_table_state(i_queue_name text, i_table_name text, i_snapshot text, i_merge_state text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.local_set_table_state(4)
--
--      Change table state.
--
-- Parameters:
--      i_queue_name    - cascaded queue name
--      i_table         - table name
--      i_snapshot      - optional remote snapshot info
--      i_merge_state   - merge state
-- ----------------------------------------------------------------------
declare
    _tbl text;
begin
    _tbl = londiste.make_fqname(i_table_name);

    update londiste.table_info
        set custom_snapshot = i_snapshot,
            merge_state = i_merge_state
      where queue_name = i_queue_name
        and table_name = _tbl
        and local;
    if not found then
        select 404, 'No such table: ' || _tbl
            into ret_code, ret_note;
        return;
    end if;

    select 200, 'Table ' || _tbl || ' state set to '
            || coalesce(quote_literal(i_merge_state), 'NULL')
        into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.local_set_table_state(i_queue_name text, i_table_name text, i_snapshot text, i_merge_state text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: local_set_table_struct(text, text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_set_table_struct(i_queue_name text, i_table_name text, i_dropped_ddl text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.local_set_table_struct(3)
--
--      Store dropped table struct temporarily.
--
-- Parameters:
--      i_queue_name    - cascaded queue name
--      i_table         - table name
--      i_dropped_ddl   - merge state
-- ----------------------------------------------------------------------
begin
    update londiste.table_info
        set dropped_ddl = i_dropped_ddl
      where queue_name = i_queue_name
        and table_name = i_table_name
        and local;
    if found then
        select 200, 'Table struct stored'
            into ret_code, ret_note;
    else
        select 404, 'no such local table: '||i_table_name
            into ret_code, ret_note;

    end if;
    return;
end;
$$;


ALTER FUNCTION londiste.local_set_table_struct(i_queue_name text, i_table_name text, i_dropped_ddl text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: local_show_missing(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION local_show_missing(i_queue_name text, OUT obj_kind text, OUT obj_name text) RETURNS SETOF record
    LANGUAGE plpgsql STABLE STRICT
    AS $$ 
-- ----------------------------------------------------------------------
-- Function: londiste.local_show_missing(1)
--
--      Return info about missing tables.  On root show tables
--      not registered on set, on branch/leaf show tables
--      in set but not registered locally.
-- ----------------------------------------------------------------------
begin
    if pgq_node.is_root_node(i_queue_name) then
        for obj_kind, obj_name in
            select r.relkind, n.nspname || '.' || r.relname
                from pg_catalog.pg_class r, pg_catalog.pg_namespace n
                where n.oid = r.relnamespace
                  and r.relkind in ('r', 'S')
                  and n.nspname not in ('pgq', 'pgq_ext', 'pgq_node', 'londiste', 'pg_catalog', 'information_schema')
                  and n.nspname !~ '^pg_(toast|temp)'
                  and not exists (select 1 from londiste.table_info
                                   where queue_name = i_queue_name and local
                                     and coalesce(dest_table, table_name) = (n.nspname || '.' || r.relname))
                order by 1, 2
        loop
            return next;
        end loop;
    else
        for obj_kind, obj_name in
            select 'S', s.seq_name from londiste.seq_info s
                where s.queue_name = i_queue_name
                  and not s.local
            union all
            select 'r', t.table_name from londiste.table_info t
                where t.queue_name = i_queue_name
                  and not t.local
            order by 1, 2
        loop
            return next;
        end loop;
    end if;
    return;
end; 
$$;


ALTER FUNCTION londiste.local_show_missing(i_queue_name text, OUT obj_kind text, OUT obj_name text) OWNER TO postgres;

--
-- Name: make_fqname(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION make_fqname(i_name text) RETURNS text
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.make_fqname(1)
--
--      Make name to schema-qualified one.
--
--      First dot is taken as schema separator.
--
--      If schema is missing, 'public' is assumed.
--
-- Parameters:
--      i_name  - object name.
--
-- Returns:
--      Schema qualified name.
-- ----------------------------------------------------------------------
begin
    if position('.' in i_name) > 0 then
        return i_name;
    else
        return 'public.' || i_name;
    end if;
end;
$$;


ALTER FUNCTION londiste.make_fqname(i_name text) OWNER TO postgres;

--
-- Name: periodic_maintenance(); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION periodic_maintenance() RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.periodic_maintenance(0)
--
--      Clean random stuff.
-- ----------------------------------------------------------------------
begin

    -- clean old EXECUTE entries
    delete from londiste.applied_execute
        where execute_time < now() - '3 months'::interval;

    return 0;
end;
$$;


ALTER FUNCTION londiste.periodic_maintenance() OWNER TO postgres;

--
-- Name: quote_fqname(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION quote_fqname(i_name text) RETURNS text
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.quote_fqname(1)
--
--      Quete fully-qualified object name for SQL.
--
--      First dot is taken as schema separator.
--
--      If schema is missing, 'public' is assumed.
--
-- Parameters:
--      i_name  - fully qualified object name.
--
-- Returns:
--      Quoted name.
-- ----------------------------------------------------------------------
declare
    res     text;
    pos     integer;
    s       text;
    n       text;
begin
    pos := position('.' in i_name);
    if pos > 0 then
        s := substring(i_name for pos - 1);
        n := substring(i_name from pos + 1);
    else
        s := 'public';
        n := i_name;
    end if;
    return quote_ident(s) || '.' || quote_ident(n);
end;
$$;


ALTER FUNCTION londiste.quote_fqname(i_name text) OWNER TO postgres;

--
-- Name: restore_table_fkey(text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION restore_table_fkey(i_from_table text, i_fkey_name text) RETURNS integer
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.restore_table_fkey(2)
--
--      Restore dropped fkey.
--
-- Parameters:
--      i_from_table - source table
--      i_fkey_name  - fkey name
--
-- Returns:
--      nothing
-- ----------------------------------------------------------------------
declare
    fkey    record;
begin
    select * into fkey
    from londiste.pending_fkeys 
    where fkey_name = i_fkey_name and from_table = i_from_table;
    
    if not found then
        return 0;
    end if;

    execute fkey.fkey_def;

    delete from londiste.pending_fkeys where fkey_name = fkey.fkey_name;
        
    return 1;
end;
$$;


ALTER FUNCTION londiste.restore_table_fkey(i_from_table text, i_fkey_name text) OWNER TO postgres;

--
-- Name: root_check_seqs(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION root_check_seqs(i_queue_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
begin
    select f.ret_code, f.ret_note
        into ret_code, ret_note
        from londiste.root_check_seqs(i_queue_name, 10000) f;
    return;
end;
$$;


ALTER FUNCTION londiste.root_check_seqs(i_queue_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: root_check_seqs(text, bigint); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION root_check_seqs(i_queue_name text, i_buffer bigint, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.root_check_seqs(1)
--
--      Check sequences, and publish values if needed.
--
-- Parameters:
--      i_queue_name    - set name
--      i_buffer        - safety room
--
-- Returns:
--      200 - OK
--      402 - Not a root node
--      404 - Queue not found
-- ----------------------------------------------------------------------
declare
    n record;
    seq record;
    real_value int8;
    pub_value int8;
    real_buffer int8;
begin
    if i_buffer is null or i_buffer < 10 then
        real_buffer := 10000;
    else
        real_buffer := i_buffer;
    end if;

    select node_type, node_name into n
        from pgq_node.node_info
        where queue_name = i_queue_name
        for update;
    if not found then
        select 404, 'Queue not found: ' || i_queue_name into ret_code, ret_note;
        return;
    end if;
    if n.node_type <> 'root' then
        select 402, 'Not a root node' into ret_code, ret_note;
        return;
    end if;

    for seq in
        select seq_name, last_value,
               londiste.quote_fqname(seq_name) as fqname
            from londiste.seq_info
            where queue_name = i_queue_name
                and local
            order by nr
    loop
        execute 'select last_value from ' || seq.fqname into real_value;
        if real_value + real_buffer >= seq.last_value then
            pub_value := real_value + real_buffer * 3;
            perform pgq.insert_event(i_queue_name, 'londiste.update-seq',
                        pub_value::text, seq.seq_name, null, null, null);
            update londiste.seq_info set last_value = pub_value
                where queue_name = i_queue_name
                    and seq_name = seq.seq_name;
        end if;
    end loop;

    select 100, 'Sequences updated' into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION londiste.root_check_seqs(i_queue_name text, i_buffer bigint, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: root_notify_change(text, text, text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION root_notify_change(i_queue_name text, i_ev_type text, i_ev_data text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.root_notify_change(3)
--
--      Send event about change in root downstream.
-- ----------------------------------------------------------------------
declare
    que     text;
    ntype   text;
begin

    if not coalesce(pgq_node.is_root_node(i_queue_name), false) then
        raise exception 'only root node can send events';
    end if;
    perform pgq.insert_event(i_queue_name, i_ev_type, i_ev_data);

    return 1;
end;
$$;


ALTER FUNCTION londiste.root_notify_change(i_queue_name text, i_ev_type text, i_ev_data text) OWNER TO postgres;

--
-- Name: split_fqname(text); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION split_fqname(i_fqname text, OUT schema_part text, OUT name_part text) RETURNS record
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.split_fqname(1)
--
--      Split fqname to schema and name parts.
--
--      First dot is taken as schema separator.
--
--      If schema is missing, 'public' is assumed.
--
-- Parameters:
--      i_fqname  - object name.
-- ----------------------------------------------------------------------
declare
    dot integer;
begin
    dot = position('.' in i_fqname);
    if dot > 0 then
        schema_part = substring(i_fqname for dot - 1);
        name_part = substring(i_fqname from dot + 1);
    else
        schema_part = 'public';
        name_part = i_fqname;
    end if;
    return;
end;
$$;


ALTER FUNCTION londiste.split_fqname(i_fqname text, OUT schema_part text, OUT name_part text) OWNER TO postgres;

--
-- Name: table_info_trigger(); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION table_info_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.table_info_trigger(0)
--
--      Trigger on londiste.table_info.  Cleans triggers from tables
--      when table is removed from londiste.table_info.
-- ----------------------------------------------------------------------
begin
    if TG_OP = 'DELETE' then
        perform londiste.drop_table_triggers(OLD.queue_name, OLD.table_name);
    end if;
    return OLD;
end;
$$;


ALTER FUNCTION londiste.table_info_trigger() OWNER TO postgres;

--
-- Name: upgrade_schema(); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION upgrade_schema() RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- updates table structure if necessary
declare
    pgversion int;
    cnt int4 = 0;
begin
    show server_version_num into pgversion;

    -- table_info: check (dropped_ddl is null or merge_state in ('in-copy', 'catching-up'))
    perform 1 from information_schema.check_constraints
      where constraint_schema = 'londiste'
        and constraint_name = 'table_info_check'
        and position('in-copy' in check_clause) > 0
        and position('catching' in check_clause) = 0;
    if found then
        alter table londiste.table_info drop constraint table_info_check;
        alter table londiste.table_info add constraint table_info_check
            check (dropped_ddl is null or merge_state in ('in-copy', 'catching-up'));
        cnt := cnt + 1;
    end if;

    -- table_info.dest_table
    perform 1 from information_schema.columns
      where table_schema = 'londiste'
        and table_name = 'table_info'
        and column_name = 'dest_table';
    if not found then
        alter table londiste.table_info add column dest_table text;
    end if;

    -- table_info: change trigger timing
    if pgversion >= 90100 then
        perform 1 from information_schema.triggers
          where event_object_schema = 'londiste'
            and event_object_table = 'table_info'
            and trigger_name = 'table_info_trigger_sync'
            and action_timing = 'AFTER';
    else
        perform 1 from information_schema.triggers
          where event_object_schema = 'londiste'
            and event_object_table = 'table_info'
            and trigger_name = 'table_info_trigger_sync'
            and condition_timing = 'AFTER';
    end if;
    if found then
        drop trigger table_info_trigger_sync on londiste.table_info;
        create trigger table_info_trigger_sync before delete on londiste.table_info
            for each row execute procedure londiste.table_info_trigger();
    end if;

    -- applied_execute.dest_table
    perform 1 from information_schema.columns
      where table_schema = 'londiste'
        and table_name = 'applied_execute'
        and column_name = 'execute_attrs';
    if not found then
        alter table londiste.applied_execute add column execute_attrs text;
    end if;

    -- applied_execute: drop queue_name from primary key
    perform 1 from pg_catalog.pg_indexes
      where schemaname = 'londiste'
        and tablename = 'applied_execute'
        and indexname = 'applied_execute_pkey'
        and indexdef like '%queue_name%';
    if found then
        alter table londiste.applied_execute
            drop constraint applied_execute_pkey;
        alter table londiste.applied_execute
            add constraint applied_execute_pkey
            primary key (execute_file);
    end if;

    -- applied_execute: drop fkey to pgq_node
    perform 1 from information_schema.table_constraints
      where constraint_schema = 'londiste'
        and table_schema = 'londiste'
        and table_name = 'applied_execute'
        and constraint_type = 'FOREIGN KEY'
        and constraint_name = 'applied_execute_queue_name_fkey';
    if found then
        alter table londiste.applied_execute
            drop constraint applied_execute_queue_name_fkey;
    end if;

    -- create roles
    perform 1 from pg_catalog.pg_roles where rolname = 'londiste_writer';
    if not found then
        create role londiste_writer in role pgq_admin;
        cnt := cnt + 1;
    end if;
    perform 1 from pg_catalog.pg_roles where rolname = 'londiste_reader';
    if not found then
        create role londiste_reader in role pgq_reader;
        cnt := cnt + 1;
    end if;

    return cnt;
end;
$$;


ALTER FUNCTION londiste.upgrade_schema() OWNER TO postgres;

--
-- Name: version(); Type: FUNCTION; Schema: londiste; Owner: postgres
--

CREATE FUNCTION version() RETURNS text
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: londiste.version(0)
--
--      Returns version string for londiste.  ATM it is based on SkyTools
--      version and only bumped when database code changes.
-- ----------------------------------------------------------------------
begin
    return '3.2.4';
end;
$$;


ALTER FUNCTION londiste.version() OWNER TO postgres;

SET search_path = pgq, pg_catalog;

--
-- Name: _grant_perms_from(text, text, text, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION _grant_perms_from(src_schema text, src_table text, dst_schema text, dst_table text) RETURNS integer
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.grant_perms_from(1)
--
--      Copy grants from one table to another.
--      Workaround for missing GRANTS option for CREATE TABLE LIKE.
-- ----------------------------------------------------------------------
declare
    fq_table text;
    sql text;
    g record;
    q_grantee text;
begin
    fq_table := quote_ident(dst_schema) || '.' || quote_ident(dst_table);

    for g in
        select grantor, grantee, privilege_type, is_grantable
            from information_schema.table_privileges
            where table_schema = src_schema
                and table_name = src_table
    loop
        if g.grantee = 'PUBLIC' then
            q_grantee = 'public';
        else
            q_grantee = quote_ident(g.grantee);
        end if;
        sql := 'grant ' || g.privilege_type || ' on ' || fq_table
            || ' to ' || q_grantee;
        if g.is_grantable = 'YES' then
            sql := sql || ' with grant option';
        end if;
        execute sql;
    end loop;

    return 1;
end;
$$;


ALTER FUNCTION pgq._grant_perms_from(src_schema text, src_table text, dst_schema text, dst_table text) OWNER TO postgres;

--
-- Name: batch_event_sql(bigint); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION batch_event_sql(x_batch_id bigint) RETURNS text
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.batch_event_sql(1)
--      Creates SELECT statement that fetches events for this batch.
--
-- Parameters:
--      x_batch_id    - ID of a active batch.
--
-- Returns:
--      SQL statement.
-- ----------------------------------------------------------------------

-- ----------------------------------------------------------------------
-- Algorithm description:
--      Given 2 snapshots, sn1 and sn2 with sn1 having xmin1, xmax1
--      and sn2 having xmin2, xmax2 create expression that filters
--      right txid's from event table.
--
--      Simplest solution would be
--      > WHERE ev_txid >= xmin1 AND ev_txid <= xmax2
--      >   AND NOT txid_visible_in_snapshot(ev_txid, sn1)
--      >   AND txid_visible_in_snapshot(ev_txid, sn2)
--
--      The simple solution has a problem with long transactions (xmin1 very low).
--      All the batches that happen when the long tx is active will need
--      to scan all events in that range.  Here is 2 optimizations used:
--
--      1)  Use [xmax1..xmax2] for range scan.  That limits the range to
--      txids that actually happened between two snapshots.  For txids
--      in the range [xmin1..xmax1] look which ones were actually
--      committed between snapshots and search for them using exact
--      values using IN (..) list.
--
--      2) As most TX are short, there could be lot of them that were
--      just below xmax1, but were committed before xmax2.  So look
--      if there are ID's near xmax1 and lower the range to include
--      them, thus decresing size of IN (..) list.
-- ----------------------------------------------------------------------
declare
    rec             record;
    sql             text;
    tbl             text;
    arr             text;
    part            text;
    select_fields   text;
    retry_expr      text;
    batch           record;
begin
    select s.sub_last_tick, s.sub_next_tick, s.sub_id, s.sub_queue,
           txid_snapshot_xmax(last.tick_snapshot) as tx_start,
           txid_snapshot_xmax(cur.tick_snapshot) as tx_end,
           last.tick_snapshot as last_snapshot,
           cur.tick_snapshot as cur_snapshot
        into batch
        from pgq.subscription s, pgq.tick last, pgq.tick cur
        where s.sub_batch = x_batch_id
          and last.tick_queue = s.sub_queue
          and last.tick_id = s.sub_last_tick
          and cur.tick_queue = s.sub_queue
          and cur.tick_id = s.sub_next_tick;
    if not found then
        raise exception 'batch not found';
    end if;

    -- load older transactions
    arr := '';
    for rec in
        -- active tx-es in prev_snapshot that were committed in cur_snapshot
        select id1 from
            txid_snapshot_xip(batch.last_snapshot) id1 left join
            txid_snapshot_xip(batch.cur_snapshot) id2 on (id1 = id2)
        where id2 is null
        order by 1 desc
    loop
        -- try to avoid big IN expression, so try to include nearby
        -- tx'es into range
        if batch.tx_start - 100 <= rec.id1 then
            batch.tx_start := rec.id1;
        else
            if arr = '' then
                arr := rec.id1::text;
            else
                arr := arr || ',' || rec.id1::text;
            end if;
        end if;
    end loop;

    -- must match pgq.event_template
    select_fields := 'select ev_id, ev_time, ev_txid, ev_retry, ev_type,'
        || ' ev_data, ev_extra1, ev_extra2, ev_extra3, ev_extra4';
    retry_expr :=  ' and (ev_owner is null or ev_owner = '
        || batch.sub_id::text || ')';

    -- now generate query that goes over all potential tables
    sql := '';
    for rec in
        select xtbl from pgq.batch_event_tables(x_batch_id) xtbl
    loop
        tbl := pgq.quote_fqname(rec.xtbl);
        -- this gets newer queries that definitely are not in prev_snapshot
        part := select_fields
            || ' from pgq.tick cur, pgq.tick last, ' || tbl || ' ev '
            || ' where cur.tick_id = ' || batch.sub_next_tick::text
            || ' and cur.tick_queue = ' || batch.sub_queue::text
            || ' and last.tick_id = ' || batch.sub_last_tick::text
            || ' and last.tick_queue = ' || batch.sub_queue::text
            || ' and ev.ev_txid >= ' || batch.tx_start::text
            || ' and ev.ev_txid <= ' || batch.tx_end::text
            || ' and txid_visible_in_snapshot(ev.ev_txid, cur.tick_snapshot)'
            || ' and not txid_visible_in_snapshot(ev.ev_txid, last.tick_snapshot)'
            || retry_expr;
        -- now include older tx-es, that were ongoing
        -- at the time of prev_snapshot
        if arr <> '' then
            part := part || ' union all '
                || select_fields || ' from ' || tbl || ' ev '
                || ' where ev.ev_txid in (' || arr || ')'
                || retry_expr;
        end if;
        if sql = '' then
            sql := part;
        else
            sql := sql || ' union all ' || part;
        end if;
    end loop;
    if sql = '' then
        raise exception 'could not construct sql for batch %', x_batch_id;
    end if;
    return sql || ' order by 1';
end;
$$;


ALTER FUNCTION pgq.batch_event_sql(x_batch_id bigint) OWNER TO postgres;

--
-- Name: batch_event_tables(bigint); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION batch_event_tables(x_batch_id bigint) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.batch_event_tables(1)
--
--     Returns set of table names where this batch events may reside.
--
-- Parameters:
--     x_batch_id    - ID of a active batch.
-- ----------------------------------------------------------------------
declare
    nr                    integer;
    tbl                   text;
    use_prev              integer;
    use_next              integer;
    batch                 record;
begin
    select
           txid_snapshot_xmin(last.tick_snapshot) as tx_min, -- absolute minimum
           txid_snapshot_xmax(cur.tick_snapshot) as tx_max, -- absolute maximum
           q.queue_data_pfx, q.queue_ntables,
           q.queue_cur_table, q.queue_switch_step1, q.queue_switch_step2
        into batch
        from pgq.tick last, pgq.tick cur, pgq.subscription s, pgq.queue q
        where cur.tick_id = s.sub_next_tick
          and cur.tick_queue = s.sub_queue
          and last.tick_id = s.sub_last_tick
          and last.tick_queue = s.sub_queue
          and s.sub_batch = x_batch_id
          and q.queue_id = s.sub_queue;
    if not found then
        raise exception 'Cannot find data for batch %', x_batch_id;
    end if;

    -- if its definitely not in one or other, look into both
    if batch.tx_max < batch.queue_switch_step1 then
        use_prev := 1;
        use_next := 0;
    elsif batch.queue_switch_step2 is not null
      and (batch.tx_min > batch.queue_switch_step2)
    then
        use_prev := 0;
        use_next := 1;
    else
        use_prev := 1;
        use_next := 1;
    end if;

    if use_prev then
        nr := batch.queue_cur_table - 1;
        if nr < 0 then
            nr := batch.queue_ntables - 1;
        end if;
        tbl := batch.queue_data_pfx || '_' || nr::text;
        return next tbl;
    end if;

    if use_next then
        tbl := batch.queue_data_pfx || '_' || batch.queue_cur_table::text;
        return next tbl;
    end if;

    return;
end;
$$;


ALTER FUNCTION pgq.batch_event_tables(x_batch_id bigint) OWNER TO postgres;

--
-- Name: batch_retry(bigint, integer); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION batch_retry(i_batch_id bigint, i_retry_seconds integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.batch_retry(2)
--
--     Put whole batch into retry queue, to be processed again later.
--
-- Parameters:
--      i_batch_id      - ID of active batch.
--      i_retry_time    - Time when the event should be put back into queue
--
-- Returns:
--     number of events inserted
-- Calls:
--      None
-- Tables directly manipulated:
--      pgq.retry_queue
-- ----------------------------------------------------------------------
declare
    _retry timestamptz;
    _cnt   integer;
    _s     record;
begin
    _retry := current_timestamp + ((i_retry_seconds::text || ' seconds')::interval);

    select * into _s from pgq.subscription where sub_batch = i_batch_id;
    if not found then
        raise exception 'batch_retry: batch % not found', i_batch_id;
    end if;

    insert into pgq.retry_queue (ev_retry_after, ev_queue,
        ev_id, ev_time, ev_txid, ev_owner, ev_retry,
        ev_type, ev_data, ev_extra1, ev_extra2,
        ev_extra3, ev_extra4)
    select distinct _retry, _s.sub_queue,
           b.ev_id, b.ev_time, NULL::int8, _s.sub_id, coalesce(b.ev_retry, 0) + 1,
           b.ev_type, b.ev_data, b.ev_extra1, b.ev_extra2,
           b.ev_extra3, b.ev_extra4
      from pgq.get_batch_events(i_batch_id) b
           left join pgq.retry_queue rq
                  on (rq.ev_id = b.ev_id
                      and rq.ev_owner = _s.sub_id
                      and rq.ev_queue = _s.sub_queue)
      where rq.ev_id is null;

    GET DIAGNOSTICS _cnt = ROW_COUNT;
    return _cnt;
end;
$$;


ALTER FUNCTION pgq.batch_retry(i_batch_id bigint, i_retry_seconds integer) OWNER TO postgres;

--
-- Name: create_queue(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION create_queue(i_queue_name text) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.create_queue(1)
--
--      Creates new queue with given name.
--
-- Returns:
--      0 - queue already exists
--      1 - queue created
-- Calls:
--      pgq.grant_perms(i_queue_name);
--      pgq.ticker(i_queue_name);
--      pgq.tune_storage(i_queue_name);
-- Tables directly manipulated:
--      insert - pgq.queue
--      create - pgq.event_N () inherits (pgq.event_template)
--      create - pgq.event_N_0 .. pgq.event_N_M () inherits (pgq.event_N)
-- ----------------------------------------------------------------------
declare
    tblpfx   text;
    tblname  text;
    idxpfx   text;
    idxname  text;
    sql      text;
    id       integer;
    tick_seq text;
    ev_seq text;
    n_tables integer;
begin
    if i_queue_name is null then
        raise exception 'Invalid NULL value';
    end if;

    -- check if exists
    perform 1 from pgq.queue where queue_name = i_queue_name;
    if found then
        return 0;
    end if;

    -- insert event
    id := nextval('pgq.queue_queue_id_seq');
    tblpfx := 'pgq.event_' || id::text;
    idxpfx := 'event_' || id::text;
    tick_seq := 'pgq.event_' || id::text || '_tick_seq';
    ev_seq := 'pgq.event_' || id::text || '_id_seq';
    insert into pgq.queue (queue_id, queue_name,
            queue_data_pfx, queue_event_seq, queue_tick_seq)
        values (id, i_queue_name, tblpfx, ev_seq, tick_seq);

    select queue_ntables into n_tables from pgq.queue
        where queue_id = id;

    -- create seqs
    execute 'CREATE SEQUENCE ' || pgq.quote_fqname(tick_seq);
    execute 'CREATE SEQUENCE ' || pgq.quote_fqname(ev_seq);

    -- create data tables
    execute 'CREATE TABLE ' || pgq.quote_fqname(tblpfx) || ' () '
            || ' INHERITS (pgq.event_template)';
    for i in 0 .. (n_tables - 1) loop
        tblname := tblpfx || '_' || i::text;
        idxname := idxpfx || '_' || i::text || '_txid_idx';
        execute 'CREATE TABLE ' || pgq.quote_fqname(tblname) || ' () '
                || ' INHERITS (' || pgq.quote_fqname(tblpfx) || ')';
        execute 'ALTER TABLE ' || pgq.quote_fqname(tblname) || ' ALTER COLUMN ev_id '
                || ' SET DEFAULT nextval(' || quote_literal(ev_seq) || ')';
        execute 'create index ' || quote_ident(idxname) || ' on '
                || pgq.quote_fqname(tblname) || ' (ev_txid)';
    end loop;

    perform pgq.grant_perms(i_queue_name);

    perform pgq.ticker(i_queue_name);

    perform pgq.tune_storage(i_queue_name);

    return 1;
end;
$$;


ALTER FUNCTION pgq.create_queue(i_queue_name text) OWNER TO postgres;

--
-- Name: current_event_table(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION current_event_table(x_queue_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.current_event_table(1)
--
--      Return active event table for particular queue.
--      Event can be added to it without going via functions,
--      e.g. by COPY.
--
--      If queue is disabled and GUC session_replication_role <> 'replica'
--      then raises exception.
--
--      or expressed in a different way - an even table of a disabled queue
--      is returned only on replica
--
-- Note:
--      The result is valid only during current transaction.
--
-- Permissions:
--      Actual insertion requires superuser access.
--
-- Parameters:
--      x_queue_name    - Queue name.
-- ----------------------------------------------------------------------
declare
    res text;
    disabled boolean;
begin
    select queue_data_pfx || '_' || queue_cur_table::text,
           queue_disable_insert
        into res, disabled
        from pgq.queue where queue_name = x_queue_name;
    if not found then
        raise exception 'Event queue not found';
    end if;
    if disabled then
        if current_setting('session_replication_role') <> 'replica' then
            raise exception 'Writing to queue disabled';
        end if;
    end if;
    return res;
end;
$$;


ALTER FUNCTION pgq.current_event_table(x_queue_name text) OWNER TO postgres;

--
-- Name: drop_queue(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION drop_queue(x_queue_name text) RETURNS integer
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.drop_queue(1)
--
--     Drop queue and all associated tables.
--     No consumers must be listening on the queue.
--
-- ----------------------------------------------------------------------
begin
    return pgq.drop_queue(x_queue_name, false);
end;
$$;


ALTER FUNCTION pgq.drop_queue(x_queue_name text) OWNER TO postgres;

--
-- Name: drop_queue(text, boolean); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION drop_queue(x_queue_name text, x_force boolean) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.drop_queue(2)
--
--     Drop queue and all associated tables.
--
-- Parameters:
--      x_queue_name    - queue name
--      x_force         - ignore (drop) existing consumers
-- Returns:
--      1 - success
-- Calls:
--      pgq.unregister_consumer(queue_name, consumer_name)
--      perform pgq.ticker(i_queue_name);
--      perform pgq.tune_storage(i_queue_name);
-- Tables directly manipulated:
--      delete - pgq.queue
--      drop - pgq.event_N (), pgq.event_N_0 .. pgq.event_N_M 
-- ----------------------------------------------------------------------
declare
    tblname  text;
    q record;
    num integer;
begin
    -- check if exists
    select * into q from pgq.queue
        where queue_name = x_queue_name
        for update;
    if not found then
        raise exception 'No such event queue';
    end if;

    if x_force then
        perform pgq.unregister_consumer(queue_name, consumer_name)
           from pgq.get_consumer_info(x_queue_name);
    else
        -- check if no consumers
        select count(*) into num from pgq.subscription
            where sub_queue = q.queue_id;
        if num > 0 then
            raise exception 'cannot drop queue, consumers still attached';
        end if;
    end if;

    -- drop data tables
    for i in 0 .. (q.queue_ntables - 1) loop
        tblname := q.queue_data_pfx || '_' || i::text;
        execute 'DROP TABLE ' || pgq.quote_fqname(tblname);
    end loop;
    execute 'DROP TABLE ' || pgq.quote_fqname(q.queue_data_pfx);

    -- delete ticks
    delete from pgq.tick where tick_queue = q.queue_id;

    -- drop seqs
    -- FIXME: any checks needed here?
    execute 'DROP SEQUENCE ' || pgq.quote_fqname(q.queue_tick_seq);
    execute 'DROP SEQUENCE ' || pgq.quote_fqname(q.queue_event_seq);

    -- delete event
    delete from pgq.queue
        where queue_name = x_queue_name;

    return 1;
end;
$$;


ALTER FUNCTION pgq.drop_queue(x_queue_name text, x_force boolean) OWNER TO postgres;

--
-- Name: event_retry(bigint, bigint, integer); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION event_retry(x_batch_id bigint, x_event_id bigint, x_retry_seconds integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.event_retry(3b)
--
--     Put the event into retry queue, to be processed later again.
--
-- Parameters:
--      x_batch_id      - ID of active batch.
--      x_event_id      - event id
--      x_retry_seconds - Time when the event should be put back into queue
--
-- Returns:
--     1 - success
--     0 - event already in retry queue
-- Calls:
--      pgq.event_retry(3a)
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
declare
    new_retry  timestamptz;
begin
    new_retry := current_timestamp + ((x_retry_seconds::text || ' seconds')::interval);
    return pgq.event_retry(x_batch_id, x_event_id, new_retry);
end;
$$;


ALTER FUNCTION pgq.event_retry(x_batch_id bigint, x_event_id bigint, x_retry_seconds integer) OWNER TO postgres;

--
-- Name: event_retry(bigint, bigint, timestamp with time zone); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION event_retry(x_batch_id bigint, x_event_id bigint, x_retry_time timestamp with time zone) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.event_retry(3a)
--
--     Put the event into retry queue, to be processed again later.
--
-- Parameters:
--      x_batch_id      - ID of active batch.
--      x_event_id      - event id
--      x_retry_time    - Time when the event should be put back into queue
--
-- Returns:
--     1 - success
--     0 - event already in retry queue
-- Calls:
--      None
-- Tables directly manipulated:
--      insert - pgq.retry_queue
-- ----------------------------------------------------------------------
begin
    insert into pgq.retry_queue (ev_retry_after, ev_queue,
        ev_id, ev_time, ev_txid, ev_owner, ev_retry, ev_type, ev_data,
        ev_extra1, ev_extra2, ev_extra3, ev_extra4)
    select x_retry_time, sub_queue,
           ev_id, ev_time, NULL, sub_id, coalesce(ev_retry, 0) + 1,
           ev_type, ev_data, ev_extra1, ev_extra2, ev_extra3, ev_extra4
      from pgq.get_batch_events(x_batch_id),
           pgq.subscription
     where sub_batch = x_batch_id
       and ev_id = x_event_id;
    if not found then
        raise exception 'event not found';
    end if;
    return 1;

-- dont worry if the event is already in queue
exception
    when unique_violation then
        return 0;
end;
$$;


ALTER FUNCTION pgq.event_retry(x_batch_id bigint, x_event_id bigint, x_retry_time timestamp with time zone) OWNER TO postgres;

--
-- Name: event_retry_raw(text, text, timestamp with time zone, bigint, timestamp with time zone, integer, text, text, text, text, text, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION event_retry_raw(x_queue text, x_consumer text, x_retry_after timestamp with time zone, x_ev_id bigint, x_ev_time timestamp with time zone, x_ev_retry integer, x_ev_type text, x_ev_data text, x_ev_extra1 text, x_ev_extra2 text, x_ev_extra3 text, x_ev_extra4 text) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.event_retry_raw(12)
--
--      Allows full control over what goes to retry queue.
--
-- Parameters:
--      x_queue         - name of the queue
--      x_consumer      - name of the consumer
--      x_retry_after   - when the event should be processed again
--      x_ev_id         - event id
--      x_ev_time       - creation time
--      x_ev_retry      - retry count
--      x_ev_type       - user data
--      x_ev_data       - user data
--      x_ev_extra1     - user data
--      x_ev_extra2     - user data
--      x_ev_extra3     - user data
--      x_ev_extra4     - user data
--
-- Returns:
--      Event ID.
-- ----------------------------------------------------------------------
declare
    q record;
    id bigint;
begin
    select sub_id, queue_event_seq, sub_queue into q
      from pgq.consumer, pgq.queue, pgq.subscription
     where queue_name = x_queue
       and co_name = x_consumer
       and sub_consumer = co_id
       and sub_queue = queue_id;
    if not found then
        raise exception 'consumer not registered';
    end if;

    id := x_ev_id;
    if id is null then
        id := nextval(q.queue_event_seq);
    end if;

    insert into pgq.retry_queue (ev_retry_after, ev_queue,
            ev_id, ev_time, ev_owner, ev_retry,
            ev_type, ev_data, ev_extra1, ev_extra2, ev_extra3, ev_extra4)
    values (x_retry_after, q.sub_queue,
            id, x_ev_time, q.sub_id, x_ev_retry,
            x_ev_type, x_ev_data, x_ev_extra1, x_ev_extra2,
            x_ev_extra3, x_ev_extra4);

    return id;
end;
$$;


ALTER FUNCTION pgq.event_retry_raw(x_queue text, x_consumer text, x_retry_after timestamp with time zone, x_ev_id bigint, x_ev_time timestamp with time zone, x_ev_retry integer, x_ev_type text, x_ev_data text, x_ev_extra1 text, x_ev_extra2 text, x_ev_extra3 text, x_ev_extra4 text) OWNER TO postgres;

--
-- Name: find_tick_helper(integer, bigint, timestamp with time zone, bigint, bigint, interval); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION find_tick_helper(i_queue_id integer, i_prev_tick_id bigint, i_prev_tick_time timestamp with time zone, i_prev_tick_seq bigint, i_min_count bigint, i_min_interval interval, OUT next_tick_id bigint, OUT next_tick_time timestamp with time zone, OUT next_tick_seq bigint) RETURNS record
    LANGUAGE plpgsql STABLE
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.find_tick_helper(6)
--
--      Helper function for pgq.next_batch_custom() to do extended tick search.
-- ----------------------------------------------------------------------
declare
    sure    boolean;
    can_set boolean;
    t       record;
    cnt     int8;
    ival    interval;
begin
    -- first, fetch last tick of the queue
    select tick_id, tick_time, tick_event_seq into t
        from pgq.tick
        where tick_queue = i_queue_id
          and tick_id > i_prev_tick_id
        order by tick_queue desc, tick_id desc
        limit 1;
    if not found then
        return;
    end if;
    
    -- check whether batch would end up within reasonable limits
    sure := true;
    can_set := false;
    if i_min_count is not null then
        cnt = t.tick_event_seq - i_prev_tick_seq;
        if cnt >= i_min_count then
            can_set := true;
        end if;
        if cnt > i_min_count * 2 then
            sure := false;
        end if;
    end if;
    if i_min_interval is not null then
        ival = t.tick_time - i_prev_tick_time;
        if ival >= i_min_interval then
            can_set := true;
        end if;
        if ival > i_min_interval * 2 then
            sure := false;
        end if;
    end if;

    -- if last tick too far away, do large scan
    if not sure then
        select tick_id, tick_time, tick_event_seq into t
            from pgq.tick
            where tick_queue = i_queue_id
              and tick_id > i_prev_tick_id
              and ((i_min_count is not null and (tick_event_seq - i_prev_tick_seq) >= i_min_count)
                  or
                   (i_min_interval is not null and (tick_time - i_prev_tick_time) >= i_min_interval))
            order by tick_queue asc, tick_id asc
            limit 1;
        can_set := true;
    end if;
    if can_set then
        next_tick_id := t.tick_id;
        next_tick_time := t.tick_time;
        next_tick_seq := t.tick_event_seq;
    end if;
    return;
end;
$$;


ALTER FUNCTION pgq.find_tick_helper(i_queue_id integer, i_prev_tick_id bigint, i_prev_tick_time timestamp with time zone, i_prev_tick_seq bigint, i_min_count bigint, i_min_interval interval, OUT next_tick_id bigint, OUT next_tick_time timestamp with time zone, OUT next_tick_seq bigint) OWNER TO postgres;

--
-- Name: finish_batch(bigint); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION finish_batch(x_batch_id bigint) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.finish_batch(1)
--
--      Closes a batch.  No more operations can be done with events
--      of this batch.
--
-- Parameters:
--      x_batch_id      - id of batch.
--
-- Returns:
--      1 if batch was found, 0 otherwise.
-- Calls:
--      None
-- Tables directly manipulated:
--      update - pgq.subscription
-- ----------------------------------------------------------------------
begin
    update pgq.subscription
        set sub_active = now(),
            sub_last_tick = sub_next_tick,
            sub_next_tick = null,
            sub_batch = null
        where sub_batch = x_batch_id;
    if not found then
        raise warning 'finish_batch: batch % not found', x_batch_id;
        return 0;
    end if;

    return 1;
end;
$$;


ALTER FUNCTION pgq.finish_batch(x_batch_id bigint) OWNER TO postgres;

--
-- Name: force_tick(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION force_tick(i_queue_name text) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.force_tick(2)
--
--      Simulate lots of events happening to force ticker to tick.
--
--      Should be called in loop, with some delay until last tick
--      changes or too much time is passed.
--
--      Such function is needed because paraller calls of pgq.ticker() are
--      dangerous, and cannot be protected with locks as snapshot
--      is taken before locking.
--
-- Parameters:
--      i_queue_name     - Name of the queue
--
-- Returns:
--      Currently last tick id.
-- ----------------------------------------------------------------------
declare
    q  record;
    t  record;
begin
    -- bump seq and get queue id
    select queue_id,
           setval(queue_event_seq, nextval(queue_event_seq)
                                   + queue_ticker_max_count * 2 + 1000) as tmp
      into q from pgq.queue
     where queue_name = i_queue_name
       and not queue_external_ticker
       and not queue_ticker_paused;

    --if not found then
    --    raise notice 'queue not found or ticks not allowed';
    --end if;

    -- return last tick id
    select tick_id into t
      from pgq.tick, pgq.queue
     where tick_queue = queue_id and queue_name = i_queue_name
     order by tick_queue desc, tick_id desc limit 1;

    return t.tick_id;
end;
$$;


ALTER FUNCTION pgq.force_tick(i_queue_name text) OWNER TO postgres;

--
-- Name: get_batch_cursor(bigint, text, integer); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION get_batch_cursor(i_batch_id bigint, i_cursor_name text, i_quick_limit integer, OUT ev_id bigint, OUT ev_time timestamp with time zone, OUT ev_txid bigint, OUT ev_retry integer, OUT ev_type text, OUT ev_data text, OUT ev_extra1 text, OUT ev_extra2 text, OUT ev_extra3 text, OUT ev_extra4 text) RETURNS SETOF record
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.get_batch_cursor(3)
--
--      Get events in batch using a cursor.
--
-- Parameters:
--      i_batch_id      - ID of active batch.
--      i_cursor_name   - Name for new cursor
--      i_quick_limit   - Number of events to return immediately
--
-- Returns:
--      List of events.
-- Calls:
--      pgq.get_batch_cursor(4)
-- ----------------------------------------------------------------------
begin
    for ev_id, ev_time, ev_txid, ev_retry, ev_type, ev_data,
        ev_extra1, ev_extra2, ev_extra3, ev_extra4
    in
        select * from pgq.get_batch_cursor(i_batch_id,
            i_cursor_name, i_quick_limit, null)
    loop
        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION pgq.get_batch_cursor(i_batch_id bigint, i_cursor_name text, i_quick_limit integer, OUT ev_id bigint, OUT ev_time timestamp with time zone, OUT ev_txid bigint, OUT ev_retry integer, OUT ev_type text, OUT ev_data text, OUT ev_extra1 text, OUT ev_extra2 text, OUT ev_extra3 text, OUT ev_extra4 text) OWNER TO postgres;

--
-- Name: get_batch_cursor(bigint, text, integer, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION get_batch_cursor(i_batch_id bigint, i_cursor_name text, i_quick_limit integer, i_extra_where text, OUT ev_id bigint, OUT ev_time timestamp with time zone, OUT ev_txid bigint, OUT ev_retry integer, OUT ev_type text, OUT ev_data text, OUT ev_extra1 text, OUT ev_extra2 text, OUT ev_extra3 text, OUT ev_extra4 text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.get_batch_cursor(4)
--
--      Get events in batch using a cursor.
--
-- Parameters:
--      i_batch_id      - ID of active batch.
--      i_cursor_name   - Name for new cursor
--      i_quick_limit   - Number of events to return immediately
--      i_extra_where   - optional where clause to filter events
--
-- Returns:
--      List of events.
-- Calls:
--      pgq.batch_event_sql(i_batch_id) - internal function which generates SQL optimised specially for getting events in this batch
-- ----------------------------------------------------------------------
declare
    _cname  text;
    _sql    text;
begin
    if i_batch_id is null or i_cursor_name is null or i_quick_limit is null then
        return;
    end if;

    _cname := quote_ident(i_cursor_name);
    _sql := pgq.batch_event_sql(i_batch_id);

    -- apply extra where
    if i_extra_where is not null then
        _sql := replace(_sql, ' order by 1', '');
        _sql := 'select * from (' || _sql
            || ') _evs where ' || i_extra_where
            || ' order by 1';
    end if;

    -- create cursor
    execute 'declare ' || _cname || ' no scroll cursor for ' || _sql;

    -- if no events wanted, don't bother with execute
    if i_quick_limit <= 0 then
        return;
    end if;

    -- return first block of events
    for ev_id, ev_time, ev_txid, ev_retry, ev_type, ev_data,
        ev_extra1, ev_extra2, ev_extra3, ev_extra4
        in execute 'fetch ' || i_quick_limit::text || ' from ' || _cname
    loop
        return next;
    end loop;

    return;
end;
$$;


ALTER FUNCTION pgq.get_batch_cursor(i_batch_id bigint, i_cursor_name text, i_quick_limit integer, i_extra_where text, OUT ev_id bigint, OUT ev_time timestamp with time zone, OUT ev_txid bigint, OUT ev_retry integer, OUT ev_type text, OUT ev_data text, OUT ev_extra1 text, OUT ev_extra2 text, OUT ev_extra3 text, OUT ev_extra4 text) OWNER TO postgres;

--
-- Name: get_batch_events(bigint); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION get_batch_events(x_batch_id bigint, OUT ev_id bigint, OUT ev_time timestamp with time zone, OUT ev_txid bigint, OUT ev_retry integer, OUT ev_type text, OUT ev_data text, OUT ev_extra1 text, OUT ev_extra2 text, OUT ev_extra3 text, OUT ev_extra4 text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.get_batch_events(1)
--
--      Get all events in batch.
--
-- Parameters:
--      x_batch_id      - ID of active batch.
--
-- Returns:
--      List of events.
-- ----------------------------------------------------------------------
declare
    sql text;
begin
    sql := pgq.batch_event_sql(x_batch_id);
    for ev_id, ev_time, ev_txid, ev_retry, ev_type, ev_data,
        ev_extra1, ev_extra2, ev_extra3, ev_extra4
        in execute sql
    loop
        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION pgq.get_batch_events(x_batch_id bigint, OUT ev_id bigint, OUT ev_time timestamp with time zone, OUT ev_txid bigint, OUT ev_retry integer, OUT ev_type text, OUT ev_data text, OUT ev_extra1 text, OUT ev_extra2 text, OUT ev_extra3 text, OUT ev_extra4 text) OWNER TO postgres;

--
-- Name: get_batch_info(bigint); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION get_batch_info(x_batch_id bigint, OUT queue_name text, OUT consumer_name text, OUT batch_start timestamp with time zone, OUT batch_end timestamp with time zone, OUT prev_tick_id bigint, OUT tick_id bigint, OUT lag interval, OUT seq_start bigint, OUT seq_end bigint) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.get_batch_info(1)
--
--      Returns detailed info about a batch.
--
-- Parameters:
--      x_batch_id      - id of a active batch.
--
-- Returns: ??? pls check
--      queue_name      - which queue this batch came from
--      consumer_name   - batch processed by
--      batch_start     - start time of batch
--      batch_end       - end time of batch
--      prev_tick_id    - start tick for this batch
--      tick_id         - end tick for this batch
--      lag             - now() - tick_id.time 
--      seq_start       - start event id for batch
--      seq_end         - end event id for batch
-- ----------------------------------------------------------------------
begin
    select q.queue_name, c.co_name,
           prev.tick_time, cur.tick_time,
           s.sub_last_tick, s.sub_next_tick,
           current_timestamp - cur.tick_time,
           prev.tick_event_seq, cur.tick_event_seq
        into queue_name, consumer_name, batch_start, batch_end,
             prev_tick_id, tick_id, lag, seq_start, seq_end
        from pgq.subscription s, pgq.tick cur, pgq.tick prev,
             pgq.queue q, pgq.consumer c
        where s.sub_batch = x_batch_id
          and prev.tick_id = s.sub_last_tick
          and prev.tick_queue = s.sub_queue
          and cur.tick_id = s.sub_next_tick
          and cur.tick_queue = s.sub_queue
          and q.queue_id = s.sub_queue
          and c.co_id = s.sub_consumer;
    return;
end;
$$;


ALTER FUNCTION pgq.get_batch_info(x_batch_id bigint, OUT queue_name text, OUT consumer_name text, OUT batch_start timestamp with time zone, OUT batch_end timestamp with time zone, OUT prev_tick_id bigint, OUT tick_id bigint, OUT lag interval, OUT seq_start bigint, OUT seq_end bigint) OWNER TO postgres;

--
-- Name: get_consumer_info(); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION get_consumer_info(OUT queue_name text, OUT consumer_name text, OUT lag interval, OUT last_seen interval, OUT last_tick bigint, OUT current_batch bigint, OUT next_tick bigint, OUT pending_events bigint) RETURNS SETOF record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.get_consumer_info(0)
--
--      Returns info about all consumers on all queues.
--
-- Returns:
--      See pgq.get_consumer_info(2)
-- ----------------------------------------------------------------------
begin
    for queue_name, consumer_name, lag, last_seen,
        last_tick, current_batch, next_tick, pending_events
    in
        select f.queue_name, f.consumer_name, f.lag, f.last_seen,
               f.last_tick, f.current_batch, f.next_tick, f.pending_events
            from pgq.get_consumer_info(null, null) f
    loop
        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION pgq.get_consumer_info(OUT queue_name text, OUT consumer_name text, OUT lag interval, OUT last_seen interval, OUT last_tick bigint, OUT current_batch bigint, OUT next_tick bigint, OUT pending_events bigint) OWNER TO postgres;

--
-- Name: get_consumer_info(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION get_consumer_info(i_queue_name text, OUT queue_name text, OUT consumer_name text, OUT lag interval, OUT last_seen interval, OUT last_tick bigint, OUT current_batch bigint, OUT next_tick bigint, OUT pending_events bigint) RETURNS SETOF record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.get_consumer_info(1)
--
--      Returns info about all consumers on single queue.
--
-- Returns:
--      See pgq.get_consumer_info(2)
-- ----------------------------------------------------------------------
begin
    for queue_name, consumer_name, lag, last_seen,
        last_tick, current_batch, next_tick, pending_events
    in
        select f.queue_name, f.consumer_name, f.lag, f.last_seen,
               f.last_tick, f.current_batch, f.next_tick, f.pending_events
            from pgq.get_consumer_info(i_queue_name, null) f
    loop
        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION pgq.get_consumer_info(i_queue_name text, OUT queue_name text, OUT consumer_name text, OUT lag interval, OUT last_seen interval, OUT last_tick bigint, OUT current_batch bigint, OUT next_tick bigint, OUT pending_events bigint) OWNER TO postgres;

--
-- Name: get_consumer_info(text, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION get_consumer_info(i_queue_name text, i_consumer_name text, OUT queue_name text, OUT consumer_name text, OUT lag interval, OUT last_seen interval, OUT last_tick bigint, OUT current_batch bigint, OUT next_tick bigint, OUT pending_events bigint) RETURNS SETOF record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.get_consumer_info(2)
--
--      Get info about particular consumer on particular queue.
--
-- Parameters:
--      i_queue_name        - name of a queue. (null = all)
--      i_consumer_name     - name of a consumer (null = all)
--
-- Returns:
--      queue_name          - Queue name
--      consumer_name       - Consumer name
--      lag                 - How old are events the consumer is processing
--      last_seen           - When the consumer seen by pgq
--      last_tick           - Tick ID of last processed tick
--      current_batch       - Current batch ID, if one is active or NULL
--      next_tick           - If batch is active, then its final tick.
-- ----------------------------------------------------------------------
declare
    _pending_events bigint;
    _queue_id bigint;
begin
    for queue_name, consumer_name, lag, last_seen,
        last_tick, current_batch, next_tick, _pending_events, _queue_id
    in
        select q.queue_name, c.co_name,
               current_timestamp - t.tick_time,
               current_timestamp - s.sub_active,
               s.sub_last_tick, s.sub_batch, s.sub_next_tick,
               t.tick_event_seq, q.queue_id
          from pgq.queue q,
               pgq.consumer c,
               pgq.subscription s
               left join pgq.tick t
                 on (t.tick_queue = s.sub_queue and t.tick_id = s.sub_last_tick)
         where q.queue_id = s.sub_queue
           and c.co_id = s.sub_consumer
           and (i_queue_name is null or q.queue_name = i_queue_name)
           and (i_consumer_name is null or c.co_name = i_consumer_name)
         order by 1,2
    loop
        select t.tick_event_seq - _pending_events
            into pending_events
            from pgq.tick t
            where t.tick_queue = _queue_id
            order by t.tick_queue desc, t.tick_id desc
            limit 1;
        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION pgq.get_consumer_info(i_queue_name text, i_consumer_name text, OUT queue_name text, OUT consumer_name text, OUT lag interval, OUT last_seen interval, OUT last_tick bigint, OUT current_batch bigint, OUT next_tick bigint, OUT pending_events bigint) OWNER TO postgres;

--
-- Name: get_queue_info(); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION get_queue_info(OUT queue_name text, OUT queue_ntables integer, OUT queue_cur_table integer, OUT queue_rotation_period interval, OUT queue_switch_time timestamp with time zone, OUT queue_external_ticker boolean, OUT queue_ticker_paused boolean, OUT queue_ticker_max_count integer, OUT queue_ticker_max_lag interval, OUT queue_ticker_idle_period interval, OUT ticker_lag interval, OUT ev_per_sec double precision, OUT ev_new bigint, OUT last_tick_id bigint) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.get_queue_info(0)
--
--      Get info about all queues.
--
-- Returns:
--      List of pgq.ret_queue_info records.
--     queue_name                  - queue name
--     queue_ntables               - number of tables in this queue
--     queue_cur_table             - ???
--     queue_rotation_period       - how often the event_N_M tables in this queue are rotated
--     queue_switch_time           - ??? when was this queue last rotated
--     queue_external_ticker       - ???
--     queue_ticker_paused         - ??? is ticker paused in this queue
--     queue_ticker_max_count      - max number of events before a tick is issued
--     queue_ticker_max_lag        - maks time without a tick
--     queue_ticker_idle_period    - how often the ticker should check this queue
--     ticker_lag                  - time from last tick
--     ev_per_sec                  - how many events per second this queue serves
--     ev_new                      - ???
--     last_tick_id                - last tick id for this queue
--
-- ----------------------------------------------------------------------
begin
    for queue_name, queue_ntables, queue_cur_table, queue_rotation_period,
        queue_switch_time, queue_external_ticker, queue_ticker_paused,
        queue_ticker_max_count, queue_ticker_max_lag, queue_ticker_idle_period,
        ticker_lag, ev_per_sec, ev_new, last_tick_id
    in select
        f.queue_name, f.queue_ntables, f.queue_cur_table, f.queue_rotation_period,
        f.queue_switch_time, f.queue_external_ticker, f.queue_ticker_paused,
        f.queue_ticker_max_count, f.queue_ticker_max_lag, f.queue_ticker_idle_period,
        f.ticker_lag, f.ev_per_sec, f.ev_new, f.last_tick_id
        from pgq.get_queue_info(null) f
    loop
        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION pgq.get_queue_info(OUT queue_name text, OUT queue_ntables integer, OUT queue_cur_table integer, OUT queue_rotation_period interval, OUT queue_switch_time timestamp with time zone, OUT queue_external_ticker boolean, OUT queue_ticker_paused boolean, OUT queue_ticker_max_count integer, OUT queue_ticker_max_lag interval, OUT queue_ticker_idle_period interval, OUT ticker_lag interval, OUT ev_per_sec double precision, OUT ev_new bigint, OUT last_tick_id bigint) OWNER TO postgres;

--
-- Name: get_queue_info(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION get_queue_info(i_queue_name text, OUT queue_name text, OUT queue_ntables integer, OUT queue_cur_table integer, OUT queue_rotation_period interval, OUT queue_switch_time timestamp with time zone, OUT queue_external_ticker boolean, OUT queue_ticker_paused boolean, OUT queue_ticker_max_count integer, OUT queue_ticker_max_lag interval, OUT queue_ticker_idle_period interval, OUT ticker_lag interval, OUT ev_per_sec double precision, OUT ev_new bigint, OUT last_tick_id bigint) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.get_queue_info(1)
--
--      Get info about particular queue.
--
-- Returns:
--      One pgq.ret_queue_info record.
--      contente same as forpgq.get_queue_info() 
-- ----------------------------------------------------------------------
declare
    _ticker_lag interval;
    _top_tick_id bigint;
    _ht_tick_id bigint;
    _top_tick_time timestamptz;
    _top_tick_event_seq bigint;
    _ht_tick_time timestamptz;
    _ht_tick_event_seq bigint;
    _queue_id integer;
    _queue_event_seq text;
begin
    for queue_name, queue_ntables, queue_cur_table, queue_rotation_period,
        queue_switch_time, queue_external_ticker, queue_ticker_paused,
        queue_ticker_max_count, queue_ticker_max_lag, queue_ticker_idle_period,
        _queue_id, _queue_event_seq
    in select
        q.queue_name, q.queue_ntables, q.queue_cur_table,
        q.queue_rotation_period, q.queue_switch_time,
        q.queue_external_ticker, q.queue_ticker_paused,
        q.queue_ticker_max_count, q.queue_ticker_max_lag,
        q.queue_ticker_idle_period,
        q.queue_id, q.queue_event_seq
        from pgq.queue q
        where (i_queue_name is null or q.queue_name = i_queue_name)
        order by q.queue_name
    loop
        -- most recent tick
        select (current_timestamp - t.tick_time),
               tick_id, t.tick_time, t.tick_event_seq
            into ticker_lag, _top_tick_id, _top_tick_time, _top_tick_event_seq
            from pgq.tick t
            where t.tick_queue = _queue_id
            order by t.tick_queue desc, t.tick_id desc
            limit 1;
        -- slightly older tick
        select ht.tick_id, ht.tick_time, ht.tick_event_seq
            into _ht_tick_id, _ht_tick_time, _ht_tick_event_seq
            from pgq.tick ht
            where ht.tick_queue = _queue_id
             and ht.tick_id >= _top_tick_id - 20
            order by ht.tick_queue asc, ht.tick_id asc
            limit 1;
        if _ht_tick_time < _top_tick_time then
            ev_per_sec = (_top_tick_event_seq - _ht_tick_event_seq) / extract(epoch from (_top_tick_time - _ht_tick_time));
        else
            ev_per_sec = null;
        end if;
        ev_new = pgq.seq_getval(_queue_event_seq) - _top_tick_event_seq;
        last_tick_id = _top_tick_id;
        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION pgq.get_queue_info(i_queue_name text, OUT queue_name text, OUT queue_ntables integer, OUT queue_cur_table integer, OUT queue_rotation_period interval, OUT queue_switch_time timestamp with time zone, OUT queue_external_ticker boolean, OUT queue_ticker_paused boolean, OUT queue_ticker_max_count integer, OUT queue_ticker_max_lag interval, OUT queue_ticker_idle_period interval, OUT ticker_lag interval, OUT ev_per_sec double precision, OUT ev_new bigint, OUT last_tick_id bigint) OWNER TO postgres;

--
-- Name: grant_perms(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION grant_perms(x_queue_name text) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.grant_perms(1)
--
--      Make event tables readable by public.
--
-- Parameters:
--      x_queue_name        - Name of the queue.
--
-- Returns:
--      nothing
-- ----------------------------------------------------------------------
declare
    q           record;
    i           integer;
    pos         integer;
    tbl_perms   text;
    seq_perms   text;
    dst_schema  text;
    dst_table   text;
    part_table  text;
begin
    select * from pgq.queue into q
        where queue_name = x_queue_name;
    if not found then
        raise exception 'Queue not found';
    end if;

    -- split data table name to components
    pos := position('.' in q.queue_data_pfx);
    if pos > 0 then
        dst_schema := substring(q.queue_data_pfx for pos - 1);
        dst_table := substring(q.queue_data_pfx from pos + 1);
    else
        dst_schema := 'public';
        dst_table := q.queue_data_pfx;
    end if;

    -- tick seq, normal users don't need to modify it
    execute 'grant select on ' || pgq.quote_fqname(q.queue_tick_seq) || ' to public';

    -- event seq
    execute 'grant select on ' || pgq.quote_fqname(q.queue_event_seq) || ' to public';
    execute 'grant usage on ' || pgq.quote_fqname(q.queue_event_seq) || ' to pgq_admin';

    -- set grants on parent table
    perform pgq._grant_perms_from('pgq', 'event_template', dst_schema, dst_table);

    -- set grants on real event tables
    for i in 0 .. q.queue_ntables - 1 loop
        part_table := dst_table  || '_' || i::text;
        perform pgq._grant_perms_from('pgq', 'event_template', dst_schema, part_table);
    end loop;

    return 1;
end;
$$;


ALTER FUNCTION pgq.grant_perms(x_queue_name text) OWNER TO postgres;

--
-- Name: insert_event(text, text, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION insert_event(queue_name text, ev_type text, ev_data text) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.insert_event(3)
--
--      Insert a event into queue.
--
-- Parameters:
--      queue_name      - Name of the queue
--      ev_type         - User-specified type for the event
--      ev_data         - User data for the event
--
-- Returns:
--      Event ID
-- Calls:
--      pgq.insert_event(7)
-- ----------------------------------------------------------------------
begin
    return pgq.insert_event(queue_name, ev_type, ev_data, null, null, null, null);
end;
$$;


ALTER FUNCTION pgq.insert_event(queue_name text, ev_type text, ev_data text) OWNER TO postgres;

--
-- Name: insert_event(text, text, text, text, text, text, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION insert_event(queue_name text, ev_type text, ev_data text, ev_extra1 text, ev_extra2 text, ev_extra3 text, ev_extra4 text) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.insert_event(7)
--
--      Insert a event into queue with all the extra fields.
--
-- Parameters:
--      queue_name      - Name of the queue
--      ev_type         - User-specified type for the event
--      ev_data         - User data for the event
--      ev_extra1       - Extra data field for the event
--      ev_extra2       - Extra data field for the event
--      ev_extra3       - Extra data field for the event
--      ev_extra4       - Extra data field for the event
--
-- Returns:
--      Event ID
-- Calls:
--      pgq.insert_event_raw(11)
-- Tables directly manipulated:
--      insert - pgq.insert_event_raw(11), a C function, inserts into current event_N_M table
-- ----------------------------------------------------------------------
begin
    return pgq.insert_event_raw(queue_name, null, now(), null, null,
            ev_type, ev_data, ev_extra1, ev_extra2, ev_extra3, ev_extra4);
end;
$$;


ALTER FUNCTION pgq.insert_event(queue_name text, ev_type text, ev_data text, ev_extra1 text, ev_extra2 text, ev_extra3 text, ev_extra4 text) OWNER TO postgres;

--
-- Name: insert_event_raw(text, bigint, timestamp with time zone, integer, integer, text, text, text, text, text, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION insert_event_raw(queue_name text, ev_id bigint, ev_time timestamp with time zone, ev_owner integer, ev_retry integer, ev_type text, ev_data text, ev_extra1 text, ev_extra2 text, ev_extra3 text, ev_extra4 text) RETURNS bigint
    LANGUAGE c
    AS '$libdir/pgq_lowlevel', 'pgq_insert_event_raw';


ALTER FUNCTION pgq.insert_event_raw(queue_name text, ev_id bigint, ev_time timestamp with time zone, ev_owner integer, ev_retry integer, ev_type text, ev_data text, ev_extra1 text, ev_extra2 text, ev_extra3 text, ev_extra4 text) OWNER TO postgres;

--
-- Name: logutriga(); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION logutriga() RETURNS trigger
    LANGUAGE c
    AS '$libdir/pgq_triggers', 'pgq_logutriga';


ALTER FUNCTION pgq.logutriga() OWNER TO postgres;

--
-- Name: maint_operations(); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION maint_operations(OUT func_name text, OUT func_arg text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.maint_operations(0)
--
--      Returns list of functions to call for maintenance.
--
--      The goal is to avoid hardcoding them into maintenance process.
--
-- Function signature:
--      Function should take either 1 or 0 arguments and return 1 if it wants
--      to be called immediately again, 0 if not.
--
-- Returns:
--      func_name   - Function to call
--      func_arg    - Optional argument to function (queue name)
-- ----------------------------------------------------------------------
declare
    ops text[];
    nrot int4;
begin
    -- rotate step 1
    nrot := 0;
    func_name := 'pgq.maint_rotate_tables_step1';
    for func_arg in
        select queue_name from pgq.queue
            where queue_rotation_period is not null
                and queue_switch_step2 is not null
                and queue_switch_time + queue_rotation_period < current_timestamp
            order by 1
    loop
        nrot := nrot + 1;
        return next;
    end loop;

    -- rotate step 2
    if nrot = 0 then
        select count(1) from pgq.queue
            where queue_rotation_period is not null
                and queue_switch_step2 is null
            into nrot;
    end if;
    if nrot > 0 then
        func_name := 'pgq.maint_rotate_tables_step2';
        func_arg := NULL;
        return next;
    end if;

    -- check if extra field exists
    perform 1 from pg_attribute
      where attrelid = 'pgq.queue'::regclass
        and attname = 'queue_extra_maint';
    if found then
        -- add extra ops
        for func_arg, ops in
            select q.queue_name, queue_extra_maint from pgq.queue q
             where queue_extra_maint is not null
             order by 1
        loop
            for i in array_lower(ops, 1) .. array_upper(ops, 1)
            loop
                func_name = ops[i];
                return next;
            end loop;
        end loop;
    end if;

    -- vacuum tables
    func_name := 'vacuum';
    for func_arg in
        select * from pgq.maint_tables_to_vacuum()
    loop
        return next;
    end loop;

    --
    -- pgq_node & londiste
    --
    -- although they belong to queue_extra_maint, they are
    -- common enough so its more effective to handle them here.
    --

    perform 1 from pg_proc p, pg_namespace n
      where p.pronamespace = n.oid
        and n.nspname = 'pgq_node'
        and p.proname = 'maint_watermark';
    if found then
        func_name := 'pgq_node.maint_watermark';
        for func_arg in
            select n.queue_name
              from pgq_node.node_info n
              where n.node_type = 'root'
        loop
            return next;
        end loop;

    end if;

    perform 1 from pg_proc p, pg_namespace n
      where p.pronamespace = n.oid
        and n.nspname = 'londiste'
        and p.proname = 'root_check_seqs';
    if found then
        func_name := 'londiste.root_check_seqs';
        for func_arg in
            select distinct s.queue_name
              from londiste.seq_info s, pgq_node.node_info n
              where s.local
                and n.node_type = 'root'
                and n.queue_name = s.queue_name
        loop
            return next;
        end loop;
    end if;

    perform 1 from pg_proc p, pg_namespace n
      where p.pronamespace = n.oid
        and n.nspname = 'londiste'
        and p.proname = 'periodic_maintenance';
    if found then
        func_name := 'londiste.periodic_maintenance';
        func_arg := NULL;
        return next;
    end if;

    return;
end;
$$;


ALTER FUNCTION pgq.maint_operations(OUT func_name text, OUT func_arg text) OWNER TO postgres;

--
-- Name: maint_retry_events(); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION maint_retry_events() RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.maint_retry_events(0)
--
--      Moves retry events back to main queue.
--
--      It moves small amount at a time.  It should be called
--      until it returns 0
--
-- Returns:
--      Number of events processed.
-- ----------------------------------------------------------------------
declare
    cnt    integer;
    rec    record;
begin
    cnt := 0;

    -- allow only single event mover at a time, without affecting inserts
    lock table pgq.retry_queue in share update exclusive mode;

    for rec in
        select queue_name,
               ev_id, ev_time, ev_owner, ev_retry, ev_type, ev_data,
               ev_extra1, ev_extra2, ev_extra3, ev_extra4
          from pgq.retry_queue, pgq.queue
         where ev_retry_after <= current_timestamp
           and queue_id = ev_queue
         order by ev_retry_after
         limit 10
    loop
        cnt := cnt + 1;
        perform pgq.insert_event_raw(rec.queue_name,
                    rec.ev_id, rec.ev_time, rec.ev_owner, rec.ev_retry,
                    rec.ev_type, rec.ev_data, rec.ev_extra1, rec.ev_extra2,
                    rec.ev_extra3, rec.ev_extra4);
        delete from pgq.retry_queue
         where ev_owner = rec.ev_owner
           and ev_id = rec.ev_id;
    end loop;
    return cnt;
end;
$$;


ALTER FUNCTION pgq.maint_retry_events() OWNER TO postgres;

--
-- Name: maint_rotate_tables_step1(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION maint_rotate_tables_step1(i_queue_name text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.maint_rotate_tables_step1(1)
--
--      Rotate tables for one queue.
--
-- Parameters:
--      i_queue_name        - Name of the queue
--
-- Returns:
--      0
-- ----------------------------------------------------------------------
declare
    badcnt          integer;
    cf              record;
    nr              integer;
    tbl             text;
    lowest_tick_id  int8;
    lowest_xmin     int8;
begin
    -- check if needed and load record
    select * from pgq.queue into cf
        where queue_name = i_queue_name
          and queue_rotation_period is not null
          and queue_switch_step2 is not null
          and queue_switch_time + queue_rotation_period < current_timestamp
        for update;
    if not found then
        return 0;
    end if;

    -- if DB is in invalid state, stop
    if txid_current() < cf.queue_switch_step1 then
        raise exception 'queue % maint failure: step1=%, current=%',
                i_queue_name, cf.queue_switch_step1, txid_current();
    end if;

    -- find lowest tick for that queue
    select min(sub_last_tick) into lowest_tick_id
      from pgq.subscription
     where sub_queue = cf.queue_id;

    -- if some consumer exists
    if lowest_tick_id is not null then
        -- is the slowest one still on previous table?
        select txid_snapshot_xmin(tick_snapshot) into lowest_xmin
          from pgq.tick
         where tick_queue = cf.queue_id
           and tick_id = lowest_tick_id;
        if not found then
            raise exception 'queue % maint failure: tick % not found', i_queue_name, lowest_tick_id;
        end if;
        if lowest_xmin <= cf.queue_switch_step2 then
            return 0; -- skip rotation then
        end if;
    end if;

    -- nobody on previous table, we can rotate
    
    -- calc next table number and name
    nr := cf.queue_cur_table + 1;
    if nr = cf.queue_ntables then
        nr := 0;
    end if;
    tbl := cf.queue_data_pfx || '_' || nr::text;

    -- there may be long lock on the table from pg_dump,
    -- detect it and skip rotate then
    begin
        execute 'lock table ' || pgq.quote_fqname(tbl) || ' nowait';
        execute 'truncate ' || pgq.quote_fqname(tbl);
    exception
        when lock_not_available then
            -- cannot truncate, skipping rotate
            return 0;
    end;

    -- remember the moment
    update pgq.queue
        set queue_cur_table = nr,
            queue_switch_time = current_timestamp,
            queue_switch_step1 = txid_current(),
            queue_switch_step2 = NULL
        where queue_id = cf.queue_id;

    -- Clean ticks by using step2 txid from previous rotation.
    -- That should keep all ticks for all batches that are completely
    -- in old table.  This keeps them for longer than needed, but:
    -- 1. we want the pgq.tick table to be big, to avoid Postgres
    --    accitentally switching to seqscans on that.
    -- 2. that way we guarantee to consumers that they an be moved
    --    back on the queue at least for one rotation_period.
    --    (may help in disaster recovery)
    delete from pgq.tick
        where tick_queue = cf.queue_id
          and txid_snapshot_xmin(tick_snapshot) < cf.queue_switch_step2;

    return 0;
end;
$$;


ALTER FUNCTION pgq.maint_rotate_tables_step1(i_queue_name text) OWNER TO postgres;

--
-- Name: maint_rotate_tables_step2(); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION maint_rotate_tables_step2() RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.maint_rotate_tables_step2(0)
--
--      Stores the txid when the rotation was visible.  It should be
--      called in separate transaction than pgq.maint_rotate_tables_step1()
-- ----------------------------------------------------------------------
begin
    update pgq.queue
       set queue_switch_step2 = txid_current()
     where queue_switch_step2 is null;
    return 0;
end;
$$;


ALTER FUNCTION pgq.maint_rotate_tables_step2() OWNER TO postgres;

--
-- Name: maint_tables_to_vacuum(); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION maint_tables_to_vacuum() RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.maint_tables_to_vacuum(0)
--
--      Returns list of tablenames that need frequent vacuuming.
--
--      The goal is to avoid hardcoding them into maintenance process.
--
-- Returns:
--      List of table names.
-- ----------------------------------------------------------------------
declare
    scm text;
    tbl text;
    fqname text;
begin
    -- assume autovacuum handles them fine
    if current_setting('autovacuum') = 'on' then
        return;
    end if;

    for scm, tbl in values
        ('pgq', 'subscription'),
        ('pgq', 'consumer'),
        ('pgq', 'queue'),
        ('pgq', 'tick'),
        ('pgq', 'retry_queue'),
        ('pgq_ext', 'completed_tick'),
        ('pgq_ext', 'completed_batch'),
        ('pgq_ext', 'completed_event'),
        ('pgq_ext', 'partial_batch'),
        --('pgq_node', 'node_location'),
        --('pgq_node', 'node_info'),
        ('pgq_node', 'local_state'),
        --('pgq_node', 'subscriber_info'),
        --('londiste', 'table_info'),
        ('londiste', 'seq_info'),
        --('londiste', 'applied_execute'),
        --('londiste', 'pending_fkeys'),
        ('txid', 'epoch'),
        ('londiste', 'completed')
    loop
        select n.nspname || '.' || t.relname into fqname
            from pg_class t, pg_namespace n
            where n.oid = t.relnamespace
                and n.nspname = scm
                and t.relname = tbl;
        if found then
            return next fqname;
        end if;
    end loop;
    return;
end;
$$;


ALTER FUNCTION pgq.maint_tables_to_vacuum() OWNER TO postgres;

--
-- Name: next_batch(text, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION next_batch(i_queue_name text, i_consumer_name text) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.next_batch(2)
--
--      Old function that returns just batch_id.
--
-- Parameters:
--      i_queue_name        - Name of the queue
--      i_consumer_name     - Name of the consumer
--
-- Returns:
--      Batch ID or NULL if there are no more events available.
-- ----------------------------------------------------------------------
declare
    res int8;
begin
    select batch_id into res
        from pgq.next_batch_info(i_queue_name, i_consumer_name);
    return res;
end;
$$;


ALTER FUNCTION pgq.next_batch(i_queue_name text, i_consumer_name text) OWNER TO postgres;

--
-- Name: next_batch_custom(text, text, interval, integer, interval); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION next_batch_custom(i_queue_name text, i_consumer_name text, i_min_lag interval, i_min_count integer, i_min_interval interval, OUT batch_id bigint, OUT cur_tick_id bigint, OUT prev_tick_id bigint, OUT cur_tick_time timestamp with time zone, OUT prev_tick_time timestamp with time zone, OUT cur_tick_event_seq bigint, OUT prev_tick_event_seq bigint) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.next_batch_custom(5)
--
--      Makes next block of events active.  Block size can be tuned
--      with i_min_count, i_min_interval parameters.  Events age can
--      be tuned with i_min_lag.
--
--      If it returns NULL, there is no events available in queue.
--      Consumer should sleep then.
--
--      The values from event_id sequence may give hint how big the
--      batch may be.  But they are inexact, they do not give exact size.
--      Client *MUST NOT* use them to detect whether the batch contains any
--      events at all - the values are unfit for that purpose.
--
-- Note:
--      i_min_lag together with i_min_interval/i_min_count is inefficient.
--
-- Parameters:
--      i_queue_name        - Name of the queue
--      i_consumer_name     - Name of the consumer
--      i_min_lag           - Consumer wants events older than that
--      i_min_count         - Consumer wants batch to contain at least this many events
--      i_min_interval      - Consumer wants batch to cover at least this much time
--
-- Returns:
--      batch_id            - Batch ID or NULL if there are no more events available.
--      cur_tick_id         - End tick id.
--      cur_tick_time       - End tick time.
--      cur_tick_event_seq  - Value from event id sequence at the time tick was issued.
--      prev_tick_id        - Start tick id.
--      prev_tick_time      - Start tick time.
--      prev_tick_event_seq - value from event id sequence at the time tick was issued.
-- Calls:
--      pgq.insert_event_raw(11)
-- Tables directly manipulated:
--      update - pgq.subscription
-- ----------------------------------------------------------------------
declare
    errmsg          text;
    queue_id        integer;
    sub_id          integer;
    cons_id         integer;
begin
    select s.sub_queue, s.sub_consumer, s.sub_id, s.sub_batch,
            t1.tick_id, t1.tick_time, t1.tick_event_seq,
            t2.tick_id, t2.tick_time, t2.tick_event_seq
        into queue_id, cons_id, sub_id, batch_id,
             prev_tick_id, prev_tick_time, prev_tick_event_seq,
             cur_tick_id, cur_tick_time, cur_tick_event_seq
        from pgq.consumer c,
             pgq.queue q,
             pgq.subscription s
             left join pgq.tick t1
                on (t1.tick_queue = s.sub_queue
                    and t1.tick_id = s.sub_last_tick)
             left join pgq.tick t2
                on (t2.tick_queue = s.sub_queue
                    and t2.tick_id = s.sub_next_tick)
        where q.queue_name = i_queue_name
          and c.co_name = i_consumer_name
          and s.sub_queue = q.queue_id
          and s.sub_consumer = c.co_id;
    if not found then
        errmsg := 'Not subscriber to queue: '
            || coalesce(i_queue_name, 'NULL')
            || '/'
            || coalesce(i_consumer_name, 'NULL');
        raise exception '%', errmsg;
    end if;

    -- sanity check
    if prev_tick_id is null then
        raise exception 'PgQ corruption: Consumer % on queue % does not see tick %', i_consumer_name, i_queue_name, prev_tick_id;
    end if;

    -- has already active batch
    if batch_id is not null then
        return;
    end if;

    if i_min_interval is null and i_min_count is null then
        -- find next tick
        select tick_id, tick_time, tick_event_seq
            into cur_tick_id, cur_tick_time, cur_tick_event_seq
            from pgq.tick
            where tick_id > prev_tick_id
              and tick_queue = queue_id
            order by tick_queue asc, tick_id asc
            limit 1;
    else
        -- find custom tick
        select next_tick_id, next_tick_time, next_tick_seq
          into cur_tick_id, cur_tick_time, cur_tick_event_seq
          from pgq.find_tick_helper(queue_id, prev_tick_id,
                                    prev_tick_time, prev_tick_event_seq,
                                    i_min_count, i_min_interval);
    end if;

    if i_min_lag is not null then
        -- enforce min lag
        if now() - cur_tick_time < i_min_lag then
            cur_tick_id := NULL;
            cur_tick_time := NULL;
            cur_tick_event_seq := NULL;
        end if;
    end if;

    if cur_tick_id is null then
        -- nothing to do
        prev_tick_id := null;
        prev_tick_time := null;
        prev_tick_event_seq := null;
        return;
    end if;

    -- get next batch
    batch_id := nextval('pgq.batch_id_seq');
    update pgq.subscription
        set sub_batch = batch_id,
            sub_next_tick = cur_tick_id,
            sub_active = now()
        where sub_queue = queue_id
          and sub_consumer = cons_id;
    return;
end;
$$;


ALTER FUNCTION pgq.next_batch_custom(i_queue_name text, i_consumer_name text, i_min_lag interval, i_min_count integer, i_min_interval interval, OUT batch_id bigint, OUT cur_tick_id bigint, OUT prev_tick_id bigint, OUT cur_tick_time timestamp with time zone, OUT prev_tick_time timestamp with time zone, OUT cur_tick_event_seq bigint, OUT prev_tick_event_seq bigint) OWNER TO postgres;

--
-- Name: next_batch_info(text, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION next_batch_info(i_queue_name text, i_consumer_name text, OUT batch_id bigint, OUT cur_tick_id bigint, OUT prev_tick_id bigint, OUT cur_tick_time timestamp with time zone, OUT prev_tick_time timestamp with time zone, OUT cur_tick_event_seq bigint, OUT prev_tick_event_seq bigint) RETURNS record
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.next_batch_info(2)
--
--      Makes next block of events active.
--
--      If it returns NULL, there is no events available in queue.
--      Consumer should sleep then.
--
--      The values from event_id sequence may give hint how big the
--      batch may be.  But they are inexact, they do not give exact size.
--      Client *MUST NOT* use them to detect whether the batch contains any
--      events at all - the values are unfit for that purpose.
--
-- Parameters:
--      i_queue_name        - Name of the queue
--      i_consumer_name     - Name of the consumer
--
-- Returns:
--      batch_id            - Batch ID or NULL if there are no more events available.
--      cur_tick_id         - End tick id.
--      cur_tick_time       - End tick time.
--      cur_tick_event_seq  - Value from event id sequence at the time tick was issued.
--      prev_tick_id        - Start tick id.
--      prev_tick_time      - Start tick time.
--      prev_tick_event_seq - value from event id sequence at the time tick was issued.
-- Calls:
--      pgq.next_batch_custom(5)
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
begin
    select f.batch_id, f.cur_tick_id, f.prev_tick_id,
           f.cur_tick_time, f.prev_tick_time,
           f.cur_tick_event_seq, f.prev_tick_event_seq
        into batch_id, cur_tick_id, prev_tick_id, cur_tick_time, prev_tick_time,
             cur_tick_event_seq, prev_tick_event_seq
        from pgq.next_batch_custom(i_queue_name, i_consumer_name, NULL, NULL, NULL) f;
    return;
end;
$$;


ALTER FUNCTION pgq.next_batch_info(i_queue_name text, i_consumer_name text, OUT batch_id bigint, OUT cur_tick_id bigint, OUT prev_tick_id bigint, OUT cur_tick_time timestamp with time zone, OUT prev_tick_time timestamp with time zone, OUT cur_tick_event_seq bigint, OUT prev_tick_event_seq bigint) OWNER TO postgres;

--
-- Name: quote_fqname(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION quote_fqname(i_name text) RETURNS text
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.quote_fqname(1)
--
--      Quete fully-qualified object name for SQL.
--
--      First dot is taken as schema separator.
--
--      If schema is missing, 'public' is assumed.
--
-- Parameters:
--      i_name  - fully qualified object name.
--
-- Returns:
--      Quoted name.
-- ----------------------------------------------------------------------
declare
    res     text;
    pos     integer;
    s       text;
    n       text;
begin
    pos := position('.' in i_name);
    if pos > 0 then
        s := substring(i_name for pos - 1);
        n := substring(i_name from pos + 1);
    else
        s := 'public';
        n := i_name;
    end if;
    return quote_ident(s) || '.' || quote_ident(n);
end;
$$;


ALTER FUNCTION pgq.quote_fqname(i_name text) OWNER TO postgres;

--
-- Name: register_consumer(text, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION register_consumer(x_queue_name text, x_consumer_id text) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.register_consumer(2)
--
--      Subscribe consumer on a queue.
--
--      From this moment forward, consumer will see all events in the queue.
--
-- Parameters:
--      x_queue_name        - Name of queue
--      x_consumer_name     - Name of consumer
--
-- Returns:
--      0  - if already registered
--      1  - if new registration
-- Calls:
--      pgq.register_consumer_at(3)
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
begin
    return pgq.register_consumer_at(x_queue_name, x_consumer_id, NULL);
end;
$$;


ALTER FUNCTION pgq.register_consumer(x_queue_name text, x_consumer_id text) OWNER TO postgres;

--
-- Name: register_consumer_at(text, text, bigint); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION register_consumer_at(x_queue_name text, x_consumer_name text, x_tick_pos bigint) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.register_consumer_at(3)
--
--      Extended registration, allows to specify tick_id.
--
-- Note:
--      For usage in special situations.
--
-- Parameters:
--      x_queue_name        - Name of a queue
--      x_consumer_name     - Name of consumer
--      x_tick_pos          - Tick ID
--
-- Returns:
--      0/1 whether consumer has already registered.
-- Calls:
--      None
-- Tables directly manipulated:
--      update/insert - pgq.subscription
-- ----------------------------------------------------------------------
declare
    tmp         text;
    last_tick   bigint;
    x_queue_id  integer;
    x_consumer_id integer;
    queue integer;
    sub record;
begin
    select queue_id into x_queue_id from pgq.queue
        where queue_name = x_queue_name;
    if not found then
        raise exception 'Event queue not created yet';
    end if;

    -- get consumer and create if new
    select co_id into x_consumer_id from pgq.consumer
        where co_name = x_consumer_name
        for update;
    if not found then
        insert into pgq.consumer (co_name) values (x_consumer_name);
        x_consumer_id := currval('pgq.consumer_co_id_seq');
    end if;

    -- if particular tick was requested, check if it exists
    if x_tick_pos is not null then
        perform 1 from pgq.tick
            where tick_queue = x_queue_id
              and tick_id = x_tick_pos;
        if not found then
            raise exception 'cannot reposition, tick not found: %', x_tick_pos;
        end if;
    end if;

    -- check if already registered
    select sub_last_tick, sub_batch into sub
        from pgq.subscription
        where sub_consumer = x_consumer_id
          and sub_queue  = x_queue_id;
    if found then
        if x_tick_pos is not null then
            -- if requested, update tick pos and drop partial batch
            update pgq.subscription
                set sub_last_tick = x_tick_pos,
                    sub_batch = null,
                    sub_next_tick = null,
                    sub_active = now()
                where sub_consumer = x_consumer_id
                  and sub_queue = x_queue_id;
        end if;
        -- already registered
        return 0;
    end if;

    --  new registration
    if x_tick_pos is null then
        -- start from current tick
        select tick_id into last_tick from pgq.tick
            where tick_queue = x_queue_id
            order by tick_queue desc, tick_id desc
            limit 1;
        if not found then
            raise exception 'No ticks for this queue.  Please run ticker on database.';
        end if;
    else
        last_tick := x_tick_pos;
    end if;

    -- register
    insert into pgq.subscription (sub_queue, sub_consumer, sub_last_tick)
        values (x_queue_id, x_consumer_id, last_tick);
    return 1;
end;
$$;


ALTER FUNCTION pgq.register_consumer_at(x_queue_name text, x_consumer_name text, x_tick_pos bigint) OWNER TO postgres;

--
-- Name: seq_getval(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION seq_getval(i_seq_name text) RETURNS bigint
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.seq_getval(1)
--
--      Read current last_val from seq, without affecting it.
--
-- Parameters:
--      i_seq_name     - Name of the sequence
--
-- Returns:
--      last value.
-- ----------------------------------------------------------------------
declare
    res     int8;
    fqname  text;
    pos     integer;
    s       text;
    n       text;
begin
    pos := position('.' in i_seq_name);
    if pos > 0 then
        s := substring(i_seq_name for pos - 1);
        n := substring(i_seq_name from pos + 1);
    else
        s := 'public';
        n := i_seq_name;
    end if;
    fqname := quote_ident(s) || '.' || quote_ident(n);

    execute 'select last_value from ' || fqname into res;
    return res;
end;
$$;


ALTER FUNCTION pgq.seq_getval(i_seq_name text) OWNER TO postgres;

--
-- Name: seq_setval(text, bigint); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION seq_setval(i_seq_name text, i_new_value bigint) RETURNS bigint
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.seq_setval(2)
--
--      Like setval() but does not allow going back.
--
-- Parameters:
--      i_seq_name      - Name of the sequence
--      i_new_value     - new value
--
-- Returns:
--      current last value.
-- ----------------------------------------------------------------------
declare
    res     int8;
    fqname  text;
begin
    fqname := pgq.quote_fqname(i_seq_name);

    res := pgq.seq_getval(i_seq_name);
    if res < i_new_value then
        perform setval(fqname, i_new_value);
        return i_new_value;
    end if;
    return res;
end;
$$;


ALTER FUNCTION pgq.seq_setval(i_seq_name text, i_new_value bigint) OWNER TO postgres;

--
-- Name: set_queue_config(text, text, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION set_queue_config(x_queue_name text, x_param_name text, x_param_value text) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.set_queue_config(3)
--
--
--     Set configuration for specified queue.
--
-- Parameters:
--      x_queue_name    - Name of the queue to configure.
--      x_param_name    - Configuration parameter name.
--      x_param_value   - Configuration parameter value.
--  
-- Returns:
--     0 if event was already in queue, 1 otherwise.
-- Calls:
--      None
-- Tables directly manipulated:
--      update - pgq.queue
-- ----------------------------------------------------------------------
declare
    v_param_name    text;
begin
    -- discard NULL input
    if x_queue_name is null or x_param_name is null then
        raise exception 'Invalid NULL value';
    end if;

    -- check if queue exists
    perform 1 from pgq.queue where queue_name = x_queue_name;
    if not found then
        raise exception 'No such event queue';
    end if;

    -- check if valid parameter name
    v_param_name := 'queue_' || x_param_name;
    if v_param_name not in (
        'queue_ticker_max_count',
        'queue_ticker_max_lag',
        'queue_ticker_idle_period',
        'queue_ticker_paused',
        'queue_rotation_period',
        'queue_external_ticker')
    then
        raise exception 'cannot change parameter "%s"', x_param_name;
    end if;

    execute 'update pgq.queue set ' 
        || v_param_name || ' = ' || quote_literal(x_param_value)
        || ' where queue_name = ' || quote_literal(x_queue_name);

    return 1;
end;
$$;


ALTER FUNCTION pgq.set_queue_config(x_queue_name text, x_param_name text, x_param_value text) OWNER TO postgres;

--
-- Name: sqltriga(); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION sqltriga() RETURNS trigger
    LANGUAGE c
    AS '$libdir/pgq_triggers', 'pgq_sqltriga';


ALTER FUNCTION pgq.sqltriga() OWNER TO postgres;

--
-- Name: ticker(); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION ticker() RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.ticker(0)
--
--     Creates ticks for all unpaused queues which dont have external ticker.
--
-- Returns:
--     Number of queues that were processed.
-- ----------------------------------------------------------------------
declare
    res bigint;
    q record;
begin
    res := 0;
    for q in
        select queue_name from pgq.queue
            where not queue_external_ticker
                  and not queue_ticker_paused
            order by queue_name
    loop
        if pgq.ticker(q.queue_name) > 0 then
            res := res + 1;
        end if;
    end loop;
    return res;
end;
$$;


ALTER FUNCTION pgq.ticker() OWNER TO postgres;

--
-- Name: ticker(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION ticker(i_queue_name text) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.ticker(1)
--
--     Check if tick is needed for the queue and insert it.
--
--     For pgqadm usage.
--
-- Parameters:
--     i_queue_name     - Name of the queue
--
-- Returns:
--     Tick id or NULL if no tick was done.
-- ----------------------------------------------------------------------
declare
    res bigint;
    q record;
    state record;
    last2 record;
begin
    select queue_id, queue_tick_seq, queue_external_ticker,
            queue_ticker_max_count, queue_ticker_max_lag,
            queue_ticker_idle_period, queue_event_seq,
            pgq.seq_getval(queue_event_seq) as event_seq,
            queue_ticker_paused
        into q
        from pgq.queue where queue_name = i_queue_name;
    if not found then
        raise exception 'no such queue';
    end if;

    if q.queue_external_ticker then
        raise exception 'This queue has external tick source.';
    end if;

    if q.queue_ticker_paused then
        raise exception 'Ticker has been paused for this queue';
    end if;

    -- load state from last tick
    select now() - tick_time as lag,
           q.event_seq - tick_event_seq as new_events,
           tick_id, tick_time, tick_event_seq,
           txid_snapshot_xmax(tick_snapshot) as sxmax,
           txid_snapshot_xmin(tick_snapshot) as sxmin
        into state
        from pgq.tick
        where tick_queue = q.queue_id
        order by tick_queue desc, tick_id desc
        limit 1;

    if found then
        if state.sxmin > txid_current() then
            raise exception 'Invalid PgQ state: old xmin=%, old xmax=%, cur txid=%',
                            state.sxmin, state.sxmax, txid_current();
        end if;
        if state.new_events < 0 then
            raise warning 'Negative new_events?  old=% cur=%', state.tick_event_seq, q.event_seq;
        end if;
        if state.sxmax > txid_current() then
            raise warning 'Dubious PgQ state: old xmax=%, cur txid=%', state.sxmax, txid_current();
        end if;

        if state.new_events > 0 then
            -- there are new events, should we wait a bit?
            if state.new_events < q.queue_ticker_max_count
                and state.lag < q.queue_ticker_max_lag
            then
                return NULL;
            end if;
        else
            -- no new events, should we apply idle period?
            -- check previous event from the last one.
            select state.tick_time - tick_time as lag
                into last2
                from pgq.tick
                where tick_queue = q.queue_id
                    and tick_id < state.tick_id
                order by tick_queue desc, tick_id desc
                limit 1;
            if found then
                -- gradually decrease the tick frequency
                if (state.lag < q.queue_ticker_max_lag / 2)
                    or
                   (state.lag < last2.lag * 2
                    and state.lag < q.queue_ticker_idle_period)
                then
                    return NULL;
                end if;
            end if;
        end if;
    end if;

    insert into pgq.tick (tick_queue, tick_id, tick_event_seq)
        values (q.queue_id, nextval(q.queue_tick_seq), q.event_seq);

    return currval(q.queue_tick_seq);
end;
$$;


ALTER FUNCTION pgq.ticker(i_queue_name text) OWNER TO postgres;

--
-- Name: ticker(text, bigint, timestamp with time zone, bigint); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION ticker(i_queue_name text, i_tick_id bigint, i_orig_timestamp timestamp with time zone, i_event_seq bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.ticker(3)
--
--     External ticker: Insert a tick with a particular tick_id and timestamp.
--
-- Parameters:
--     i_queue_name     - Name of the queue
--     i_tick_id        - Id of new tick.
--
-- Returns:
--     Tick id.
-- ----------------------------------------------------------------------
begin
    insert into pgq.tick (tick_queue, tick_id, tick_time, tick_event_seq)
    select queue_id, i_tick_id, i_orig_timestamp, i_event_seq
        from pgq.queue
        where queue_name = i_queue_name
          and queue_external_ticker
          and not queue_ticker_paused;
    if not found then
        raise exception 'queue not found or ticker disabled: %', i_queue_name;
    end if;

    -- make sure seqs stay current
    perform pgq.seq_setval(queue_tick_seq, i_tick_id),
            pgq.seq_setval(queue_event_seq, i_event_seq)
        from pgq.queue
        where queue_name = i_queue_name;

    return i_tick_id;
end;
$$;


ALTER FUNCTION pgq.ticker(i_queue_name text, i_tick_id bigint, i_orig_timestamp timestamp with time zone, i_event_seq bigint) OWNER TO postgres;

--
-- Name: tune_storage(text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION tune_storage(i_queue_name text) RETURNS integer
    LANGUAGE plpgsql STRICT
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.tune_storage(1)
--
--      Tunes storage settings for queue data tables
-- ----------------------------------------------------------------------
declare
    tbl  text;
    tbloid oid;
    q record;
    i int4;
    sql text;
    pgver int4;
begin
    pgver := current_setting('server_version_num');

    select * into q
      from pgq.queue where queue_name = i_queue_name;
    if not found then
        return 0;
    end if;

    for i in 0 .. (q.queue_ntables - 1) loop
        tbl := q.queue_data_pfx || '_' || i::text;

        -- set fillfactor
        sql := 'alter table ' || tbl || ' set (fillfactor = 100';

        -- autovacuum for 8.4+
        if pgver >= 80400 then
            sql := sql || ', autovacuum_enabled=off, toast.autovacuum_enabled =off';
        end if;
        sql := sql || ')';
        execute sql;

        -- autovacuum for 8.3
        if pgver < 80400 then
            tbloid := tbl::regclass::oid;
            delete from pg_catalog.pg_autovacuum where vacrelid = tbloid;
            insert into pg_catalog.pg_autovacuum values (tbloid, false, -1,-1,-1,-1,-1,-1,-1,-1);
        end if;
    end loop;

    return 1;
end;
$$;


ALTER FUNCTION pgq.tune_storage(i_queue_name text) OWNER TO postgres;

--
-- Name: unregister_consumer(text, text); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION unregister_consumer(x_queue_name text, x_consumer_name text) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.unregister_consumer(2)
--
--      Unsubscribe consumer from the queue.
--      Also consumer's retry events are deleted.
--
-- Parameters:
--      x_queue_name        - Name of the queue
--      x_consumer_name     - Name of the consumer
--
-- Returns:
--      number of (sub)consumers unregistered
-- Calls:
--      None
-- Tables directly manipulated:
--      delete - pgq.retry_queue
--      delete - pgq.subscription
-- ----------------------------------------------------------------------
declare
    x_sub_id integer;
    _sub_id_cnt integer;
    _consumer_id integer;
    _is_subconsumer boolean;
begin
    select s.sub_id, c.co_id,
           -- subconsumers can only have both null or both not null - main consumer for subconsumers has only one not null
           (s.sub_last_tick IS NULL AND s.sub_next_tick IS NULL) OR (s.sub_last_tick IS NOT NULL AND s.sub_next_tick IS NOT NULL)
      into x_sub_id, _consumer_id, _is_subconsumer
      from pgq.subscription s, pgq.consumer c, pgq.queue q
     where s.sub_queue = q.queue_id
       and s.sub_consumer = c.co_id
       and q.queue_name = x_queue_name
       and c.co_name = x_consumer_name
       for update of s, c;
    if not found then
        return 0;
    end if;

    -- consumer + subconsumer count
    select count(*) into _sub_id_cnt
        from pgq.subscription
       where sub_id = x_sub_id;

    -- delete only one subconsumer
    if _sub_id_cnt > 1 and _is_subconsumer then
        delete from pgq.subscription
              where sub_id = x_sub_id
                and sub_consumer = _consumer_id;
        return 1;
    else
        -- delete main consumer (including possible subconsumers)

        -- retry events
        delete from pgq.retry_queue
            where ev_owner = x_sub_id;

        -- this will drop subconsumers too
        delete from pgq.subscription
            where sub_id = x_sub_id;

        perform 1 from pgq.subscription
            where sub_consumer = _consumer_id;
        if not found then
            delete from pgq.consumer
                where co_id = _consumer_id;
        end if;

        return _sub_id_cnt;
    end if;

end;
$$;


ALTER FUNCTION pgq.unregister_consumer(x_queue_name text, x_consumer_name text) OWNER TO postgres;

--
-- Name: upgrade_schema(); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION upgrade_schema() RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- updates table structure if necessary
declare
    cnt int4 = 0;
begin

    -- pgq.subscription.sub_last_tick: NOT NULL -> NULL
    perform 1 from information_schema.columns
      where table_schema = 'pgq'
        and table_name = 'subscription'
        and column_name ='sub_last_tick'
        and is_nullable = 'NO';
    if found then
        alter table pgq.subscription
            alter column sub_last_tick
            drop not null;
        cnt := cnt + 1;
    end if;

    -- create roles
    perform 1 from pg_catalog.pg_roles where rolname = 'pgq_reader';
    if not found then
        create role pgq_reader;
        cnt := cnt + 1;
    end if;
    perform 1 from pg_catalog.pg_roles where rolname = 'pgq_writer';
    if not found then
        create role pgq_writer;
        cnt := cnt + 1;
    end if;
    perform 1 from pg_catalog.pg_roles where rolname = 'pgq_admin';
    if not found then
        create role pgq_admin in role pgq_reader, pgq_writer;
        cnt := cnt + 1;
    end if;

    return cnt;
end;
$$;


ALTER FUNCTION pgq.upgrade_schema() OWNER TO postgres;

--
-- Name: version(); Type: FUNCTION; Schema: pgq; Owner: postgres
--

CREATE FUNCTION version() RETURNS text
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq.version(0)
--
--      Returns version string for pgq.  ATM it is based on SkyTools
--      version and only bumped when database code changes.
-- ----------------------------------------------------------------------
begin
    return '3.2.6';
end;
$$;


ALTER FUNCTION pgq.version() OWNER TO postgres;

SET search_path = pgq_ext, pg_catalog;

--
-- Name: get_last_tick(text); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION get_last_tick(a_consumer text) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.get_last_tick(1)
--
--	Gets last completed tick for this consumer 
--
-- Parameters:
--      a_consumer - consumer name
--
-- Returns:
--	    tick_id - last completed tick 
-- Calls:
--      pgq_ext.get_last_tick(2)
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
begin
    return pgq_ext.get_last_tick(a_consumer, '');
end;
$$;


ALTER FUNCTION pgq_ext.get_last_tick(a_consumer text) OWNER TO postgres;

--
-- Name: get_last_tick(text, text); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION get_last_tick(a_consumer text, a_subconsumer text) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.get_last_tick(2)
--
--	Gets last completed tick for this consumer 
--
-- Parameters:
--      a_consumer - consumer name
--      a_subconsumer - subconsumer name
--
-- Returns:
--	    tick_id - last completed tick 
-- Calls:
--      None
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
declare
    res   int8;
begin
    select last_tick_id into res
      from pgq_ext.completed_tick
     where consumer_id = a_consumer
       and subconsumer_id = a_subconsumer;
    return res;
end;
$$;


ALTER FUNCTION pgq_ext.get_last_tick(a_consumer text, a_subconsumer text) OWNER TO postgres;

--
-- Name: is_batch_done(text, bigint); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION is_batch_done(a_consumer text, a_batch_id bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.is_batch_done(2)
--
--	    Checks if a certain consumer has completed the batch 
--
-- Parameters:
--      a_consumer - consumer name
--      a_batch_id - a batch id
--
-- Returns:
--	    true if batch is done, else false 
-- Calls:
--      pgq_ext.is_batch_done(3)
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
begin
    return pgq_ext.is_batch_done(a_consumer, '', a_batch_id);
end;
$$;


ALTER FUNCTION pgq_ext.is_batch_done(a_consumer text, a_batch_id bigint) OWNER TO postgres;

--
-- Name: is_batch_done(text, text, bigint); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION is_batch_done(a_consumer text, a_subconsumer text, a_batch_id bigint) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.is_batch_done(3)
--
--	    Checks if a certain consumer and subconsumer have completed the batch 
--
-- Parameters:
--      a_consumer - consumer name
--      a_subconsumer - subconsumer name
--      a_batch_id - a batch id
--
-- Returns:
--	    true if batch is done, else false 
-- Calls:
--      None
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
declare
    res   boolean;
begin
    select last_batch_id = a_batch_id
      into res from pgq_ext.completed_batch
     where consumer_id = a_consumer
       and subconsumer_id = a_subconsumer;
    if not found then
        return false;
    end if;
    return res;
end;
$$;


ALTER FUNCTION pgq_ext.is_batch_done(a_consumer text, a_subconsumer text, a_batch_id bigint) OWNER TO postgres;

--
-- Name: is_event_done(text, bigint, bigint); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION is_event_done(a_consumer text, a_batch_id bigint, a_event_id bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.is_event_done(3)
--
--	    Checks if a certain consumer has "done" and event
--      in a batch  
--
-- Parameters:
--      a_consumer - consumer name
--      a_batch_id - a batch id
--      a_event_id - an event id
--
-- Returns:
--	    true if event is done, else false 
-- Calls:
--      Nonpgq_ext.is_event_done(4)
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
begin
    return pgq_ext.is_event_done(a_consumer, '', a_batch_id, a_event_id);
end;
$$;


ALTER FUNCTION pgq_ext.is_event_done(a_consumer text, a_batch_id bigint, a_event_id bigint) OWNER TO postgres;

--
-- Name: is_event_done(text, text, bigint, bigint); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION is_event_done(a_consumer text, a_subconsumer text, a_batch_id bigint, a_event_id bigint) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.is_event_done(4)
--
--	    Checks if a certain consumer and subconsumer have "done" and event
--      in a batch  
--
-- Parameters:
--      a_consumer - consumer name
--      a_subconsumer - subconsumer name
--      a_batch_id - a batch id
--      a_event_id - an event id
--
-- Returns:
--	    true if event is done, else false 
-- Calls:
--      None
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
declare
    res   bigint;
begin
    perform 1 from pgq_ext.completed_event
     where consumer_id = a_consumer
       and subconsumer_id = a_subconsumer
       and batch_id = a_batch_id
       and event_id = a_event_id;
    return found;
end;
$$;


ALTER FUNCTION pgq_ext.is_event_done(a_consumer text, a_subconsumer text, a_batch_id bigint, a_event_id bigint) OWNER TO postgres;

--
-- Name: set_batch_done(text, bigint); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION set_batch_done(a_consumer text, a_batch_id bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.set_batch_done(3)
--
--	    Marks a batch as "done"  for certain consumer 
--
-- Parameters:
--      a_consumer - consumer name
--      a_batch_id - a batch id
--
-- Returns:
--      false if it already was done
--	    true for successfully marking it as done 
-- Calls:
--      pgq_ext.set_batch_done(3)
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
begin
    return pgq_ext.set_batch_done(a_consumer, '', a_batch_id);
end;
$$;


ALTER FUNCTION pgq_ext.set_batch_done(a_consumer text, a_batch_id bigint) OWNER TO postgres;

--
-- Name: set_batch_done(text, text, bigint); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION set_batch_done(a_consumer text, a_subconsumer text, a_batch_id bigint) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.set_batch_done(3)
--
--	    Marks a batch as "done"  for certain consumer and subconsumer 
--
-- Parameters:
--      a_consumer - consumer name
--      a_subconsumer - subconsumer name
--      a_batch_id - a batch id
--
-- Returns:
--      false if it already was done
--	    true for successfully marking it as done 
-- Calls:
--      None
-- Tables directly manipulated:
--      update - pgq_ext.completed_batch
-- ----------------------------------------------------------------------
begin
    if pgq_ext.is_batch_done(a_consumer, a_subconsumer, a_batch_id) then
        return false;
    end if;

    if a_batch_id > 0 then
        update pgq_ext.completed_batch
           set last_batch_id = a_batch_id
         where consumer_id = a_consumer
           and subconsumer_id = a_subconsumer;
        if not found then
            insert into pgq_ext.completed_batch (consumer_id, subconsumer_id, last_batch_id)
                values (a_consumer, a_subconsumer, a_batch_id);
        end if;
    end if;

    return true;
end;
$$;


ALTER FUNCTION pgq_ext.set_batch_done(a_consumer text, a_subconsumer text, a_batch_id bigint) OWNER TO postgres;

--
-- Name: set_event_done(text, bigint, bigint); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION set_event_done(a_consumer text, a_batch_id bigint, a_event_id bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.set_event_done(3)
--
--	    Marks and event done in a batch for a certain consumer and subconsumer
--
-- Parameters:
--      a_consumer - consumer name
--      a_batch_id - a batch id
--      a_event_id - an event id
--
-- Returns:
--      false if already done
--	    true on success 
-- Calls:
--      pgq_ext.set_event_done(4)
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
begin
    return pgq_ext.set_event_done(a_consumer, '', a_batch_id, a_event_id);
end;
$$;


ALTER FUNCTION pgq_ext.set_event_done(a_consumer text, a_batch_id bigint, a_event_id bigint) OWNER TO postgres;

--
-- Name: set_event_done(text, text, bigint, bigint); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION set_event_done(a_consumer text, a_subconsumer text, a_batch_id bigint, a_event_id bigint) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.set_event_done(4)
--
--	    Marks and event done in a batch for a certain consumer and subconsumer
--
-- Parameters:
--      a_consumer - consumer name
--      a_subconsumer - subconsumer name
--      a_batch_id - a batch id
--      a_event_id - an event id
--
-- Returns:
--      false if already done
--	    true on success 
-- Calls:
--      None
-- Tables directly manipulated:
--      insert - pgq_ext.partial_batch
--      delete - pgq_ext.completed_event
--      update - pgq_ext.partial_batch
--      insert - pgq_ext.completed_event
-- ----------------------------------------------------------------------
declare
    old_batch bigint;
begin
    -- check if done
    perform 1 from pgq_ext.completed_event
     where consumer_id = a_consumer
       and subconsumer_id = a_subconsumer
       and batch_id = a_batch_id
       and event_id = a_event_id;
    if found then
        return false;
    end if;

    -- if batch changed, do cleanup
    select cur_batch_id into old_batch
        from pgq_ext.partial_batch
        where consumer_id = a_consumer
          and subconsumer_id = a_subconsumer;
    if not found then
        -- first time here
        insert into pgq_ext.partial_batch
            (consumer_id, subconsumer_id, cur_batch_id)
            values (a_consumer, a_subconsumer, a_batch_id);
    elsif old_batch <> a_batch_id then
        -- batch changed, that means old is finished on queue db
        -- thus the tagged events are not needed anymore
        delete from pgq_ext.completed_event
            where consumer_id = a_consumer
              and subconsumer_id = a_subconsumer
              and batch_id = old_batch;
        -- remember current one
        update pgq_ext.partial_batch
            set cur_batch_id = a_batch_id
            where consumer_id = a_consumer
              and subconsumer_id = a_subconsumer;
    end if;

    -- tag as done
    insert into pgq_ext.completed_event
        (consumer_id, subconsumer_id, batch_id, event_id)
        values (a_consumer, a_subconsumer, a_batch_id, a_event_id);

    return true;
end;
$$;


ALTER FUNCTION pgq_ext.set_event_done(a_consumer text, a_subconsumer text, a_batch_id bigint, a_event_id bigint) OWNER TO postgres;

--
-- Name: set_last_tick(text, bigint); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION set_last_tick(a_consumer text, a_tick_id bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.set_last_tick(2)
--
--	    records last completed tick for consumer,
--      removes row if a_tick_id is NULL 
--
-- Parameters:
--      a_consumer - consumer name
--      a_tick_id - a tick id
--
-- Returns:
--      1
-- Calls:
--      pgq_ext.set_last_tick(2)
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
begin
    return pgq_ext.set_last_tick(a_consumer, '', a_tick_id);
end;
$$;


ALTER FUNCTION pgq_ext.set_last_tick(a_consumer text, a_tick_id bigint) OWNER TO postgres;

--
-- Name: set_last_tick(text, text, bigint); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION set_last_tick(a_consumer text, a_subconsumer text, a_tick_id bigint) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.set_last_tick(3)
--
--	    records last completed tick for consumer,
--      removes row if a_tick_id is NULL 
--
-- Parameters:
--      a_consumer - consumer name
--      a_subconsumer - subconsumer name
--      a_tick_id - a tick id
--
-- Returns:
--      1
-- Calls:
--      None
-- Tables directly manipulated:
--      delete - pgq_ext.completed_tick
--      update - pgq_ext.completed_tick
--      insert - pgq_ext.completed_tick 
-- ----------------------------------------------------------------------
begin
    if a_tick_id is null then
        delete from pgq_ext.completed_tick
         where consumer_id = a_consumer
           and subconsumer_id = a_subconsumer;
    else   
        update pgq_ext.completed_tick
           set last_tick_id = a_tick_id
         where consumer_id = a_consumer
           and subconsumer_id = a_subconsumer;
        if not found then
            insert into pgq_ext.completed_tick
                (consumer_id, subconsumer_id, last_tick_id)
                values (a_consumer, a_subconsumer, a_tick_id);
        end if;
    end if;

    return 1;
end;
$$;


ALTER FUNCTION pgq_ext.set_last_tick(a_consumer text, a_subconsumer text, a_tick_id bigint) OWNER TO postgres;

--
-- Name: upgrade_schema(); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION upgrade_schema() RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- updates table structure if necessary
-- ----------------------------------------------------------------------
-- Function: pgq_ext.upgrade_schema()
--
--	    Upgrades tables to have column subconsumer_id 
--
-- Parameters:
--      None
--
-- Returns:
--	    number of tables updated 
-- Calls:
--      None
-- Tables directly manipulated:
--      alter - pgq_ext.completed_batch
--      alter - pgq_ext.completed_tick
--      alter - pgq_ext.partial_batch
--      alter - pgq_ext.completed_event
-- ----------------------------------------------------------------------
declare
    cnt int4 = 0;
    tbl text;
    sql text;
begin
    -- pgq_ext.completed_batch: subconsumer_id
    perform 1 from information_schema.columns
      where table_schema = 'pgq_ext'
        and table_name = 'completed_batch'
        and column_name = 'subconsumer_id';
    if not found then
        alter table pgq_ext.completed_batch
            add column subconsumer_id text;
        update pgq_ext.completed_batch
            set subconsumer_id = '';
        alter table pgq_ext.completed_batch
            alter column subconsumer_id set not null;
        alter table pgq_ext.completed_batch
            drop constraint completed_batch_pkey;
        alter table pgq_ext.completed_batch
            add constraint completed_batch_pkey
            primary key (consumer_id, subconsumer_id);
        cnt := cnt + 1;
    end if;

    -- pgq_ext.completed_tick: subconsumer_id
    perform 1 from information_schema.columns
      where table_schema = 'pgq_ext'
        and table_name = 'completed_tick'
        and column_name = 'subconsumer_id';
    if not found then
        alter table pgq_ext.completed_tick
            add column subconsumer_id text;
        update pgq_ext.completed_tick
            set subconsumer_id = '';
        alter table pgq_ext.completed_tick
            alter column subconsumer_id set not null;
        alter table pgq_ext.completed_tick
            drop constraint completed_tick_pkey;
        alter table pgq_ext.completed_tick
            add constraint completed_tick_pkey
            primary key (consumer_id, subconsumer_id);
        cnt := cnt + 1;
    end if;

    -- pgq_ext.partial_batch: subconsumer_id
    perform 1 from information_schema.columns
      where table_schema = 'pgq_ext'
        and table_name = 'partial_batch'
        and column_name = 'subconsumer_id';
    if not found then
        alter table pgq_ext.partial_batch
            add column subconsumer_id text;
        update pgq_ext.partial_batch
            set subconsumer_id = '';
        alter table pgq_ext.partial_batch
            alter column subconsumer_id set not null;
        alter table pgq_ext.partial_batch
            drop constraint partial_batch_pkey;
        alter table pgq_ext.partial_batch
            add constraint partial_batch_pkey
            primary key (consumer_id, subconsumer_id);
        cnt := cnt + 1;
    end if;

    -- pgq_ext.completed_event: subconsumer_id
    perform 1 from information_schema.columns
      where table_schema = 'pgq_ext'
        and table_name = 'completed_event'
        and column_name = 'subconsumer_id';
    if not found then
        alter table pgq_ext.completed_event
            add column subconsumer_id text;
        update pgq_ext.completed_event
            set subconsumer_id = '';
        alter table pgq_ext.completed_event
            alter column subconsumer_id set not null;
        alter table pgq_ext.completed_event
            drop constraint completed_event_pkey;
        alter table pgq_ext.completed_event
            add constraint completed_event_pkey
            primary key (consumer_id, subconsumer_id, batch_id, event_id);
        cnt := cnt + 1;
    end if;

    -- add default value to subconsumer_id column
    for tbl in
        select table_name
           from information_schema.columns
           where table_schema = 'pgq_ext'
             and table_name in ('completed_tick', 'completed_event', 'partial_batch', 'completed_batch')
             and column_name = 'subconsumer_id'
             and column_default is null
    loop
        sql := 'alter table pgq_ext.' || tbl
            || ' alter column subconsumer_id set default ' || quote_literal('');
        execute sql;
        cnt := cnt + 1;
    end loop;

    return cnt;
end;
$$;


ALTER FUNCTION pgq_ext.upgrade_schema() OWNER TO postgres;

--
-- Name: version(); Type: FUNCTION; Schema: pgq_ext; Owner: postgres
--

CREATE FUNCTION version() RETURNS text
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_ext.version(0)
--
--      Returns version string for pgq_ext.  ATM it is based SkyTools version
--      only bumped when database code changes.
-- ----------------------------------------------------------------------
begin
    return '3.1';
end;
$$;


ALTER FUNCTION pgq_ext.version() OWNER TO postgres;

SET search_path = pgq_node, pg_catalog;

--
-- Name: change_consumer_provider(text, text, text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION change_consumer_provider(i_queue_name text, i_consumer_name text, i_new_provider text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.change_consumer_provider(3)
--
--      Change provider for this consumer.
--
-- Parameters:
--      i_queue_name  - queue name
--      i_consumer_name  - consumer name
--      i_new_provider - node name for new provider
-- Returns:
--      ret_code - error code
--      200 - ok
--      404 - no such consumer or new node
--      ret_note - description
-- ----------------------------------------------------------------------
begin
    perform 1 from pgq_node.node_location
      where queue_name = i_queue_name
        and node_name = i_new_provider;
    if not found then
        select 404, 'New node not found: ' || i_new_provider
          into ret_code, ret_note;
        return;
    end if;

    update pgq_node.local_state
       set provider_node = i_new_provider,
           uptodate = false
     where queue_name = i_queue_name
       and consumer_name = i_consumer_name;
    if not found then
        select 404, 'Unknown consumer: ' || i_queue_name || '/' || i_consumer_name
          into ret_code, ret_note;
        return;
    end if;
    select 200, 'Consumer provider node set to : ' || i_new_provider
      into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.change_consumer_provider(i_queue_name text, i_consumer_name text, i_new_provider text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: create_node(text, text, text, text, text, bigint, text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION create_node(i_queue_name text, i_node_type text, i_node_name text, i_worker_name text, i_provider_name text, i_global_watermark bigint, i_combined_queue text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.create_node(7)
--
--      Initialize node.
--
-- Parameters:
--      i_node_name - cascaded queue name
--      i_node_type - node type
--      i_node_name - node name
--      i_worker_name - worker consumer name
--      i_provider_name - provider node name for non-root nodes
--      i_global_watermark - global lowest tick_id
--      i_combined_queue - merge-leaf: target queue
--
-- Returns:
--      200 - Ok
--      401 - node already initialized
--      ???? - maybe we coud use more error codes ?
--
-- Node Types:
--      root - master node
--      branch - subscriber node that can be provider to others
--      leaf - subscriber node that cannot be provider to others
-- Calls:
--      None
-- Tables directly manipulated:
--      None
-- ----------------------------------------------------------------------
declare
    _wm_consumer text;
    _global_wm bigint;
begin
    perform 1 from pgq_node.node_info where queue_name = i_queue_name;
    if found then
        select 401, 'Node already initialized' into ret_code, ret_note;
        return;
    end if;

    _wm_consumer := '.global_watermark';

    if i_node_type = 'root' then
        if coalesce(i_provider_name, i_global_watermark::text,
                    i_combined_queue) is not null then
            select 401, 'unexpected args for '||i_node_type into ret_code, ret_note;
            return;
        end if;

        perform pgq.create_queue(i_queue_name);
        perform pgq.register_consumer(i_queue_name, _wm_consumer);
        _global_wm := (select last_tick from pgq.get_consumer_info(i_queue_name, _wm_consumer));
    elsif i_node_type = 'branch' then
        if i_provider_name is null then
            select 401, 'provider not set for '||i_node_type into ret_code, ret_note;
            return;
        end if;
        if i_global_watermark is null then
            select 401, 'global watermark not set for '||i_node_type into ret_code, ret_note;
            return;
        end if;
        perform pgq.create_queue(i_queue_name);
        update pgq.queue
            set queue_external_ticker = true,
                queue_disable_insert = true
            where queue_name = i_queue_name;
        if i_global_watermark > 1 then
            perform pgq.ticker(i_queue_name, i_global_watermark, now(), 1);
        end if;
        perform pgq.register_consumer_at(i_queue_name, _wm_consumer, i_global_watermark);
        _global_wm := i_global_watermark;
    elsif i_node_type = 'leaf' then
        _global_wm := i_global_watermark;
        if i_combined_queue is not null then
            perform 1 from pgq.get_queue_info(i_combined_queue);
            if not found then
                select 401, 'non-existing queue on leaf side: '||i_combined_queue
                into ret_code, ret_note;
                return;
            end if;
        end if;
    else
        select 401, 'bad node type: '||i_node_type
          into ret_code, ret_note;
    end if;

    insert into pgq_node.node_info
      (queue_name, node_type, node_name,
       worker_name, combined_queue)
    values (i_queue_name, i_node_type, i_node_name,
       i_worker_name, i_combined_queue);

    if i_node_type <> 'root' then
        select f.ret_code, f.ret_note into ret_code, ret_note
          from pgq_node.register_consumer(i_queue_name, i_worker_name,
                    i_provider_name, _global_wm) f;
    else
        select f.ret_code, f.ret_note into ret_code, ret_note
          from pgq_node.register_consumer(i_queue_name, i_worker_name,
                    i_node_name, _global_wm) f;
    end if;
        if ret_code <> 200 then
            return;
        end if;

    select 200, 'Node "' || i_node_name || '" initialized for queue "'
           || i_queue_name || '" with type "' || i_node_type || '"'
        into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.create_node(i_queue_name text, i_node_type text, i_node_name text, i_worker_name text, i_provider_name text, i_global_watermark bigint, i_combined_queue text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: demote_root(text, integer, text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION demote_root(i_queue_name text, i_step integer, i_new_provider text, OUT ret_code integer, OUT ret_note text, OUT last_tick bigint) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.demote_root(3)
--
--      Multi-step root demotion to branch.
--
--      Must be be called for each step in sequence:
--
--      Step 1 - disable writing to queue.
--      Step 2 - wait until writers go away, do tick.
--      Step 3 - change type, register.
--
-- Parameters:
--      i_queue_name    - queue name
--      i_step          - step number
--      i_new_provider  - new provider node
-- Returns:
--      200 - success
--      404 - node not initialized for queue 
--      301 - node is not root
-- ----------------------------------------------------------------------
declare
    n_type      text;
    w_name      text;
    sql         text;
    ev_id       int8;
    ev_tbl      text;
begin
    select node_type, worker_name into n_type, w_name
        from pgq_node.node_info
        where queue_name = i_queue_name
        for update;
    if not found then
        select 404, 'Node not initialized for queue: ' || i_queue_name
          into ret_code, ret_note;
        return;
    end if;

    if n_type != 'root' then
        select 301, 'Node not root'
          into ret_code, ret_note;
        return;
    end if;
    if i_step > 1 then
        select queue_data_pfx
            into ev_tbl
            from pgq.queue
            where queue_name = i_queue_name
                and queue_disable_insert
                and queue_external_ticker;
        if not found then
            raise exception 'steps in wrong order';
        end if;
    end if;

    if i_step = 1 then
        update pgq.queue
            set queue_disable_insert = true,
                queue_external_ticker = true
            where queue_name = i_queue_name;
        if not found then
            select 404, 'Huh, no queue?: ' || i_queue_name
              into ret_code, ret_note;
            return;
        end if;
        select 200, 'Step 1: Writing disabled for: ' || i_queue_name
          into ret_code, ret_note;
    elsif i_step = 2 then
        set local session_replication_role = 'replica';

        -- lock parent table to stop updates, allow reading
        sql := 'lock table ' || ev_tbl || ' in exclusive mode';
        execute sql;
        

        select nextval(queue_tick_seq), nextval(queue_event_seq)
            into last_tick, ev_id
            from pgq.queue
            where queue_name = i_queue_name;

        perform pgq.ticker(i_queue_name, last_tick, now(), ev_id);

        select 200, 'Step 2: Inserted last tick: ' || i_queue_name
            into ret_code, ret_note;
    elsif i_step = 3 then
        -- change type, point worker to new provider
        select t.tick_id into last_tick
            from pgq.tick t, pgq.queue q
            where q.queue_name = i_queue_name
                and t.tick_queue = q.queue_id
            order by t.tick_queue desc, t.tick_id desc
            limit 1;
        update pgq_node.node_info
            set node_type = 'branch'
            where queue_name = i_queue_name;
        update pgq_node.local_state
            set provider_node = i_new_provider,
                last_tick_id = last_tick,
                uptodate = false
            where queue_name = i_queue_name
                and consumer_name = w_name;
        select 200, 'Step 3: Demoted root to branch: ' || i_queue_name
          into ret_code, ret_note;
    else
        raise exception 'incorrect step number';
    end if;
    return;
end;
$$;


ALTER FUNCTION pgq_node.demote_root(i_queue_name text, i_step integer, i_new_provider text, OUT ret_code integer, OUT ret_note text, OUT last_tick bigint) OWNER TO postgres;

--
-- Name: drop_node(text, text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION drop_node(i_queue_name text, i_node_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.drop_node(2)
--
--      Drop node. This needs to be run on all the members of a set
--      to properly get rid of the node.
--
-- Parameters:
--      i_queue_name - queue name
--      i_node_name - node_name
--
-- Returns:
--      ret_code - error code
--      ret_note - error description
--
-- Return Codes:
--      200 - Ok
--      304 - No such queue
--      406 - That is a provider
-- Calls:
--      None
-- Tables directly manipulated:
--      None
------------------------------------------------------------------------
declare
    _is_local   boolean;
    _is_prov    boolean;
begin
    select (n.node_name = i_node_name),
           (select s.provider_node = i_node_name
              from pgq_node.local_state s
              where s.queue_name = i_queue_name
                and s.consumer_name = n.worker_name)
        into _is_local, _is_prov
        from pgq_node.node_info n
        where n.queue_name = i_queue_name;

    if not found then
        -- proceed with cleaning anyway, as there schenarios
        -- where some data is left around
        _is_prov := false;
        _is_local := true;
    end if;

    -- drop local state
    if _is_local then
        delete from pgq_node.subscriber_info
         where queue_name = i_queue_name;

        delete from pgq_node.local_state
         where queue_name = i_queue_name;

        delete from pgq_node.node_info
         where queue_name = i_queue_name
            and node_name = i_node_name;

        perform pgq.drop_queue(queue_name, true)
           from pgq.queue where queue_name = i_queue_name;

        delete from pgq_node.node_location
         where queue_name = i_queue_name
           and node_name <> i_node_name;
    elsif _is_prov then
        select 405, 'Cannot drop provider node: ' || i_node_name into ret_code, ret_note;
        return;
    else
        perform pgq_node.unregister_subscriber(i_queue_name, i_node_name);
    end if;

    -- let the unregister_location send event if needed
    select f.ret_code, f.ret_note
        from pgq_node.unregister_location(i_queue_name, i_node_name) f
        into ret_code, ret_note;

    select 200, 'Node dropped: ' || i_node_name
        into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.drop_node(i_queue_name text, i_node_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: get_consumer_info(text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION get_consumer_info(i_queue_name text, OUT consumer_name text, OUT provider_node text, OUT last_tick_id bigint, OUT paused boolean, OUT uptodate boolean, OUT cur_error text) RETURNS SETOF record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.get_consumer_info(1)
--
--      Get consumer list that work on the local node.
--
-- Parameters:
--      i_queue_name  - cascaded queue name
--
-- Returns:
--      consumer_name   - cascaded consumer name
--      provider_node   - node from where the consumer reads from
--      last_tick_id    - last committed tick
--      paused          - if consumer is paused
--      uptodate        - if consumer is uptodate
--      cur_error       - failure reason
-- ----------------------------------------------------------------------
begin
    for consumer_name, provider_node, last_tick_id, paused, uptodate, cur_error in
        select s.consumer_name, s.provider_node, s.last_tick_id,
               s.paused, s.uptodate, s.cur_error
            from pgq_node.local_state s
            where s.queue_name = i_queue_name
            order by 1
    loop
        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION pgq_node.get_consumer_info(i_queue_name text, OUT consumer_name text, OUT provider_node text, OUT last_tick_id bigint, OUT paused boolean, OUT uptodate boolean, OUT cur_error text) OWNER TO postgres;

--
-- Name: get_consumer_state(text, text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION get_consumer_state(i_queue_name text, i_consumer_name text, OUT ret_code integer, OUT ret_note text, OUT node_type text, OUT node_name text, OUT completed_tick bigint, OUT provider_node text, OUT provider_location text, OUT paused boolean, OUT uptodate boolean, OUT cur_error text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.get_consumer_state(2)
--
--      Get info for cascaded consumer that targets local node.
--
-- Parameters:
--      i_node_name  - cascaded queue name
--      i_consumer_name - cascaded consumer name
--
-- Returns:
--      node_type - local node type
--      node_name - local node name
--      completed_tick - last committed tick
--      provider_node - provider node name
--      provider_location - connect string to provider node
--      paused - this node should not do any work
--      uptodate - if consumer has loaded last changes
--      cur_error - failure reason
-- ----------------------------------------------------------------------
begin
    select n.node_type, n.node_name
      into node_type, node_name
      from pgq_node.node_info n
    where n.queue_name = i_queue_name;
    if not found then
        select 404, 'Unknown queue: ' || i_queue_name
          into ret_code, ret_note;
        return;
    end if;
    select s.last_tick_id, s.provider_node, s.paused, s.uptodate, s.cur_error
      into completed_tick, provider_node, paused, uptodate, cur_error
      from pgq_node.local_state s
     where s.queue_name = i_queue_name
       and s.consumer_name = i_consumer_name;
    if not found then
        select 404, 'Unknown consumer: ' || i_queue_name || '/' || i_consumer_name
          into ret_code, ret_note;
        return;
    end if;
    select 100, 'Ok', p.node_location
      into ret_code, ret_note, provider_location
      from pgq_node.node_location p
     where p.queue_name = i_queue_name
      and p.node_name = provider_node;
    if not found then
        select 404, 'Unknown provider node: ' || i_queue_name || '/' || provider_node
          into ret_code, ret_note;
        return;
    end if;
    return;
end;
$$;


ALTER FUNCTION pgq_node.get_consumer_state(i_queue_name text, i_consumer_name text, OUT ret_code integer, OUT ret_note text, OUT node_type text, OUT node_name text, OUT completed_tick bigint, OUT provider_node text, OUT provider_location text, OUT paused boolean, OUT uptodate boolean, OUT cur_error text) OWNER TO postgres;

--
-- Name: get_node_info(text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION get_node_info(i_queue_name text, OUT ret_code integer, OUT ret_note text, OUT node_type text, OUT node_name text, OUT global_watermark bigint, OUT local_watermark bigint, OUT provider_node text, OUT provider_location text, OUT combined_queue text, OUT combined_type text, OUT worker_name text, OUT worker_paused boolean, OUT worker_uptodate boolean, OUT worker_last_tick bigint, OUT node_attrs text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.get_node_info(1)
--
--      Get local node info for cascaded queue.
--
-- Parameters:
--      i_queue_name  - cascaded queue name
--
-- Returns:
--      node_type - local node type
--      node_name - local node name
--      global_watermark - queue's global watermark
--      local_watermark - queue's local watermark, for this and below nodes
--      provider_node - provider node name
--      provider_location - provider connect string
--      combined_queue - queue name for target set
--      combined_type - node type of target set
--      worker_name - consumer name that maintains this node
--      worker_paused - is worker paused
--      worker_uptodate - is worker seen the changes
--      worker_last_tick - last committed tick_id by worker
--      node_attrs - urlencoded dict of random attrs for worker (eg. sync_watermark)
-- ----------------------------------------------------------------------
declare
    sql text;
begin
    select 100, 'Ok', n.node_type, n.node_name,
           c.node_type, c.queue_name, w.provider_node, l.node_location,
           n.worker_name, w.paused, w.uptodate, w.last_tick_id,
           n.node_attrs
      into ret_code, ret_note, node_type, node_name,
           combined_type, combined_queue, provider_node, provider_location,
           worker_name, worker_paused, worker_uptodate, worker_last_tick,
           node_attrs
      from pgq_node.node_info n
           left join pgq_node.node_info c on (c.queue_name = n.combined_queue)
           left join pgq_node.local_state w on (w.queue_name = n.queue_name and w.consumer_name = n.worker_name)
           left join pgq_node.node_location l on (l.queue_name = w.queue_name and l.node_name = w.provider_node)
      where n.queue_name = i_queue_name;
    if not found then
        select 404, 'Unknown queue: ' || i_queue_name into ret_code, ret_note;
        return;
    end if;

    if node_type in ('root', 'branch') then
        select min(case when consumer_name = '.global_watermark' then null else last_tick end),
               min(case when consumer_name = '.global_watermark' then last_tick else null end)
          into local_watermark, global_watermark
          from pgq.get_consumer_info(i_queue_name);
        if local_watermark is null then
            select t.tick_id into local_watermark
              from pgq.tick t, pgq.queue q
             where t.tick_queue = q.queue_id
               and q.queue_name = i_queue_name
             order by 1 desc
             limit 1;
        end if;
    else
        local_watermark := worker_last_tick;
    end if;

    if node_type = 'root' then
        select tick_id from pgq.tick t, pgq.queue q
         where q.queue_name = i_queue_name
           and t.tick_queue = q.queue_id
         order by t.tick_queue desc, t.tick_id desc
         limit 1
         into worker_last_tick;
    end if;

    return;
end;
$$;


ALTER FUNCTION pgq_node.get_node_info(i_queue_name text, OUT ret_code integer, OUT ret_note text, OUT node_type text, OUT node_name text, OUT global_watermark bigint, OUT local_watermark bigint, OUT provider_node text, OUT provider_location text, OUT combined_queue text, OUT combined_type text, OUT worker_name text, OUT worker_paused boolean, OUT worker_uptodate boolean, OUT worker_last_tick bigint, OUT node_attrs text) OWNER TO postgres;

--
-- Name: get_queue_locations(text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION get_queue_locations(i_queue_name text, OUT node_name text, OUT node_location text, OUT dead boolean) RETURNS SETOF record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.get_queue_locations(1)
--
--      Get node list for the queue.
--
-- Parameters:
--      i_queue_name    - queue name
--
-- Returns:
--      node_name       - node name
--      node_location   - libpq connect string for the node
--      dead            - whether the node should be considered dead
-- ----------------------------------------------------------------------
begin
    for node_name, node_location, dead in
        select l.node_name, l.node_location, l.dead
          from pgq_node.node_location l
         where l.queue_name = i_queue_name
    loop
        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION pgq_node.get_queue_locations(i_queue_name text, OUT node_name text, OUT node_location text, OUT dead boolean) OWNER TO postgres;

--
-- Name: get_subscriber_info(text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION get_subscriber_info(i_queue_name text, OUT node_name text, OUT worker_name text, OUT node_watermark bigint) RETURNS SETOF record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.get_subscriber_info(1)
--
--      Get subscriber list for the local node.
--
--      It may be out-of-date, due to in-progress
--      administrative change.
--      Node's local provider info ( pgq_node.get_node_info() or pgq_node.get_worker_state(1) )
--      is the authoritative source.
--
-- Parameters:
--      i_queue_name  - cascaded queue name
--
-- Returns:
--      node_name       - node name that uses current node as provider
--      worker_name     - consumer that maintains remote node
--      local_watermark - lowest tick_id on subscriber
-- ----------------------------------------------------------------------
declare
    _watermark_name text;
begin
    for node_name, worker_name, _watermark_name in
        select s.subscriber_node, s.worker_name, s.watermark_name
          from pgq_node.subscriber_info s
         where s.queue_name = i_queue_name
         order by 1
    loop
        select last_tick into node_watermark
            from pgq.get_consumer_info(i_queue_name, _watermark_name);
        return next;
    end loop;
    return;
end;
$$;


ALTER FUNCTION pgq_node.get_subscriber_info(i_queue_name text, OUT node_name text, OUT worker_name text, OUT node_watermark bigint) OWNER TO postgres;

--
-- Name: get_worker_state(text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION get_worker_state(i_queue_name text, OUT ret_code integer, OUT ret_note text, OUT node_type text, OUT node_name text, OUT completed_tick bigint, OUT provider_node text, OUT provider_location text, OUT paused boolean, OUT uptodate boolean, OUT cur_error text, OUT worker_name text, OUT global_watermark bigint, OUT local_watermark bigint, OUT local_queue_top bigint, OUT combined_queue text, OUT combined_type text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.get_worker_state(1)
--
--      Get info for consumer that maintains local node.
--
-- Parameters:
--      i_queue_name  - cascaded queue name
--
-- Returns:
--      node_type - local node type
--      node_name - local node name
--      completed_tick - last committed tick
--      provider_node - provider node name
--      provider_location - connect string to provider node
--      paused - this node should not do any work
--      uptodate - if consumer has loaded last changes
--      cur_error - failure reason

--      worker_name - consumer name that maintains this node
--      global_watermark - queue's global watermark
--      local_watermark - queue's local watermark, for this and below nodes
--      local_queue_top - last tick in local queue
--      combined_queue - queue name for target set
--      combined_type - node type of target setA
-- ----------------------------------------------------------------------
begin
    select n.node_type, n.node_name, n.worker_name, n.combined_queue
      into node_type, node_name, worker_name, combined_queue
      from pgq_node.node_info n
     where n.queue_name = i_queue_name;
    if not found then
        select 404, 'Unknown queue: ' || i_queue_name
          into ret_code, ret_note;
        return;
    end if;
    select s.last_tick_id, s.provider_node, s.paused, s.uptodate, s.cur_error
      into completed_tick, provider_node, paused, uptodate, cur_error
      from pgq_node.local_state s
     where s.queue_name = i_queue_name
       and s.consumer_name = worker_name;
    if not found then
        select 404, 'Unknown consumer: ' || i_queue_name || '/' || worker_name
          into ret_code, ret_note;
        return;
    end if;
    select 100, 'Ok', p.node_location
      into ret_code, ret_note, provider_location
      from pgq_node.node_location p
     where p.queue_name = i_queue_name
      and p.node_name = provider_node;
    if not found then
        select 404, 'Unknown provider node: ' || i_queue_name || '/' || provider_node
          into ret_code, ret_note;
        return;
    end if;

    if combined_queue is not null then
        select n.node_type into combined_type
          from pgq_node.node_info n
         where n.queue_name = get_worker_state.combined_queue;
        if not found then
            select 404, 'Combinde queue node not found: ' || combined_queue
              into ret_code, ret_note;
            return;
        end if;
    end if;

    if node_type in ('root', 'branch') then
        select min(case when consumer_name = '.global_watermark' then null else last_tick end),
               min(case when consumer_name = '.global_watermark' then last_tick else null end)
          into local_watermark, global_watermark
          from pgq.get_consumer_info(i_queue_name);
        if local_watermark is null then
            select t.tick_id into local_watermark
              from pgq.tick t, pgq.queue q
             where t.tick_queue = q.queue_id
               and q.queue_name = i_queue_name
             order by 1 desc
             limit 1;
        end if;

        select tick_id from pgq.tick t, pgq.queue q
         where q.queue_name = i_queue_name
           and t.tick_queue = q.queue_id
         order by t.tick_queue desc, t.tick_id desc
         limit 1 into local_queue_top;
    else
        local_watermark := completed_tick;
    end if;

    return;
end;
$$;


ALTER FUNCTION pgq_node.get_worker_state(i_queue_name text, OUT ret_code integer, OUT ret_note text, OUT node_type text, OUT node_name text, OUT completed_tick bigint, OUT provider_node text, OUT provider_location text, OUT paused boolean, OUT uptodate boolean, OUT cur_error text, OUT worker_name text, OUT global_watermark bigint, OUT local_watermark bigint, OUT local_queue_top bigint, OUT combined_queue text, OUT combined_type text) OWNER TO postgres;

--
-- Name: is_leaf_node(text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION is_leaf_node(i_queue_name text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.is_leaf_node(1)
--
--      Checs if node is leaf.
--
-- Parameters:
--      i_queue_name  - queue name
-- Returns:
--      true - if this this the leaf node for queue 
-- ----------------------------------------------------------------------
declare
    res bool;
begin
    select n.node_type = 'leaf' into res
      from pgq_node.node_info n
      where n.queue_name = i_queue_name;
    if not found then
        raise exception 'queue does not exist: %', i_queue_name;
    end if;
    return res;
end;
$$;


ALTER FUNCTION pgq_node.is_leaf_node(i_queue_name text) OWNER TO postgres;

--
-- Name: is_root_node(text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION is_root_node(i_queue_name text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.is_root_node(1)
--
--      Checs if node is root.
--
-- Parameters:
--      i_queue_name  - queue name
-- Returns:
--      true - if this this the root node for queue 
-- ----------------------------------------------------------------------
declare
    res bool;
begin
    select n.node_type = 'root' into res
      from pgq_node.node_info n
      where n.queue_name = i_queue_name;
    if not found then
        raise exception 'queue does not exist: %', i_queue_name;
    end if;
    return res;
end;
$$;


ALTER FUNCTION pgq_node.is_root_node(i_queue_name text) OWNER TO postgres;

--
-- Name: maint_watermark(text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION maint_watermark(i_queue_name text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.maint_watermark(1)
--
--      Move global watermark on root node.
--
-- Returns:
--      0 - tells pgqd to call just once
-- ----------------------------------------------------------------------
declare
    _lag interval;
begin
    perform 1 from pgq_node.node_info
      where queue_name = i_queue_name
        and node_type = 'root'
      for update;
    if not found then
        return 0;
    end if;

    select lag into _lag from pgq.get_consumer_info(i_queue_name, '.global_watermark');
    if _lag >= '5 minutes'::interval then
        perform pgq_node.set_global_watermark(i_queue_name, NULL);
    end if;

    return 0;
end;
$$;


ALTER FUNCTION pgq_node.maint_watermark(i_queue_name text) OWNER TO postgres;

--
-- Name: promote_branch(text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION promote_branch(i_queue_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.promote_branch(1)
--
--      Promote branch node to root.
--
-- Parameters:
--      i_queue_name  - queue name
--
-- Returns:
--      200 - success
--      404 - node not initialized for queue 
--      301 - node is not branch
-- ----------------------------------------------------------------------
declare
    n_name      text;
    n_type      text;
    w_name      text;
    last_tick   bigint;
    sql         text;
begin
    select node_name, node_type, worker_name into n_name, n_type, w_name
        from pgq_node.node_info
        where queue_name = i_queue_name
        for update;
    if not found then
        select 404, 'Node not initialized for queue: ' || i_queue_name
          into ret_code, ret_note;
        return;
    end if;

    if n_type != 'branch' then
        select 301, 'Node not branch'
          into ret_code, ret_note;
        return;
    end if;

    update pgq.queue
        set queue_disable_insert = false,
            queue_external_ticker = false
        where queue_name = i_queue_name;

    -- change type, point worker to itself
    select t.tick_id into last_tick
        from pgq.tick t, pgq.queue q
        where q.queue_name = i_queue_name
            and t.tick_queue = q.queue_id
        order by t.tick_queue desc, t.tick_id desc
        limit 1;

    -- make tick seq larger than last tick
    perform pgq.seq_setval(queue_tick_seq, last_tick)
        from pgq.queue where queue_name = i_queue_name;

    update pgq_node.node_info
        set node_type = 'root'
        where queue_name = i_queue_name;

    update pgq_node.local_state
        set provider_node = n_name,
            last_tick_id = last_tick,
            uptodate = false
        where queue_name = i_queue_name
            and consumer_name = w_name;

    select 200, 'Branch node promoted to root'
      into ret_code, ret_note;

    return;
end;
$$;


ALTER FUNCTION pgq_node.promote_branch(i_queue_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: register_consumer(text, text, text, bigint); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION register_consumer(i_queue_name text, i_consumer_name text, i_provider_node text, i_custom_tick_id bigint, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.register_consumer(4)
--
--      Subscribe plain cascaded consumer to a target node.
--      That means it's planning to read from remote node
--      and write to local node.
--
-- Parameters:
--      i_queue_name - set name
--      i_consumer_name - cascaded consumer name
--      i_provider_node - node name
--      i_custom_tick_id - tick id
--
-- Returns:
--      ret_code - error code
--      200 - ok
--      201 - already registered
--      401 - no such queue
--      ret_note - description
-- ----------------------------------------------------------------------
declare
    n record;
    node_wm_name text;
    node_pos bigint;
begin
    select node_type into n
      from pgq_node.node_info where queue_name = i_queue_name
       for update;
    if not found then
        select 404, 'Unknown queue: ' || i_queue_name into ret_code, ret_note;
        return;
    end if;
    perform 1 from pgq_node.local_state
      where queue_name = i_queue_name
        and consumer_name = i_consumer_name;
    if found then
        update pgq_node.local_state
           set provider_node = i_provider_node,
               last_tick_id = i_custom_tick_id
         where queue_name = i_queue_name
           and consumer_name = i_consumer_name;
        select 201, 'Consumer already registered: ' || i_queue_name
               || '/' || i_consumer_name  into ret_code, ret_note;
        return;
    end if;

    insert into pgq_node.local_state (queue_name, consumer_name, provider_node, last_tick_id)
           values (i_queue_name, i_consumer_name, i_provider_node, i_custom_tick_id);

    select 200, 'Consumer '||i_consumer_name||' registered on queue '||i_queue_name
        into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.register_consumer(i_queue_name text, i_consumer_name text, i_provider_node text, i_custom_tick_id bigint, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: register_location(text, text, text, boolean); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION register_location(i_queue_name text, i_node_name text, i_node_location text, i_dead boolean, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.register_location(4)
--
--      Add new node location.
--
-- Parameters:
--      i_queue_name - queue name
--      i_node_name - node name
--      i_node_location - node connect string
--      i_dead - dead flag for node
--
-- Returns:
--      ret_code - error code
--      ret_note - error description
--
-- Return Codes:
--      200 - Ok
-- ----------------------------------------------------------------------
declare
    node record;
begin
    select node_type = 'root' as is_root into node
      from pgq_node.node_info where queue_name = i_queue_name
       for update;
    -- may return 0 rows

    perform 1 from pgq_node.node_location
     where queue_name = i_queue_name
       and node_name = i_node_name;
    if found then
        update pgq_node.node_location
           set node_location = coalesce(i_node_location, node_location),
               dead = i_dead
         where queue_name = i_queue_name
           and node_name = i_node_name;
    elsif i_node_location is not null then
        insert into pgq_node.node_location (queue_name, node_name, node_location, dead)
        values (i_queue_name, i_node_name, i_node_location, i_dead);
    end if;

    if node.is_root then
        perform pgq.insert_event(i_queue_name, 'pgq.location-info',
                                 i_node_name, i_queue_name, i_node_location, i_dead::text, null)
           from pgq_node.node_info n
         where n.queue_name = i_queue_name;
    end if;

    select 200, 'Location registered' into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.register_location(i_queue_name text, i_node_name text, i_node_location text, i_dead boolean, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: register_subscriber(text, text, text, bigint); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION register_subscriber(i_queue_name text, i_remote_node_name text, i_remote_worker_name text, i_custom_tick_id bigint, OUT ret_code integer, OUT ret_note text, OUT global_watermark bigint) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.register_subscriber(4)
--
--      Subscribe remote node to local node at custom position.
--      Should be used when changing provider for existing node.
--
-- Parameters:
--      i_node_name - set name
--      i_remote_node_name - node name
--      i_remote_worker_name - consumer name
--      i_custom_tick_id - tick id [optional]
--
-- Returns:
--      ret_code - error code
--      ret_note - description
--      global_watermark - minimal watermark
-- ----------------------------------------------------------------------
declare
    n record;
    node_wm_name text;
    node_pos bigint;
begin
    select node_type into n
      from pgq_node.node_info where queue_name = i_queue_name
       for update;
    if not found then
        select 404, 'Unknown queue: ' || i_queue_name into ret_code, ret_note;
        return;
    end if;
    select last_tick into global_watermark
      from pgq.get_consumer_info(i_queue_name, '.global_watermark');

    if n.node_type not in ('root', 'branch') then
        select 401, 'Cannot subscribe to ' || n.node_type || ' node'
          into ret_code, ret_note;
        return;
    end if;

    node_wm_name := '.' || i_remote_node_name || '.watermark';
    node_pos := coalesce(i_custom_tick_id, global_watermark);

    perform pgq.register_consumer_at(i_queue_name, node_wm_name, global_watermark);

    perform pgq.register_consumer_at(i_queue_name, i_remote_worker_name, node_pos);

    insert into pgq_node.subscriber_info (queue_name, subscriber_node, worker_name, watermark_name)
        values (i_queue_name, i_remote_node_name, i_remote_worker_name, node_wm_name);

    select 200, 'Subscriber registered: '||i_remote_node_name into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.register_subscriber(i_queue_name text, i_remote_node_name text, i_remote_worker_name text, i_custom_tick_id bigint, OUT ret_code integer, OUT ret_note text, OUT global_watermark bigint) OWNER TO postgres;

--
-- Name: set_consumer_completed(text, text, bigint); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION set_consumer_completed(i_queue_name text, i_consumer_name text, i_tick_id bigint, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.set_consumer_completed(3)
--
--      Set last completed tick id for the cascaded consumer
--      that it has committed to local node.
--
-- Parameters:
--      i_queue_name - cascaded queue name
--      i_consumer_name - cascaded consumer name
--      i_tick_id   - tick id
-- Returns:
--      200 - ok
--      404 - consumer not known
-- ----------------------------------------------------------------------
begin
    update pgq_node.local_state
       set last_tick_id = i_tick_id,
           cur_error = NULL
     where queue_name = i_queue_name
       and consumer_name = i_consumer_name;
    if found then
        select 100, 'Consumer ' || i_consumer_name || ' compleded tick = ' || i_tick_id::text
            into ret_code, ret_note;
    else
        select 404, 'Consumer not known: '
               || i_queue_name || '/' || i_consumer_name
          into ret_code, ret_note;
    end if;
    return;
end;
$$;


ALTER FUNCTION pgq_node.set_consumer_completed(i_queue_name text, i_consumer_name text, i_tick_id bigint, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: set_consumer_error(text, text, text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION set_consumer_error(i_queue_name text, i_consumer_name text, i_error_msg text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.set_consumer_error(3)
--
--      If batch processing fails, consumer can store it's last error in db.
-- Returns:
--      100 - ok
--      101 - consumer not known
-- ----------------------------------------------------------------------
begin
    update pgq_node.local_state
       set cur_error = i_error_msg
     where queue_name = i_queue_name
       and consumer_name = i_consumer_name;
    if found then
        select 100, 'Consumer ' || i_consumer_name || ' error = ' || i_error_msg
            into ret_code, ret_note;
    else
        select 101, 'Consumer not known, ignoring: '
               || i_queue_name || '/' || i_consumer_name
          into ret_code, ret_note;
    end if;
    return;
end;
$$;


ALTER FUNCTION pgq_node.set_consumer_error(i_queue_name text, i_consumer_name text, i_error_msg text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: set_consumer_paused(text, text, boolean); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION set_consumer_paused(i_queue_name text, i_consumer_name text, i_paused boolean, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.set_consumer_paused(3)
--
--      Set consumer paused flag.
--
-- Parameters:
--      i_queue_name - cascaded queue name
--      i_consumer_name - cascaded consumer name
--      i_paused   - new flag state
-- Returns:
--      200 - ok
--      201 - already paused
--      404 - consumer not found
-- ----------------------------------------------------------------------
declare
    old_flag    boolean;
    word        text;
begin
    if i_paused then
        word := 'paused';
    else
        word := 'resumed';
    end if;

    select paused into old_flag
        from pgq_node.local_state
        where queue_name = i_queue_name
          and consumer_name = i_consumer_name
        for update;
    if not found then
        select 404, 'Unknown consumer: ' || i_consumer_name
            into ret_code, ret_note;
    elsif old_flag = i_paused then
        select 201, 'Consumer ' || i_consumer_name || ' already ' || word
            into ret_code, ret_note;
    else
        update pgq_node.local_state
            set paused = i_paused,
                uptodate = false
            where queue_name = i_queue_name
            and consumer_name = i_consumer_name;

        select 200, 'Consumer '||i_consumer_name||' tagged as '||word into ret_code, ret_note;
    end if;
    return;

end;
$$;


ALTER FUNCTION pgq_node.set_consumer_paused(i_queue_name text, i_consumer_name text, i_paused boolean, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: set_consumer_uptodate(text, text, boolean); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION set_consumer_uptodate(i_queue_name text, i_consumer_name text, i_uptodate boolean, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.set_consumer_uptodate(3)
--
--      Set consumer uptodate flag.....
--
-- Parameters:
--      i_queue_name - queue name
--      i_consumer_name - consumer name
--      i_uptodate - new flag state
--
-- Returns:
--      200 - ok
--      404 - consumer not known
-- ----------------------------------------------------------------------
begin
    update pgq_node.local_state
       set uptodate = i_uptodate
     where queue_name = i_queue_name
       and consumer_name = i_consumer_name;
    if found then
        select 200, 'Consumer uptodate = ' || i_uptodate::int4::text
               into ret_code, ret_note;
    else
        select 404, 'Consumer not known: '
               || i_queue_name || '/' || i_consumer_name
          into ret_code, ret_note;
    end if;
    return;
end;
$$;


ALTER FUNCTION pgq_node.set_consumer_uptodate(i_queue_name text, i_consumer_name text, i_uptodate boolean, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: set_global_watermark(text, bigint); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION set_global_watermark(i_queue_name text, i_watermark bigint, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.set_global_watermark(2)
--
--      Move global watermark on branch/leaf, publish on root.
--
-- Parameters:
--      i_queue_name    - queue name
--      i_watermark     - global tick_id that is processed everywhere.
--                        NULL on root, then local wm is published.
-- ----------------------------------------------------------------------
declare
    this        record;
    _wm         bigint;
    wm_consumer text;
begin
    wm_consumer = '.global_watermark';

    select node_type, queue_name, worker_name into this
        from pgq_node.node_info
        where queue_name = i_queue_name
        for update;
    if not found then
        select 404, 'Queue' || i_queue_name || ' not found'
          into ret_code, ret_note;
        return;
    end if;

    _wm = i_watermark;
    if this.node_type = 'root' then
        if i_watermark is null then
            select f.ret_code, f.ret_note, f.local_watermark
                into ret_code, ret_note, _wm
                from pgq_node.get_node_info(i_queue_name) f;
            if ret_code >= 300 then
                return;
            end if;
            if _wm is null then
                raise exception 'local_watermark=NULL from get_node_info()?';
            end if;
        end if;

        -- move watermark
        perform pgq.register_consumer_at(i_queue_name, wm_consumer, _wm);

        -- send event downstream
        perform pgq.insert_event(i_queue_name, 'pgq.global-watermark', _wm::text,
                                 i_queue_name, null, null, null);
        -- update root workers pos to avoid it getting stale
        update pgq_node.local_state
            set last_tick_id = _wm
            where queue_name = i_queue_name
                and consumer_name = this.worker_name;
    elsif this.node_type = 'branch' then
        if i_watermark is null then
            select 500, 'bad usage: wm=null on branch node'
                into ret_code, ret_note;
            return;
        end if;

        -- tick can be missing if we are processing
        -- old batches that set watermark outside
        -- current range
        perform 1 from pgq.tick t, pgq.queue q
          where q.queue_name = i_queue_name
            and t.tick_queue = q.queue_id
            and t.tick_id = _wm;
        if not found then
            select 200, 'Skipping global watermark update to ' || _wm::text
                into ret_code, ret_note;
            return;
        end if;

        -- move watermark
        perform pgq.register_consumer_at(i_queue_name, wm_consumer, _wm);
    else
        select 100, 'Ignoring global watermark in leaf'
            into ret_code, ret_note;
        return;
    end if;

    select 200, 'Global watermark set to ' || _wm::text
        into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.set_global_watermark(i_queue_name text, i_watermark bigint, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: set_node_attrs(text, text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION set_node_attrs(i_queue_name text, i_node_attrs text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.create_attrs(2)
--
--      Set node attributes.
--
-- Parameters:
--      i_node_name - cascaded queue name
--      i_node_attrs - urlencoded node attrs
--
-- Returns:
--      200 - ok
--      404 - node not found
-- ----------------------------------------------------------------------
begin
    update pgq_node.node_info
        set node_attrs = i_node_attrs
        where queue_name = i_queue_name;
    if not found then
        select 404, 'Node not found' into ret_code, ret_note;
        return;
    end if;

    select 200, 'Node attributes updated'
        into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.set_node_attrs(i_queue_name text, i_node_attrs text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: set_partition_watermark(text, text, bigint); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION set_partition_watermark(i_combined_queue_name text, i_part_queue_name text, i_watermark bigint, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.set_partition_watermark(3)
--
--      Move merge-leaf position on combined-branch.
--
-- Parameters:
--      i_combined_queue_name - local combined queue name
--      i_part_queue_name     - local part queue name (merge-leaf)
--      i_watermark         - partition tick_id that came inside combined-root batch
--
-- Returns:
--      200 - success
--      201 - no partition queue
--      401 - worker registration not found
-- ----------------------------------------------------------------------
declare
    n record;
begin
    -- check if combined-branch exists
    select c.node_type, p.worker_name into n
        from pgq_node.node_info c, pgq_node.node_info p
        where p.queue_name = i_part_queue_name
          and c.queue_name = i_combined_queue_name
          and p.combined_queue = c.queue_name
          and p.node_type = 'leaf'
          and c.node_type = 'branch';
    if not found then
        select 201, 'Part-queue does not exist' into ret_code, ret_note;
        return;
    end if;

    update pgq_node.local_state
       set last_tick_id = i_watermark
     where queue_name = i_part_queue_name
       and consumer_name = n.worker_name;
    if not found then
        select 401, 'Worker registration not found' into ret_code, ret_note;
        return;
    end if;

    select 200, 'Ok' into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.set_partition_watermark(i_combined_queue_name text, i_part_queue_name text, i_watermark bigint, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: set_subscriber_watermark(text, text, bigint); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION set_subscriber_watermark(i_queue_name text, i_node_name text, i_watermark bigint, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.set_subscriber_watermark(3)
--
--      Notify provider about subscribers lowest watermark.
--
--      Called on provider at interval by each worker  
--
-- Parameters:
--      i_queue_name - cascaded queue name
--      i_node_name - subscriber node name
--      i_watermark - tick_id
--
-- Returns:
--      ret_code    - error code
--      ret_note    - description
-- ----------------------------------------------------------------------
declare
    n       record;
    wm_name text;
begin
    wm_name := '.' || i_node_name || '.watermark';
    select * into n from pgq.get_consumer_info(i_queue_name, wm_name);
    if not found then
        select 404, 'node '||i_node_name||' not subscribed to queue ', i_queue_name
            into ret_code, ret_note;
        return;
    end if;

    -- todo: check if wm sane?
    if i_watermark < n.last_tick then
        select 405, 'watermark must not be moved backwards'
            into ret_code, ret_note;
        return;
    elsif i_watermark = n.last_tick then
        select 100, 'watermark already set'
            into ret_code, ret_note;
        return;
    end if;

    perform pgq.register_consumer_at(i_queue_name, wm_name, i_watermark);

    select 200, wm_name || ' set to ' || i_watermark::text
        into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.set_subscriber_watermark(i_queue_name text, i_node_name text, i_watermark bigint, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: unregister_consumer(text, text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION unregister_consumer(i_queue_name text, i_consumer_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.unregister_consumer(2)
--
--      Unregister cascaded consumer from local node.
--
-- Parameters:
--      i_queue_name - cascaded queue name
--      i_consumer_name - cascaded consumer name
--
-- Returns:
--      ret_code - error code
--      200 - ok
--      404 - no such queue
--      ret_note - description
-- ----------------------------------------------------------------------
begin
    perform 1 from pgq_node.node_info where queue_name = i_queue_name
       for update;
    if not found then
        select 404, 'Unknown queue: ' || i_queue_name into ret_code, ret_note;
        return;
    end if;

    delete from pgq_node.local_state
      where queue_name = i_queue_name
        and consumer_name = i_consumer_name;

    select 200, 'Consumer '||i_consumer_name||' unregistered from '||i_queue_name
        into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.unregister_consumer(i_queue_name text, i_consumer_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: unregister_location(text, text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION unregister_location(i_queue_name text, i_node_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.unregister_location(2)
--
--      Drop unreferenced node.
--
-- Parameters:
--      i_queue_name - queue name
--      i_node_name - node to drop
--
-- Returns:
--      ret_code - error code
--      ret_note - error description
--
-- Return Codes:
--      200 - Ok
--      301 - Location not found
--      403 - Cannot drop nodes own or parent location
-- ----------------------------------------------------------------------
declare
    _queue_name  text;
    _wm_consumer text;
    _global_wm   bigint;
    sub          record;
    node         record;
begin
    select n.node_name, n.node_type, s.provider_node
        into node
        from pgq_node.node_info n
        left join pgq_node.local_state s
        on (s.consumer_name = n.worker_name
            and s.queue_name = n.queue_name)
        where n.queue_name = i_queue_name;
    if found then
        if node.node_name = i_node_name then
            select 403, 'Cannot drop nodes own location' into ret_code, ret_note;
            return;
        end if;
        if node.provider_node = i_node_name then
            select 403, 'Cannot drop location of nodes parent' into ret_code, ret_note;
            return;
        end if;
    end if;

    --
    -- There may be obsolete subscriptions around
    -- drop them silently.
    --
    perform pgq_node.unregister_subscriber(i_queue_name, i_node_name);

    --
    -- Actual removal
    --
    delete from pgq_node.node_location
     where queue_name = i_queue_name
       and node_name = i_node_name;

    if found then
        select 200, 'Ok' into ret_code, ret_note;
    else
        select 301, 'Location not found: ' || i_queue_name || '/' || i_node_name
          into ret_code, ret_note;
    end if;

    if node.node_type = 'root' then
        perform pgq.insert_event(i_queue_name, 'pgq.unregister-location',
                                 i_node_name, i_queue_name, null, null, null)
           from pgq_node.node_info n
         where n.queue_name = i_queue_name;
    end if;

    return;
end;
$$;


ALTER FUNCTION pgq_node.unregister_location(i_queue_name text, i_node_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: unregister_subscriber(text, text); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION unregister_subscriber(i_queue_name text, i_remote_node_name text, OUT ret_code integer, OUT ret_note text) RETURNS record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.unregister_subscriber(2)
--
--      Unsubscribe remote node from local node.
--
-- Parameters:
--      i_queue_name - set name
--      i_remote_node_name - node name
--
-- Returns:
--      ret_code - error code
--      ret_note - description
-- ----------------------------------------------------------------------
declare
    n_wm_name text;
    worker_name text;
begin
    n_wm_name := '.' || i_remote_node_name || '.watermark';
    select s.worker_name into worker_name from pgq_node.subscriber_info s
        where queue_name = i_queue_name and subscriber_node = i_remote_node_name;
    if not found then
        select 304, 'Subscriber not found' into ret_code, ret_note;
        return;
    end if;

    delete from pgq_node.subscriber_info
        where queue_name = i_queue_name
            and subscriber_node = i_remote_node_name;

    perform pgq.unregister_consumer(i_queue_name, n_wm_name);
    perform pgq.unregister_consumer(i_queue_name, worker_name);

    select 200, 'Subscriber unregistered: '||i_remote_node_name
        into ret_code, ret_note;
    return;
end;
$$;


ALTER FUNCTION pgq_node.unregister_subscriber(i_queue_name text, i_remote_node_name text, OUT ret_code integer, OUT ret_note text) OWNER TO postgres;

--
-- Name: upgrade_schema(); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION upgrade_schema() RETURNS integer
    LANGUAGE plpgsql
    AS $$
-- updates table structure if necessary
declare
    cnt int4 = 0;
begin
    -- node_info.node_attrs
    perform 1 from information_schema.columns
      where table_schema = 'pgq_node'
        and table_name = 'node_info'
        and column_name = 'node_attrs';
    if not found then
        alter table pgq_node.node_info add column node_attrs text;
        cnt := cnt + 1;
    end if;

    return cnt;
end;
$$;


ALTER FUNCTION pgq_node.upgrade_schema() OWNER TO postgres;

--
-- Name: version(); Type: FUNCTION; Schema: pgq_node; Owner: postgres
--

CREATE FUNCTION version() RETURNS text
    LANGUAGE plpgsql
    AS $$
-- ----------------------------------------------------------------------
-- Function: pgq_node.version(0)
--
--      Returns version string for pgq_node.  ATM it is based on SkyTools
--      version and only bumped when database code changes.
-- ----------------------------------------------------------------------
begin
    return '3.2.5';
end;
$$;


ALTER FUNCTION pgq_node.version() OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: check_address_update(); Type: FUNCTION; Schema: public; Owner: avito
--

CREATE FUNCTION check_address_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
user_rec record;
BEGIN
SELECT INTO user_rec *
FROM users
WHERE id = OLD.user_id AND status = '3';
IF NOT FOUND
THEN RETURN NULL; ELSE RETURN NEW; END IF;
END;
$$;


ALTER FUNCTION public.check_address_update() OWNER TO avito;

SET search_path = londiste, pg_catalog;

--
-- Name: applied_execute; Type: TABLE; Schema: londiste; Owner: postgres; Tablespace: 
--

CREATE TABLE applied_execute (
    queue_name text NOT NULL,
    execute_file text NOT NULL,
    execute_time timestamp with time zone DEFAULT now() NOT NULL,
    execute_sql text NOT NULL,
    execute_attrs text
);


ALTER TABLE applied_execute OWNER TO postgres;

--
-- Name: seq_info; Type: TABLE; Schema: londiste; Owner: postgres; Tablespace: 
--

CREATE TABLE seq_info (
    nr integer NOT NULL,
    queue_name text NOT NULL,
    seq_name text NOT NULL,
    local boolean DEFAULT false NOT NULL,
    last_value bigint NOT NULL
);


ALTER TABLE seq_info OWNER TO postgres;

--
-- Name: seq_info_nr_seq; Type: SEQUENCE; Schema: londiste; Owner: postgres
--

CREATE SEQUENCE seq_info_nr_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_info_nr_seq OWNER TO postgres;

--
-- Name: seq_info_nr_seq; Type: SEQUENCE OWNED BY; Schema: londiste; Owner: postgres
--

ALTER SEQUENCE seq_info_nr_seq OWNED BY seq_info.nr;


--
-- Name: table_info; Type: TABLE; Schema: londiste; Owner: postgres; Tablespace: 
--

CREATE TABLE table_info (
    nr integer NOT NULL,
    queue_name text NOT NULL,
    table_name text NOT NULL,
    local boolean DEFAULT false NOT NULL,
    merge_state text,
    custom_snapshot text,
    dropped_ddl text,
    table_attrs text,
    dest_table text,
    CONSTRAINT table_info_check CHECK (((dropped_ddl IS NULL) OR (merge_state = ANY (ARRAY['in-copy'::text, 'catching-up'::text]))))
);


ALTER TABLE table_info OWNER TO postgres;

--
-- Name: table_info_nr_seq; Type: SEQUENCE; Schema: londiste; Owner: postgres
--

CREATE SEQUENCE table_info_nr_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE table_info_nr_seq OWNER TO postgres;

--
-- Name: table_info_nr_seq; Type: SEQUENCE OWNED BY; Schema: londiste; Owner: postgres
--

ALTER SEQUENCE table_info_nr_seq OWNED BY table_info.nr;


SET search_path = pgq, pg_catalog;

--
-- Name: batch_id_seq; Type: SEQUENCE; Schema: pgq; Owner: postgres
--

CREATE SEQUENCE batch_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE batch_id_seq OWNER TO postgres;

--
-- Name: consumer; Type: TABLE; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE TABLE consumer (
    co_id integer NOT NULL,
    co_name text NOT NULL
);


ALTER TABLE consumer OWNER TO postgres;

--
-- Name: consumer_co_id_seq; Type: SEQUENCE; Schema: pgq; Owner: postgres
--

CREATE SEQUENCE consumer_co_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consumer_co_id_seq OWNER TO postgres;

--
-- Name: consumer_co_id_seq; Type: SEQUENCE OWNED BY; Schema: pgq; Owner: postgres
--

ALTER SEQUENCE consumer_co_id_seq OWNED BY consumer.co_id;


--
-- Name: event_template; Type: TABLE; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE TABLE event_template (
    ev_id bigint NOT NULL,
    ev_time timestamp with time zone NOT NULL,
    ev_txid bigint DEFAULT txid_current() NOT NULL,
    ev_owner integer,
    ev_retry integer,
    ev_type text,
    ev_data text,
    ev_extra1 text,
    ev_extra2 text,
    ev_extra3 text,
    ev_extra4 text
);


ALTER TABLE event_template OWNER TO postgres;

--
-- Name: event_1; Type: TABLE; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE TABLE event_1 (
)
INHERITS (event_template);


ALTER TABLE event_1 OWNER TO postgres;

--
-- Name: event_1_0; Type: TABLE; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE TABLE event_1_0 (
)
INHERITS (event_1)
WITH (fillfactor=100, autovacuum_enabled=off, toast.autovacuum_enabled=off);


ALTER TABLE event_1_0 OWNER TO postgres;

--
-- Name: event_1_1; Type: TABLE; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE TABLE event_1_1 (
)
INHERITS (event_1)
WITH (fillfactor=100, autovacuum_enabled=off, toast.autovacuum_enabled=off);


ALTER TABLE event_1_1 OWNER TO postgres;

--
-- Name: event_1_2; Type: TABLE; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE TABLE event_1_2 (
)
INHERITS (event_1)
WITH (fillfactor=100, autovacuum_enabled=off, toast.autovacuum_enabled=off);


ALTER TABLE event_1_2 OWNER TO postgres;

--
-- Name: event_1_id_seq; Type: SEQUENCE; Schema: pgq; Owner: postgres
--

CREATE SEQUENCE event_1_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE event_1_id_seq OWNER TO postgres;

--
-- Name: event_1_tick_seq; Type: SEQUENCE; Schema: pgq; Owner: postgres
--

CREATE SEQUENCE event_1_tick_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE event_1_tick_seq OWNER TO postgres;

--
-- Name: queue; Type: TABLE; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE TABLE queue (
    queue_id integer NOT NULL,
    queue_name text NOT NULL,
    queue_ntables integer DEFAULT 3 NOT NULL,
    queue_cur_table integer DEFAULT 0 NOT NULL,
    queue_rotation_period interval DEFAULT '02:00:00'::interval NOT NULL,
    queue_switch_step1 bigint DEFAULT txid_current() NOT NULL,
    queue_switch_step2 bigint DEFAULT txid_current(),
    queue_switch_time timestamp with time zone DEFAULT now() NOT NULL,
    queue_external_ticker boolean DEFAULT false NOT NULL,
    queue_disable_insert boolean DEFAULT false NOT NULL,
    queue_ticker_paused boolean DEFAULT false NOT NULL,
    queue_ticker_max_count integer DEFAULT 500 NOT NULL,
    queue_ticker_max_lag interval DEFAULT '00:00:03'::interval NOT NULL,
    queue_ticker_idle_period interval DEFAULT '00:01:00'::interval NOT NULL,
    queue_per_tx_limit integer,
    queue_data_pfx text NOT NULL,
    queue_event_seq text NOT NULL,
    queue_tick_seq text NOT NULL
);


ALTER TABLE queue OWNER TO postgres;

--
-- Name: queue_queue_id_seq; Type: SEQUENCE; Schema: pgq; Owner: postgres
--

CREATE SEQUENCE queue_queue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE queue_queue_id_seq OWNER TO postgres;

--
-- Name: queue_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: pgq; Owner: postgres
--

ALTER SEQUENCE queue_queue_id_seq OWNED BY queue.queue_id;


--
-- Name: retry_queue; Type: TABLE; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE TABLE retry_queue (
    ev_retry_after timestamp with time zone NOT NULL,
    ev_queue integer NOT NULL,
    ev_id bigint NOT NULL,
    ev_time timestamp with time zone NOT NULL,
    ev_txid bigint,
    ev_owner integer NOT NULL,
    ev_retry integer,
    ev_type text,
    ev_data text,
    ev_extra1 text,
    ev_extra2 text,
    ev_extra3 text,
    ev_extra4 text
);


ALTER TABLE retry_queue OWNER TO postgres;

--
-- Name: subscription; Type: TABLE; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE TABLE subscription (
    sub_id integer NOT NULL,
    sub_queue integer NOT NULL,
    sub_consumer integer NOT NULL,
    sub_last_tick bigint,
    sub_active timestamp with time zone DEFAULT now() NOT NULL,
    sub_batch bigint,
    sub_next_tick bigint
);


ALTER TABLE subscription OWNER TO postgres;

--
-- Name: subscription_sub_id_seq; Type: SEQUENCE; Schema: pgq; Owner: postgres
--

CREATE SEQUENCE subscription_sub_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE subscription_sub_id_seq OWNER TO postgres;

--
-- Name: subscription_sub_id_seq; Type: SEQUENCE OWNED BY; Schema: pgq; Owner: postgres
--

ALTER SEQUENCE subscription_sub_id_seq OWNED BY subscription.sub_id;


--
-- Name: tick; Type: TABLE; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE TABLE tick (
    tick_queue integer NOT NULL,
    tick_id bigint NOT NULL,
    tick_time timestamp with time zone DEFAULT now() NOT NULL,
    tick_snapshot txid_snapshot DEFAULT txid_current_snapshot() NOT NULL,
    tick_event_seq bigint NOT NULL
);


ALTER TABLE tick OWNER TO postgres;

SET search_path = pgq_ext, pg_catalog;

--
-- Name: completed_batch; Type: TABLE; Schema: pgq_ext; Owner: postgres; Tablespace: 
--

CREATE TABLE completed_batch (
    consumer_id text NOT NULL,
    subconsumer_id text DEFAULT ''::text NOT NULL,
    last_batch_id bigint NOT NULL
);


ALTER TABLE completed_batch OWNER TO postgres;

--
-- Name: completed_event; Type: TABLE; Schema: pgq_ext; Owner: postgres; Tablespace: 
--

CREATE TABLE completed_event (
    consumer_id text NOT NULL,
    subconsumer_id text DEFAULT ''::text NOT NULL,
    batch_id bigint NOT NULL,
    event_id bigint NOT NULL
);


ALTER TABLE completed_event OWNER TO postgres;

--
-- Name: completed_tick; Type: TABLE; Schema: pgq_ext; Owner: postgres; Tablespace: 
--

CREATE TABLE completed_tick (
    consumer_id text NOT NULL,
    subconsumer_id text DEFAULT ''::text NOT NULL,
    last_tick_id bigint NOT NULL
);


ALTER TABLE completed_tick OWNER TO postgres;

--
-- Name: partial_batch; Type: TABLE; Schema: pgq_ext; Owner: postgres; Tablespace: 
--

CREATE TABLE partial_batch (
    consumer_id text NOT NULL,
    subconsumer_id text DEFAULT ''::text NOT NULL,
    cur_batch_id bigint NOT NULL
);


ALTER TABLE partial_batch OWNER TO postgres;

SET search_path = pgq_node, pg_catalog;

--
-- Name: local_state; Type: TABLE; Schema: pgq_node; Owner: postgres; Tablespace: 
--

CREATE TABLE local_state (
    queue_name text NOT NULL,
    consumer_name text NOT NULL,
    provider_node text NOT NULL,
    last_tick_id bigint NOT NULL,
    cur_error text,
    paused boolean DEFAULT false NOT NULL,
    uptodate boolean DEFAULT false NOT NULL
);


ALTER TABLE local_state OWNER TO postgres;

--
-- Name: node_info; Type: TABLE; Schema: pgq_node; Owner: postgres; Tablespace: 
--

CREATE TABLE node_info (
    queue_name text NOT NULL,
    node_type text NOT NULL,
    node_name text NOT NULL,
    worker_name text,
    combined_queue text,
    node_attrs text,
    CONSTRAINT node_info_check CHECK (
CASE
    WHEN (node_type = 'root'::text) THEN ((worker_name IS NOT NULL) AND (combined_queue IS NULL))
    WHEN (node_type = 'branch'::text) THEN ((worker_name IS NOT NULL) AND (combined_queue IS NULL))
    WHEN (node_type = 'leaf'::text) THEN (worker_name IS NOT NULL)
    ELSE false
END),
    CONSTRAINT node_info_node_type_check CHECK ((node_type = ANY (ARRAY['root'::text, 'branch'::text, 'leaf'::text])))
);


ALTER TABLE node_info OWNER TO postgres;

--
-- Name: node_location; Type: TABLE; Schema: pgq_node; Owner: postgres; Tablespace: 
--

CREATE TABLE node_location (
    queue_name text NOT NULL,
    node_name text NOT NULL,
    node_location text NOT NULL,
    dead boolean DEFAULT false NOT NULL
);


ALTER TABLE node_location OWNER TO postgres;

--
-- Name: subscriber_info; Type: TABLE; Schema: pgq_node; Owner: postgres; Tablespace: 
--

CREATE TABLE subscriber_info (
    queue_name text NOT NULL,
    subscriber_node text NOT NULL,
    worker_name text NOT NULL,
    watermark_name text NOT NULL
);


ALTER TABLE subscriber_info OWNER TO postgres;

SET search_path = public, pg_catalog;

SET default_with_oids = true;

--
-- Name: addresses; Type: TABLE; Schema: public; Owner: avito; Tablespace: 
--

CREATE TABLE addresses (
    user_id integer,
    address text NOT NULL,
    id integer NOT NULL
);


ALTER TABLE addresses OWNER TO avito;

--
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: avito
--

CREATE SEQUENCE addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE addresses_id_seq OWNER TO avito;

--
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: avito
--

ALTER SEQUENCE addresses_id_seq OWNED BY addresses.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: avito; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    phone character varying(100),
    email character varying(100),
    status avito_status DEFAULT '2'::avito_status NOT NULL
);


ALTER TABLE users OWNER TO avito;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: avito
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO avito;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: avito
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


SET search_path = londiste, pg_catalog;

--
-- Name: nr; Type: DEFAULT; Schema: londiste; Owner: postgres
--

ALTER TABLE ONLY seq_info ALTER COLUMN nr SET DEFAULT nextval('seq_info_nr_seq'::regclass);


--
-- Name: nr; Type: DEFAULT; Schema: londiste; Owner: postgres
--

ALTER TABLE ONLY table_info ALTER COLUMN nr SET DEFAULT nextval('table_info_nr_seq'::regclass);


SET search_path = pgq, pg_catalog;

--
-- Name: co_id; Type: DEFAULT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY consumer ALTER COLUMN co_id SET DEFAULT nextval('consumer_co_id_seq'::regclass);


--
-- Name: ev_txid; Type: DEFAULT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY event_1 ALTER COLUMN ev_txid SET DEFAULT txid_current();


--
-- Name: ev_id; Type: DEFAULT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY event_1_0 ALTER COLUMN ev_id SET DEFAULT nextval('event_1_id_seq'::regclass);


--
-- Name: ev_txid; Type: DEFAULT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY event_1_0 ALTER COLUMN ev_txid SET DEFAULT txid_current();


--
-- Name: ev_id; Type: DEFAULT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY event_1_1 ALTER COLUMN ev_id SET DEFAULT nextval('event_1_id_seq'::regclass);


--
-- Name: ev_txid; Type: DEFAULT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY event_1_1 ALTER COLUMN ev_txid SET DEFAULT txid_current();


--
-- Name: ev_id; Type: DEFAULT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY event_1_2 ALTER COLUMN ev_id SET DEFAULT nextval('event_1_id_seq'::regclass);


--
-- Name: ev_txid; Type: DEFAULT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY event_1_2 ALTER COLUMN ev_txid SET DEFAULT txid_current();


--
-- Name: queue_id; Type: DEFAULT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY queue ALTER COLUMN queue_id SET DEFAULT nextval('queue_queue_id_seq'::regclass);


--
-- Name: sub_id; Type: DEFAULT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY subscription ALTER COLUMN sub_id SET DEFAULT nextval('subscription_sub_id_seq'::regclass);


SET search_path = public, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: avito
--

ALTER TABLE ONLY addresses ALTER COLUMN id SET DEFAULT nextval('addresses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: avito
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


SET search_path = londiste, pg_catalog;

--
-- Data for Name: applied_execute; Type: TABLE DATA; Schema: londiste; Owner: postgres
--

COPY applied_execute (queue_name, execute_file, execute_time, execute_sql, execute_attrs) FROM stdin;
\.


--
-- Data for Name: pending_fkeys; Type: TABLE DATA; Schema: londiste; Owner: postgres
--

COPY pending_fkeys (from_table, to_table, fkey_name, fkey_def) FROM stdin;
\.


--
-- Data for Name: seq_info; Type: TABLE DATA; Schema: londiste; Owner: postgres
--

COPY seq_info (nr, queue_name, seq_name, local, last_value) FROM stdin;
\.


--
-- Name: seq_info_nr_seq; Type: SEQUENCE SET; Schema: londiste; Owner: postgres
--

SELECT pg_catalog.setval('seq_info_nr_seq', 1, false);


--
-- Data for Name: table_info; Type: TABLE DATA; Schema: londiste; Owner: postgres
--

COPY table_info (nr, queue_name, table_name, local, merge_state, custom_snapshot, dropped_ddl, table_attrs, dest_table) FROM stdin;
1	avito_unique_addresses	public.addresses	t	ok	\N	\N	\N	\N
\.


--
-- Name: table_info_nr_seq; Type: SEQUENCE SET; Schema: londiste; Owner: postgres
--

SELECT pg_catalog.setval('table_info_nr_seq', 1, true);


SET search_path = pgq, pg_catalog;

--
-- Name: batch_id_seq; Type: SEQUENCE SET; Schema: pgq; Owner: postgres
--

SELECT pg_catalog.setval('batch_id_seq', 3448, true);


--
-- Data for Name: consumer; Type: TABLE DATA; Schema: pgq; Owner: postgres
--

COPY consumer (co_id, co_name) FROM stdin;
1	.global_watermark
2	avito_rconsumer
\.


--
-- Name: consumer_co_id_seq; Type: SEQUENCE SET; Schema: pgq; Owner: postgres
--

SELECT pg_catalog.setval('consumer_co_id_seq', 2, true);


--
-- Data for Name: event_1; Type: TABLE DATA; Schema: pgq; Owner: postgres
--

COPY event_1 (ev_id, ev_time, ev_txid, ev_owner, ev_retry, ev_type, ev_data, ev_extra1, ev_extra2, ev_extra3, ev_extra4) FROM stdin;
\.


--
-- Data for Name: event_1_0; Type: TABLE DATA; Schema: pgq; Owner: postgres
--

COPY event_1_0 (ev_id, ev_time, ev_txid, ev_owner, ev_retry, ev_type, ev_data, ev_extra1, ev_extra2, ev_extra3, ev_extra4) FROM stdin;
827	2015-10-07 11:01:32.898735+08	892318	\N	\N	pgq.global-watermark	3264	avito_unique_addresses	\N	\N	\N
828	2015-10-07 11:01:38.108339+08	892347	\N	\N	pgq.global-watermark	3265	avito_unique_addresses	\N	\N	\N
829	2015-10-07 11:06:46.6525+08	893969	\N	\N	pgq.global-watermark	3272	avito_unique_addresses	\N	\N	\N
830	2015-10-07 11:11:33.037161+08	895459	\N	\N	pgq.global-watermark	3277	avito_unique_addresses	\N	\N	\N
831	2015-10-07 11:11:55.202725+08	895590	\N	\N	pgq.global-watermark	3278	avito_unique_addresses	\N	\N	\N
832	2015-10-07 11:17:03.719492+08	897194	\N	\N	pgq.global-watermark	3284	avito_unique_addresses	\N	\N	\N
833	2015-10-07 11:22:12.227717+08	898814	\N	\N	pgq.global-watermark	3290	avito_unique_addresses	\N	\N	\N
834	2015-10-07 11:27:20.734399+08	900423	\N	\N	pgq.global-watermark	3296	avito_unique_addresses	\N	\N	\N
835	2015-10-07 11:32:29.289387+08	902045	\N	\N	pgq.global-watermark	3302	avito_unique_addresses	\N	\N	\N
836	2015-10-07 11:37:33.411061+08	903629	\N	\N	pgq.global-watermark	3308	avito_unique_addresses	\N	\N	\N
837	2015-10-07 11:37:37.769231+08	903658	\N	\N	pgq.global-watermark	3309	avito_unique_addresses	\N	\N	\N
838	2015-10-07 11:42:46.297657+08	905280	\N	\N	pgq.global-watermark	3316	avito_unique_addresses	\N	\N	\N
839	2015-10-07 11:47:33.571399+08	906775	\N	\N	pgq.global-watermark	3321	avito_unique_addresses	\N	\N	\N
840	2015-10-07 11:47:54.830601+08	906901	\N	\N	pgq.global-watermark	3322	avito_unique_addresses	\N	\N	\N
841	2015-10-07 11:53:03.365793+08	908506	\N	\N	pgq.global-watermark	3328	avito_unique_addresses	\N	\N	\N
842	2015-10-07 11:58:11.861758+08	910126	\N	\N	pgq.global-watermark	3334	avito_unique_addresses	\N	\N	\N
843	2015-10-07 12:03:20.411777+08	911735	\N	\N	pgq.global-watermark	3340	avito_unique_addresses	\N	\N	\N
844	2015-10-07 12:08:29.064781+08	913356	\N	\N	pgq.global-watermark	3346	avito_unique_addresses	\N	\N	\N
845	2015-10-07 12:13:34.077116+08	914947	\N	\N	pgq.global-watermark	3352	avito_unique_addresses	\N	\N	\N
846	2015-10-07 12:13:37.782134+08	914971	\N	\N	pgq.global-watermark	3353	avito_unique_addresses	\N	\N	\N
847	2015-10-07 12:18:46.284241+08	916601	\N	\N	pgq.global-watermark	3360	avito_unique_addresses	\N	\N	\N
848	2015-10-07 12:23:34.25262+08	918112	\N	\N	pgq.global-watermark	3368	avito_unique_addresses	\N	\N	\N
849	2015-10-07 12:23:54.779829+08	918232	\N	\N	pgq.global-watermark	3369	avito_unique_addresses	\N	\N	\N
850	2015-10-07 12:29:03.322226+08	919843	\N	\N	pgq.global-watermark	3375	avito_unique_addresses	\N	\N	\N
851	2015-10-07 12:34:11.891951+08	921463	\N	\N	pgq.global-watermark	3381	avito_unique_addresses	\N	\N	\N
852	2015-10-07 12:39:20.412676+08	923072	\N	\N	pgq.global-watermark	3387	avito_unique_addresses	\N	\N	\N
853	2015-10-07 12:44:28.907161+08	924692	\N	\N	pgq.global-watermark	3393	avito_unique_addresses	\N	\N	\N
854	2015-10-07 12:49:34.638413+08	926292	\N	\N	pgq.global-watermark	3400	avito_unique_addresses	\N	\N	\N
855	2015-10-07 12:49:37.410783+08	926307	\N	\N	pgq.global-watermark	3401	avito_unique_addresses	\N	\N	\N
856	2015-10-07 12:54:45.933291+08	927934	\N	\N	pgq.global-watermark	3409	avito_unique_addresses	\N	\N	\N
857	2015-10-07 12:57:10.331789+08	928689	\N	\N	U:id	user_id=2&address=1980+Northern+Boulevard+Americana+Manhasset+Manhasset%2c+NY+11030+United+States&id=28	public.addresses	user_id=2&address=1980+Northern+Boulevard+Americana+Manhasset+Manhasset%2c+NY+11030+United+States&id=28	\N	\N
\.


--
-- Data for Name: event_1_1; Type: TABLE DATA; Schema: pgq; Owner: postgres
--

COPY event_1_1 (ev_id, ev_time, ev_txid, ev_owner, ev_retry, ev_type, ev_data, ev_extra1, ev_extra2, ev_extra3, ev_extra4) FROM stdin;
858	2015-10-07 12:59:02.590054+08	929281	\N	\N	D:id	user_id=2&address=101+Riverside+Square+The+Shops+at+Riverside+Hackensack%2c+NJ+07601+United+States&id=27	public.addresses	\N	\N	\N
859	2015-10-07 12:59:34.801163+08	929449	\N	\N	pgq.global-watermark	3415	avito_unique_addresses	\N	\N	\N
860	2015-10-07 12:59:54.443182+08	929564	\N	\N	pgq.global-watermark	3416	avito_unique_addresses	\N	\N	\N
\.


--
-- Data for Name: event_1_2; Type: TABLE DATA; Schema: pgq; Owner: postgres
--

COPY event_1_2 (ev_id, ev_time, ev_txid, ev_owner, ev_retry, ev_type, ev_data, ev_extra1, ev_extra2, ev_extra3, ev_extra4) FROM stdin;
797	2015-10-07 08:58:13.321653+08	853529	\N	\N	pgq.global-watermark	3105	avito_unique_addresses	\N	\N	\N
798	2015-10-07 09:03:21.820181+08	855139	\N	\N	pgq.global-watermark	3111	avito_unique_addresses	\N	\N	\N
799	2015-10-07 09:08:30.39421+08	856759	\N	\N	pgq.global-watermark	3117	avito_unique_addresses	\N	\N	\N
800	2015-10-07 09:13:31.285973+08	858331	\N	\N	pgq.global-watermark	3124	avito_unique_addresses	\N	\N	\N
801	2015-10-07 09:13:38.939538+08	858381	\N	\N	pgq.global-watermark	3125	avito_unique_addresses	\N	\N	\N
802	2015-10-07 09:18:47.444125+08	859998	\N	\N	pgq.global-watermark	3132	avito_unique_addresses	\N	\N	\N
803	2015-10-07 09:23:31.429637+08	861480	\N	\N	pgq.global-watermark	3138	avito_unique_addresses	\N	\N	\N
804	2015-10-07 09:23:55.97494+08	861622	\N	\N	pgq.global-watermark	3141	avito_unique_addresses	\N	\N	\N
805	2015-10-07 09:29:04.565906+08	863238	\N	\N	pgq.global-watermark	3149	avito_unique_addresses	\N	\N	\N
806	2015-10-07 09:34:13.105599+08	864858	\N	\N	pgq.global-watermark	3155	avito_unique_addresses	\N	\N	\N
807	2015-10-07 09:39:21.618569+08	866468	\N	\N	pgq.global-watermark	3161	avito_unique_addresses	\N	\N	\N
808	2015-10-07 09:44:30.129683+08	868091	\N	\N	pgq.global-watermark	3168	avito_unique_addresses	\N	\N	\N
809	2015-10-07 09:49:31.819899+08	869666	\N	\N	pgq.global-watermark	3174	avito_unique_addresses	\N	\N	\N
810	2015-10-07 09:49:38.720737+08	869705	\N	\N	pgq.global-watermark	3175	avito_unique_addresses	\N	\N	\N
811	2015-10-07 09:54:47.231258+08	871328	\N	\N	pgq.global-watermark	3182	avito_unique_addresses	\N	\N	\N
812	2015-10-07 09:59:31.972349+08	872812	\N	\N	pgq.global-watermark	3187	avito_unique_addresses	\N	\N	\N
813	2015-10-07 09:59:55.764239+08	872948	\N	\N	pgq.global-watermark	3188	avito_unique_addresses	\N	\N	\N
814	2015-10-07 10:05:04.261773+08	874552	\N	\N	pgq.global-watermark	3194	avito_unique_addresses	\N	\N	\N
815	2015-10-07 10:10:12.80788+08	876174	\N	\N	pgq.global-watermark	3200	avito_unique_addresses	\N	\N	\N
816	2015-10-07 10:15:21.324302+08	877784	\N	\N	pgq.global-watermark	3206	avito_unique_addresses	\N	\N	\N
817	2015-10-07 10:20:29.854271+08	879407	\N	\N	pgq.global-watermark	3213	avito_unique_addresses	\N	\N	\N
818	2015-10-07 10:25:32.345693+08	880989	\N	\N	pgq.global-watermark	3220	avito_unique_addresses	\N	\N	\N
819	2015-10-07 10:25:38.372274+08	881023	\N	\N	pgq.global-watermark	3221	avito_unique_addresses	\N	\N	\N
820	2015-10-07 10:30:46.94742+08	882646	\N	\N	pgq.global-watermark	3228	avito_unique_addresses	\N	\N	\N
821	2015-10-07 10:35:32.487564+08	884143	\N	\N	pgq.global-watermark	3233	avito_unique_addresses	\N	\N	\N
822	2015-10-07 10:35:55.48019+08	884274	\N	\N	pgq.global-watermark	3234	avito_unique_addresses	\N	\N	\N
823	2015-10-07 10:41:04.003726+08	885878	\N	\N	pgq.global-watermark	3240	avito_unique_addresses	\N	\N	\N
824	2015-10-07 10:46:12.506813+08	887499	\N	\N	pgq.global-watermark	3246	avito_unique_addresses	\N	\N	\N
825	2015-10-07 10:51:21.066127+08	889110	\N	\N	pgq.global-watermark	3252	avito_unique_addresses	\N	\N	\N
826	2015-10-07 10:56:29.586557+08	890730	\N	\N	pgq.global-watermark	3258	avito_unique_addresses	\N	\N	\N
\.


--
-- Name: event_1_id_seq; Type: SEQUENCE SET; Schema: pgq; Owner: postgres
--

SELECT pg_catalog.setval('event_1_id_seq', 860, true);


--
-- Name: event_1_tick_seq; Type: SEQUENCE SET; Schema: pgq; Owner: postgres
--

SELECT pg_catalog.setval('event_1_tick_seq', 3417, true);


--
-- Data for Name: event_template; Type: TABLE DATA; Schema: pgq; Owner: postgres
--

COPY event_template (ev_id, ev_time, ev_txid, ev_owner, ev_retry, ev_type, ev_data, ev_extra1, ev_extra2, ev_extra3, ev_extra4) FROM stdin;
\.


--
-- Data for Name: queue; Type: TABLE DATA; Schema: pgq; Owner: postgres
--

COPY queue (queue_id, queue_name, queue_ntables, queue_cur_table, queue_rotation_period, queue_switch_step1, queue_switch_step2, queue_switch_time, queue_external_ticker, queue_disable_insert, queue_ticker_paused, queue_ticker_max_count, queue_ticker_max_lag, queue_ticker_idle_period, queue_per_tx_limit, queue_data_pfx, queue_event_seq, queue_tick_seq) FROM stdin;
1	avito_unique_addresses	3	1	02:00:00	928820	928822	2015-10-07 12:57:34.747068+08	f	f	f	500	00:00:03	00:01:00	\N	pgq.event_1	pgq.event_1_id_seq	pgq.event_1_tick_seq
\.


--
-- Name: queue_queue_id_seq; Type: SEQUENCE SET; Schema: pgq; Owner: postgres
--

SELECT pg_catalog.setval('queue_queue_id_seq', 1, true);


--
-- Data for Name: retry_queue; Type: TABLE DATA; Schema: pgq; Owner: postgres
--

COPY retry_queue (ev_retry_after, ev_queue, ev_id, ev_time, ev_txid, ev_owner, ev_retry, ev_type, ev_data, ev_extra1, ev_extra2, ev_extra3, ev_extra4) FROM stdin;
\.


--
-- Data for Name: subscription; Type: TABLE DATA; Schema: pgq; Owner: postgres
--

COPY subscription (sub_id, sub_queue, sub_consumer, sub_last_tick, sub_active, sub_batch, sub_next_tick) FROM stdin;
1	1	1	3416	2015-10-07 12:59:54.443182+08	\N	\N
2	1	2	3417	2015-10-07 12:59:55.784817+08	\N	\N
\.


--
-- Name: subscription_sub_id_seq; Type: SEQUENCE SET; Schema: pgq; Owner: postgres
--

SELECT pg_catalog.setval('subscription_sub_id_seq', 2, true);


--
-- Data for Name: tick; Type: TABLE DATA; Schema: pgq; Owner: postgres
--

COPY tick (tick_queue, tick_id, tick_time, tick_snapshot, tick_event_seq) FROM stdin;
1	3261	2015-10-07 10:58:30.408016+08	891363:891363:	826
1	3262	2015-10-07 10:59:30.546252+08	891674:891674:	826
1	3263	2015-10-07 11:00:30.673859+08	891995:891995:	826
1	3264	2015-10-07 11:01:30.809636+08	892300:892300:	826
1	3265	2015-10-07 11:01:33.818725+08	892322:892322:	827
1	3266	2015-10-07 11:01:38.828899+08	892352:892352:	828
1	3267	2015-10-07 11:01:49.848278+08	892418:892418:	828
1	3268	2015-10-07 11:02:11.897314+08	892536:892536:	828
1	3269	2015-10-07 11:02:55.998427+08	892760:892760:	828
1	3270	2015-10-07 11:03:56.132429+08	893082:893082:	828
1	3271	2015-10-07 11:04:56.253851+08	893387:893387:	828
1	3272	2015-10-07 11:05:56.382679+08	893708:893708:	828
1	3273	2015-10-07 11:06:47.493452+08	893971:893971:	829
1	3274	2015-10-07 11:07:47.631806+08	894291:894291:	829
1	3275	2015-10-07 11:08:47.760934+08	894599:894599:	829
1	3276	2015-10-07 11:09:47.887158+08	894919:894919:	829
1	3277	2015-10-07 11:10:48.017794+08	895227:895227:	829
1	3278	2015-10-07 11:11:33.107543+08	895460:895460:	830
1	3279	2015-10-07 11:11:56.153167+08	895593:895593:	831
1	3280	2015-10-07 11:12:42.256827+08	895830:895830:	831
1	3281	2015-10-07 11:13:42.387618+08	896144:896144:	831
1	3282	2015-10-07 11:14:42.51541+08	896457:896457:	831
1	3283	2015-10-07 11:15:42.644207+08	896772:896772:	831
1	3284	2015-10-07 11:16:42.777654+08	897086:897086:	831
1	3285	2015-10-07 11:17:03.818973+08	897196:897196:	832
1	3286	2015-10-07 11:17:45.908535+08	897420:897422:897420	832
1	3287	2015-10-07 11:18:46.047958+08	897732:897732:	832
1	3288	2015-10-07 11:19:46.164159+08	898051:898051:	832
1	3289	2015-10-07 11:20:46.289176+08	898360:898360:	832
1	3290	2015-10-07 11:21:46.41593+08	898679:898679:	832
1	3291	2015-10-07 11:22:12.476234+08	898817:898817:	833
1	3292	2015-10-07 11:23:04.602791+08	899083:899083:	833
1	3293	2015-10-07 11:24:04.737417+08	899404:899404:	833
1	3294	2015-10-07 11:25:04.865492+08	899713:899713:	833
1	3295	2015-10-07 11:26:05.000323+08	900033:900033:	833
1	3296	2015-10-07 11:27:05.123956+08	900340:900340:	833
1	3297	2015-10-07 11:27:21.163145+08	900425:900425:	834
1	3400	2015-10-07 12:49:21.488552+08	926219:926221:	853
1	3401	2015-10-07 12:49:35.519935+08	926293:926293:	854
1	3402	2015-10-07 12:49:38.528002+08	926318:926318:	855
1	3403	2015-10-07 12:49:45.541094+08	926360:926360:	855
1	3404	2015-10-07 12:49:59.569326+08	926439:926439:	855
1	3405	2015-10-07 12:50:27.628507+08	926583:926583:	855
1	3406	2015-10-07 12:51:23.757062+08	926874:926874:	855
1	3407	2015-10-07 12:52:23.884632+08	927194:927194:	855
1	3408	2015-10-07 12:53:24.021213+08	927502:927502:	855
1	3409	2015-10-07 12:54:24.150968+08	927822:927822:	855
1	3410	2015-10-07 12:54:46.198472+08	927936:927936:	856
1	3414	2015-10-07 12:58:10.651327+08	929017:929017:	857
1	3415	2015-10-07 12:59:02.765097+08	929282:929282:	858
1	3416	2015-10-07 12:59:34.836792+08	929450:929450:	859
1	3417	2015-10-07 12:59:54.878076+08	929566:929566:	860
1	3298	2015-10-07 11:27:53.251288+08	900602:900602:	834
1	3299	2015-10-07 11:28:53.358511+08	900915:900917:	834
1	3300	2015-10-07 11:29:53.486351+08	901232:901232:	834
1	3301	2015-10-07 11:30:53.614759+08	901542:901542:	834
1	3302	2015-10-07 11:31:53.74296+08	901861:901861:	834
1	3303	2015-10-07 11:32:29.821763+08	902047:902047:	835
1	3304	2015-10-07 11:33:29.949813+08	902355:902355:	835
1	3305	2015-10-07 11:34:30.090977+08	902677:902677:	835
1	3306	2015-10-07 11:35:30.218794+08	902985:902985:	835
1	3307	2015-10-07 11:36:30.34696+08	903302:903302:	835
1	3308	2015-10-07 11:37:30.472047+08	903613:903613:	835
1	3309	2015-10-07 11:37:33.480241+08	903634:903634:	836
1	3310	2015-10-07 11:37:38.490046+08	903662:903662:	837
1	3311	2015-10-07 11:37:48.512255+08	903727:903727:	837
1	3312	2015-10-07 11:38:09.547958+08	903833:903833:	837
1	3313	2015-10-07 11:38:51.637906+08	904050:904050:	837
1	3314	2015-10-07 11:39:51.754289+08	904371:904371:	837
1	3315	2015-10-07 11:40:51.875581+08	904680:904680:	837
1	3316	2015-10-07 11:41:52.012735+08	904999:904999:	837
1	3317	2015-10-07 11:42:47.139033+08	905282:905282:	838
1	3318	2015-10-07 11:43:47.271082+08	905601:905601:	838
1	3319	2015-10-07 11:44:47.398+08	905910:905910:	838
1	3320	2015-10-07 11:45:47.532463+08	906229:906229:	838
1	3321	2015-10-07 11:46:47.658213+08	906538:906538:	838
1	3322	2015-10-07 11:47:33.753126+08	906777:906777:	839
1	3323	2015-10-07 11:47:55.797854+08	906903:906903:	840
1	3324	2015-10-07 11:48:39.892193+08	907130:907130:	840
1	3325	2015-10-07 11:49:40.01553+08	907440:907440:	840
1	3326	2015-10-07 11:50:40.144764+08	907757:907761:907757	840
1	3411	2015-10-07 12:55:30.29478+08	928166:928166:	856
1	3412	2015-10-07 12:56:30.424073+08	928484:928484:	856
1	3413	2015-10-07 12:57:10.513338+08	928690:928690:	857
1	3327	2015-10-07 11:51:40.273684+08	908072:908072:	840
1	3328	2015-10-07 11:52:40.409577+08	908389:908389:	840
1	3329	2015-10-07 11:53:03.461077+08	908510:908510:	841
1	3330	2015-10-07 11:53:49.568249+08	908756:908756:	841
1	3331	2015-10-07 11:54:49.69903+08	909065:909065:	841
1	3332	2015-10-07 11:55:49.828412+08	909384:909384:	841
1	3333	2015-10-07 11:56:49.953905+08	909692:909692:	841
1	3334	2015-10-07 11:57:50.080931+08	910012:910012:	841
1	3335	2015-10-07 11:58:12.130461+08	910128:910128:	842
1	3336	2015-10-07 11:58:57.223951+08	910360:910360:	842
1	3337	2015-10-07 11:59:57.341599+08	910681:910681:	842
1	3338	2015-10-07 12:00:57.475998+08	910989:910989:	842
1	3339	2015-10-07 12:01:57.6031+08	911307:911307:	842
1	3340	2015-10-07 12:02:57.73915+08	911618:911618:	842
1	3341	2015-10-07 12:03:20.793128+08	911741:911741:	843
1	3342	2015-10-07 12:04:07.888496+08	911994:911994:	843
1	3343	2015-10-07 12:05:08.040788+08	912303:912303:	843
1	3344	2015-10-07 12:06:08.163931+08	912623:912623:	843
1	3345	2015-10-07 12:07:08.318211+08	912928:912928:	843
1	3346	2015-10-07 12:08:08.465731+08	913248:913248:	843
1	3347	2015-10-07 12:08:29.507174+08	913359:913359:	844
1	3348	2015-10-07 12:09:11.619519+08	913577:913577:	844
1	3349	2015-10-07 12:10:11.794039+08	913898:913898:	844
1	3350	2015-10-07 12:11:11.926607+08	914208:914208:	844
1	3351	2015-10-07 12:12:12.094405+08	914527:914527:	844
1	3352	2015-10-07 12:13:12.308625+08	914832:914832:	844
1	3353	2015-10-07 12:13:34.382612+08	914948:914948:	845
1	3354	2015-10-07 12:13:38.394017+08	914975:914975:	846
1	3355	2015-10-07 12:13:47.414709+08	915027:915027:	846
1	3356	2015-10-07 12:14:06.45072+08	915137:915137:	846
1	3357	2015-10-07 12:14:45.517829+08	915341:915341:	846
1	3358	2015-10-07 12:15:45.626634+08	915656:915656:	846
1	3359	2015-10-07 12:16:45.735759+08	915969:915969:	846
1	3360	2015-10-07 12:17:45.843281+08	916285:916285:	846
1	3361	2015-10-07 12:18:45.9622+08	916597:916597:	846
1	3362	2015-10-07 12:18:48.972073+08	916618:916618:	847
1	3363	2015-10-07 12:18:55.987106+08	916658:916658:	847
1	3364	2015-10-07 12:19:11.017209+08	916739:916739:	847
1	3365	2015-10-07 12:19:42.074855+08	916902:916902:	847
1	3366	2015-10-07 12:20:42.179148+08	917217:917217:	847
1	3367	2015-10-07 12:21:42.289515+08	917525:917525:	847
1	3368	2015-10-07 12:22:42.401861+08	917844:917844:	847
1	3369	2015-10-07 12:23:34.499631+08	918113:918113:	848
1	3370	2015-10-07 12:23:55.541944+08	918234:918234:	849
1	3371	2015-10-07 12:24:38.617397+08	918457:918457:	849
1	3372	2015-10-07 12:25:38.727054+08	918766:918766:	849
1	3373	2015-10-07 12:26:38.833546+08	919085:919085:	849
1	3374	2015-10-07 12:27:38.958825+08	919396:919396:	849
1	3375	2015-10-07 12:28:39.077296+08	919715:919717:	849
1	3376	2015-10-07 12:29:04.122238+08	919846:919848:	850
1	3377	2015-10-07 12:29:55.212943+08	920118:920118:	850
1	3378	2015-10-07 12:30:55.318374+08	920427:920427:	850
1	3379	2015-10-07 12:31:55.426908+08	920746:920746:	850
1	3380	2015-10-07 12:32:55.541318+08	921055:921055:	850
1	3381	2015-10-07 12:33:55.649857+08	921374:921374:	850
1	3382	2015-10-07 12:34:12.683075+08	921466:921466:	851
1	3383	2015-10-07 12:34:47.74439+08	921646:921646:	851
1	3384	2015-10-07 12:35:47.854059+08	921961:921961:	851
1	3385	2015-10-07 12:36:47.962558+08	922274:922274:	851
1	3386	2015-10-07 12:37:48.066903+08	922589:922589:	851
1	3387	2015-10-07 12:38:48.173779+08	922902:922902:	851
1	3388	2015-10-07 12:39:21.236023+08	923074:923074:	852
1	3389	2015-10-07 12:40:21.333901+08	923396:923396:	852
1	3390	2015-10-07 12:41:21.441292+08	923702:923702:	852
1	3391	2015-10-07 12:42:21.572325+08	924021:924021:	852
1	3392	2015-10-07 12:43:21.714229+08	924333:924333:	852
1	3393	2015-10-07 12:44:21.838221+08	924650:924650:	852
1	3394	2015-10-07 12:44:29.855705+08	924697:924697:	853
1	3395	2015-10-07 12:44:46.890993+08	924784:924784:	853
1	3396	2015-10-07 12:45:20.965423+08	924963:924963:	853
1	3397	2015-10-07 12:46:21.094361+08	925282:925282:	853
1	3398	2015-10-07 12:47:21.232909+08	925588:925588:	853
1	3399	2015-10-07 12:48:21.361654+08	925911:925911:	853
\.


SET search_path = pgq_ext, pg_catalog;

--
-- Data for Name: completed_batch; Type: TABLE DATA; Schema: pgq_ext; Owner: postgres
--

COPY completed_batch (consumer_id, subconsumer_id, last_batch_id) FROM stdin;
\.


--
-- Data for Name: completed_event; Type: TABLE DATA; Schema: pgq_ext; Owner: postgres
--

COPY completed_event (consumer_id, subconsumer_id, batch_id, event_id) FROM stdin;
\.


--
-- Data for Name: completed_tick; Type: TABLE DATA; Schema: pgq_ext; Owner: postgres
--

COPY completed_tick (consumer_id, subconsumer_id, last_tick_id) FROM stdin;
\.


--
-- Data for Name: partial_batch; Type: TABLE DATA; Schema: pgq_ext; Owner: postgres
--

COPY partial_batch (consumer_id, subconsumer_id, cur_batch_id) FROM stdin;
\.


SET search_path = pgq_node, pg_catalog;

--
-- Data for Name: local_state; Type: TABLE DATA; Schema: pgq_node; Owner: postgres
--

COPY local_state (queue_name, consumer_name, provider_node, last_tick_id, cur_error, paused, uptodate) FROM stdin;
avito_unique_addresses	avito_rconsumer	avito_addresses	3416	\N	f	t
\.


--
-- Data for Name: node_info; Type: TABLE DATA; Schema: pgq_node; Owner: postgres
--

COPY node_info (queue_name, node_type, node_name, worker_name, combined_queue, node_attrs) FROM stdin;
avito_unique_addresses	root	avito_addresses	avito_rconsumer	\N	\N
\.


--
-- Data for Name: node_location; Type: TABLE DATA; Schema: pgq_node; Owner: postgres
--

COPY node_location (queue_name, node_name, node_location, dead) FROM stdin;
avito_unique_addresses	avito_addresses	dbname=avito	f
\.


--
-- Data for Name: subscriber_info; Type: TABLE DATA; Schema: pgq_node; Owner: postgres
--

COPY subscriber_info (queue_name, subscriber_node, worker_name, watermark_name) FROM stdin;
\.


SET search_path = public, pg_catalog;

--
-- Data for Name: addresses; Type: TABLE DATA; Schema: public; Owner: avito
--

COPY addresses (user_id, address, id) FROM stdin;
1	Иркутск, Россия	29
2	1980 Northern Boulevard Americana Manhasset Manhasset, NY 11030 United States	28
2	New York City, New York, United States	23
2	727 5th Ave New York, NY 10022 United States	24
2	37 Wall St New York, NY 10005 United States	25
2	97 Greene St New York, NY 10012 United States	26
6	Moscow, Russia	30
3	Owensboro, Kentucky, United States	31
13	Beverly Hills, California, United States	32
9	Warsaw, Poland	33
5	Cleveland, Ohio, United States	34
14	Иркутск, Россия	35
10	Царское Село, Санкт-Петербургская губерния, Российская империя	36
11	Moscow, Russia	37
8	Paris, France	38
\.


--
-- Name: addresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: avito
--

SELECT pg_catalog.setval('addresses_id_seq', 38, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: avito
--

COPY users (id, name, phone, email, status) FROM stdin;
5	Phil Donahue	\N	\N	2
9	Осип Мандельштам	\N	\N	2
6	Владимир Семенович Высоцкий	\N	\N	2
10	Николай Александрович Романов	\N	\N	3
3	Jack Sparrow	\N	\N	2
11	Марина Ивановна Цветаева	\N	\N	2
8	Константин Бальмонт	\N	\N	2
1	Антон Патес	\N	\N	3
2	Tiffany Maxwell	\N	\N	3
13	Ray Charles	\N	\N	2
14	Аркадий Гайдар	\N	\N	2
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: avito
--

SELECT pg_catalog.setval('users_id_seq', 14, true);


SET search_path = londiste, pg_catalog;

--
-- Name: applied_execute_pkey; Type: CONSTRAINT; Schema: londiste; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY applied_execute
    ADD CONSTRAINT applied_execute_pkey PRIMARY KEY (execute_file);


--
-- Name: pending_fkeys_pkey; Type: CONSTRAINT; Schema: londiste; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pending_fkeys
    ADD CONSTRAINT pending_fkeys_pkey PRIMARY KEY (from_table, fkey_name);


--
-- Name: seq_info_pkey; Type: CONSTRAINT; Schema: londiste; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY seq_info
    ADD CONSTRAINT seq_info_pkey PRIMARY KEY (queue_name, seq_name);


--
-- Name: table_info_pkey; Type: CONSTRAINT; Schema: londiste; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY table_info
    ADD CONSTRAINT table_info_pkey PRIMARY KEY (queue_name, table_name);


SET search_path = pgq, pg_catalog;

--
-- Name: consumer_name_uq; Type: CONSTRAINT; Schema: pgq; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY consumer
    ADD CONSTRAINT consumer_name_uq UNIQUE (co_name);


--
-- Name: consumer_pkey; Type: CONSTRAINT; Schema: pgq; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY consumer
    ADD CONSTRAINT consumer_pkey PRIMARY KEY (co_id);


--
-- Name: queue_name_uq; Type: CONSTRAINT; Schema: pgq; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT queue_name_uq UNIQUE (queue_name);


--
-- Name: queue_pkey; Type: CONSTRAINT; Schema: pgq; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT queue_pkey PRIMARY KEY (queue_id);


--
-- Name: rq_pkey; Type: CONSTRAINT; Schema: pgq; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY retry_queue
    ADD CONSTRAINT rq_pkey PRIMARY KEY (ev_owner, ev_id);


--
-- Name: subscription_batch_idx; Type: CONSTRAINT; Schema: pgq; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subscription
    ADD CONSTRAINT subscription_batch_idx UNIQUE (sub_batch);


--
-- Name: subscription_pkey; Type: CONSTRAINT; Schema: pgq; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subscription
    ADD CONSTRAINT subscription_pkey PRIMARY KEY (sub_queue, sub_consumer);


--
-- Name: tick_pkey; Type: CONSTRAINT; Schema: pgq; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tick
    ADD CONSTRAINT tick_pkey PRIMARY KEY (tick_queue, tick_id);


SET search_path = pgq_ext, pg_catalog;

--
-- Name: completed_batch_pkey; Type: CONSTRAINT; Schema: pgq_ext; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY completed_batch
    ADD CONSTRAINT completed_batch_pkey PRIMARY KEY (consumer_id, subconsumer_id);


--
-- Name: completed_event_pkey; Type: CONSTRAINT; Schema: pgq_ext; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY completed_event
    ADD CONSTRAINT completed_event_pkey PRIMARY KEY (consumer_id, subconsumer_id, batch_id, event_id);


--
-- Name: completed_tick_pkey; Type: CONSTRAINT; Schema: pgq_ext; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY completed_tick
    ADD CONSTRAINT completed_tick_pkey PRIMARY KEY (consumer_id, subconsumer_id);


--
-- Name: partial_batch_pkey; Type: CONSTRAINT; Schema: pgq_ext; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY partial_batch
    ADD CONSTRAINT partial_batch_pkey PRIMARY KEY (consumer_id, subconsumer_id);


SET search_path = pgq_node, pg_catalog;

--
-- Name: local_state_pkey; Type: CONSTRAINT; Schema: pgq_node; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY local_state
    ADD CONSTRAINT local_state_pkey PRIMARY KEY (queue_name, consumer_name);


--
-- Name: node_info_pkey; Type: CONSTRAINT; Schema: pgq_node; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY node_info
    ADD CONSTRAINT node_info_pkey PRIMARY KEY (queue_name);


--
-- Name: node_location_pkey; Type: CONSTRAINT; Schema: pgq_node; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY node_location
    ADD CONSTRAINT node_location_pkey PRIMARY KEY (queue_name, node_name);


--
-- Name: subscriber_info_pkey; Type: CONSTRAINT; Schema: pgq_node; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subscriber_info
    ADD CONSTRAINT subscriber_info_pkey PRIMARY KEY (queue_name, subscriber_node);


SET search_path = public, pg_catalog;

--
-- Name: addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: avito; Tablespace: 
--

ALTER TABLE ONLY addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: avito; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


SET search_path = pgq, pg_catalog;

--
-- Name: event_1_0_txid_idx; Type: INDEX; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE INDEX event_1_0_txid_idx ON event_1_0 USING btree (ev_txid);


--
-- Name: event_1_1_txid_idx; Type: INDEX; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE INDEX event_1_1_txid_idx ON event_1_1 USING btree (ev_txid);


--
-- Name: event_1_2_txid_idx; Type: INDEX; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE INDEX event_1_2_txid_idx ON event_1_2 USING btree (ev_txid);


--
-- Name: rq_retry_idx; Type: INDEX; Schema: pgq; Owner: postgres; Tablespace: 
--

CREATE INDEX rq_retry_idx ON retry_queue USING btree (ev_retry_after);


SET search_path = londiste, pg_catalog;

--
-- Name: table_info_trigger_sync; Type: TRIGGER; Schema: londiste; Owner: postgres
--

CREATE TRIGGER table_info_trigger_sync BEFORE DELETE ON table_info FOR EACH ROW EXECUTE PROCEDURE table_info_trigger();


SET search_path = public, pg_catalog;

--
-- Name: _londiste_avito_unique_addresses; Type: TRIGGER; Schema: public; Owner: avito
--

CREATE TRIGGER _londiste_avito_unique_addresses AFTER INSERT OR DELETE ON addresses FOR EACH ROW EXECUTE PROCEDURE pgq.logutriga('avito_unique_addresses');


--
-- Name: _londiste_avito_unique_addresses_truncate; Type: TRIGGER; Schema: public; Owner: avito
--

CREATE TRIGGER _londiste_avito_unique_addresses_truncate AFTER TRUNCATE ON addresses FOR EACH STATEMENT EXECUTE PROCEDURE pgq.sqltriga('avito_unique_addresses');


--
-- Name: _londiste_avito_unique_addresses_update; Type: TRIGGER; Schema: public; Owner: avito
--

CREATE TRIGGER _londiste_avito_unique_addresses_update AFTER UPDATE ON addresses FOR EACH ROW EXECUTE PROCEDURE pgq.logutriga('avito_unique_addresses', 'backup');


--
-- Name: check_update; Type: TRIGGER; Schema: public; Owner: avito
--

CREATE TRIGGER check_update BEFORE UPDATE ON addresses FOR EACH ROW WHEN ((old.address IS DISTINCT FROM new.address)) EXECUTE PROCEDURE check_address_update();


SET search_path = londiste, pg_catalog;

--
-- Name: seq_info_queue_name_fkey; Type: FK CONSTRAINT; Schema: londiste; Owner: postgres
--

ALTER TABLE ONLY seq_info
    ADD CONSTRAINT seq_info_queue_name_fkey FOREIGN KEY (queue_name) REFERENCES pgq_node.node_info(queue_name) ON DELETE CASCADE;


--
-- Name: table_info_queue_name_fkey; Type: FK CONSTRAINT; Schema: londiste; Owner: postgres
--

ALTER TABLE ONLY table_info
    ADD CONSTRAINT table_info_queue_name_fkey FOREIGN KEY (queue_name) REFERENCES pgq_node.node_info(queue_name) ON DELETE CASCADE;


SET search_path = pgq, pg_catalog;

--
-- Name: rq_queue_id_fkey; Type: FK CONSTRAINT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY retry_queue
    ADD CONSTRAINT rq_queue_id_fkey FOREIGN KEY (ev_queue) REFERENCES queue(queue_id);


--
-- Name: sub_consumer_fkey; Type: FK CONSTRAINT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY subscription
    ADD CONSTRAINT sub_consumer_fkey FOREIGN KEY (sub_consumer) REFERENCES consumer(co_id);


--
-- Name: sub_queue_fkey; Type: FK CONSTRAINT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY subscription
    ADD CONSTRAINT sub_queue_fkey FOREIGN KEY (sub_queue) REFERENCES queue(queue_id);


--
-- Name: tick_queue_fkey; Type: FK CONSTRAINT; Schema: pgq; Owner: postgres
--

ALTER TABLE ONLY tick
    ADD CONSTRAINT tick_queue_fkey FOREIGN KEY (tick_queue) REFERENCES queue(queue_id);


SET search_path = pgq_node, pg_catalog;

--
-- Name: local_state_queue_name_fkey; Type: FK CONSTRAINT; Schema: pgq_node; Owner: postgres
--

ALTER TABLE ONLY local_state
    ADD CONSTRAINT local_state_queue_name_fkey FOREIGN KEY (queue_name) REFERENCES node_info(queue_name);


--
-- Name: local_state_queue_name_fkey1; Type: FK CONSTRAINT; Schema: pgq_node; Owner: postgres
--

ALTER TABLE ONLY local_state
    ADD CONSTRAINT local_state_queue_name_fkey1 FOREIGN KEY (queue_name, provider_node) REFERENCES node_location(queue_name, node_name);


--
-- Name: node_info_queue_name_fkey; Type: FK CONSTRAINT; Schema: pgq_node; Owner: postgres
--

ALTER TABLE ONLY node_info
    ADD CONSTRAINT node_info_queue_name_fkey FOREIGN KEY (queue_name, node_name) REFERENCES node_location(queue_name, node_name);


--
-- Name: subscriber_info_queue_name_fkey; Type: FK CONSTRAINT; Schema: pgq_node; Owner: postgres
--

ALTER TABLE ONLY subscriber_info
    ADD CONSTRAINT subscriber_info_queue_name_fkey FOREIGN KEY (queue_name) REFERENCES node_info(queue_name);


--
-- Name: subscriber_info_queue_name_fkey1; Type: FK CONSTRAINT; Schema: pgq_node; Owner: postgres
--

ALTER TABLE ONLY subscriber_info
    ADD CONSTRAINT subscriber_info_queue_name_fkey1 FOREIGN KEY (queue_name, subscriber_node) REFERENCES node_location(queue_name, node_name);


--
-- Name: subscriber_info_watermark_name_fkey; Type: FK CONSTRAINT; Schema: pgq_node; Owner: postgres
--

ALTER TABLE ONLY subscriber_info
    ADD CONSTRAINT subscriber_info_watermark_name_fkey FOREIGN KEY (watermark_name) REFERENCES pgq.consumer(co_name);


--
-- Name: subscriber_info_worker_name_fkey; Type: FK CONSTRAINT; Schema: pgq_node; Owner: postgres
--

ALTER TABLE ONLY subscriber_info
    ADD CONSTRAINT subscriber_info_worker_name_fkey FOREIGN KEY (worker_name) REFERENCES pgq.consumer(co_name);


SET search_path = public, pg_catalog;

--
-- Name: addresses_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: avito
--

ALTER TABLE ONLY addresses
    ADD CONSTRAINT addresses_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: londiste; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA londiste FROM PUBLIC;
REVOKE ALL ON SCHEMA londiste FROM postgres;
GRANT ALL ON SCHEMA londiste TO postgres;
GRANT USAGE ON SCHEMA londiste TO PUBLIC;


--
-- Name: pgq; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA pgq FROM PUBLIC;
REVOKE ALL ON SCHEMA pgq FROM postgres;
GRANT ALL ON SCHEMA pgq TO postgres;
GRANT USAGE ON SCHEMA pgq TO PUBLIC;


--
-- Name: pgq_ext; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA pgq_ext FROM PUBLIC;
REVOKE ALL ON SCHEMA pgq_ext FROM postgres;
GRANT ALL ON SCHEMA pgq_ext TO postgres;
GRANT USAGE ON SCHEMA pgq_ext TO PUBLIC;


--
-- Name: pgq_node; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA pgq_node FROM PUBLIC;
REVOKE ALL ON SCHEMA pgq_node FROM postgres;
GRANT ALL ON SCHEMA pgq_node TO postgres;
GRANT USAGE ON SCHEMA pgq_node TO PUBLIC;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


SET search_path = londiste, pg_catalog;

--
-- Name: pending_fkeys; Type: ACL; Schema: londiste; Owner: postgres
--

REVOKE ALL ON TABLE pending_fkeys FROM PUBLIC;
REVOKE ALL ON TABLE pending_fkeys FROM postgres;
GRANT ALL ON TABLE pending_fkeys TO postgres;
GRANT SELECT ON TABLE pending_fkeys TO PUBLIC;


--
-- Name: applied_execute; Type: ACL; Schema: londiste; Owner: postgres
--

REVOKE ALL ON TABLE applied_execute FROM PUBLIC;
REVOKE ALL ON TABLE applied_execute FROM postgres;
GRANT ALL ON TABLE applied_execute TO postgres;
GRANT SELECT ON TABLE applied_execute TO PUBLIC;


--
-- Name: seq_info; Type: ACL; Schema: londiste; Owner: postgres
--

REVOKE ALL ON TABLE seq_info FROM PUBLIC;
REVOKE ALL ON TABLE seq_info FROM postgres;
GRANT ALL ON TABLE seq_info TO postgres;
GRANT SELECT ON TABLE seq_info TO PUBLIC;


--
-- Name: table_info; Type: ACL; Schema: londiste; Owner: postgres
--

REVOKE ALL ON TABLE table_info FROM PUBLIC;
REVOKE ALL ON TABLE table_info FROM postgres;
GRANT ALL ON TABLE table_info TO postgres;
GRANT SELECT ON TABLE table_info TO PUBLIC;


SET search_path = pgq, pg_catalog;

--
-- Name: consumer; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON TABLE consumer FROM PUBLIC;
REVOKE ALL ON TABLE consumer FROM postgres;
GRANT ALL ON TABLE consumer TO postgres;
GRANT SELECT ON TABLE consumer TO PUBLIC;


--
-- Name: event_template; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON TABLE event_template FROM PUBLIC;
REVOKE ALL ON TABLE event_template FROM postgres;
GRANT ALL ON TABLE event_template TO postgres;
GRANT SELECT ON TABLE event_template TO PUBLIC;


--
-- Name: event_1; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON TABLE event_1 FROM PUBLIC;
GRANT SELECT ON TABLE event_1 TO PUBLIC;


--
-- Name: event_1_0; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON TABLE event_1_0 FROM PUBLIC;
GRANT SELECT ON TABLE event_1_0 TO PUBLIC;


--
-- Name: event_1_1; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON TABLE event_1_1 FROM PUBLIC;
GRANT SELECT ON TABLE event_1_1 TO PUBLIC;


--
-- Name: event_1_2; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON TABLE event_1_2 FROM PUBLIC;
GRANT SELECT ON TABLE event_1_2 TO PUBLIC;


--
-- Name: event_1_id_seq; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON SEQUENCE event_1_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE event_1_id_seq FROM postgres;
GRANT ALL ON SEQUENCE event_1_id_seq TO postgres;
GRANT SELECT ON SEQUENCE event_1_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE event_1_id_seq TO pgq_admin;


--
-- Name: event_1_tick_seq; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON SEQUENCE event_1_tick_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE event_1_tick_seq FROM postgres;
GRANT ALL ON SEQUENCE event_1_tick_seq TO postgres;
GRANT SELECT ON SEQUENCE event_1_tick_seq TO PUBLIC;


--
-- Name: queue; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON TABLE queue FROM PUBLIC;
REVOKE ALL ON TABLE queue FROM postgres;
GRANT ALL ON TABLE queue TO postgres;
GRANT SELECT ON TABLE queue TO PUBLIC;


--
-- Name: retry_queue; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON TABLE retry_queue FROM PUBLIC;
REVOKE ALL ON TABLE retry_queue FROM postgres;
GRANT ALL ON TABLE retry_queue TO postgres;
GRANT SELECT ON TABLE retry_queue TO PUBLIC;


--
-- Name: subscription; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON TABLE subscription FROM PUBLIC;
REVOKE ALL ON TABLE subscription FROM postgres;
GRANT ALL ON TABLE subscription TO postgres;
GRANT SELECT ON TABLE subscription TO PUBLIC;


--
-- Name: tick; Type: ACL; Schema: pgq; Owner: postgres
--

REVOKE ALL ON TABLE tick FROM PUBLIC;
REVOKE ALL ON TABLE tick FROM postgres;
GRANT ALL ON TABLE tick TO postgres;
GRANT SELECT ON TABLE tick TO PUBLIC;


--
-- PostgreSQL database dump complete
--

