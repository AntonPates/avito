__author__ = 'anton'
# coding: utf-8
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import threading
import urlparse
import cgi
import json
import psycopg2,sys
reload(sys)
sys.setdefaultencoding('UTF-8') #normal work with UTF-8

class Handler(BaseHTTPRequestHandler):

    def do_GET(self):
        parsed_path = urlparse.urlparse(self.path)
        message_parts = [
                'CLIENT VALUES:',
                'client_address=%s (%s)' % (self.client_address,
                                            self.address_string()),
                'command=%s' % self.command,
                'path=%s' % self.path,
                'real path=%s' % parsed_path.path,
                'query=%s' % parsed_path.query,
                'request_version=%s' % self.request_version,
                '',
                'SERVER VALUES:',
                'server_version=%s' % self.server_version,
                'sys_version=%s' % self.sys_version,
                'protocol_version=%s' % self.protocol_version,
                '',
                'HEADERS RECEIVED:',
                ]
        for name, value in sorted(self.headers.items()):
            message_parts.append('%s=%s' % (name, value.rstrip()))
        message_parts.append('')
        message = '\r\n'.join(message_parts)

        message2 =  threading.currentThread().getName()
        #self.wfile.write(message + '\r\n' +message2)
        if parsed_path.path == "/":
            self.send_response(200)
            self.end_headers()
            file = open('form.html', 'r')
            self.wfile.write(file.read())
        elif parsed_path.path == "/public/js/ui-bootstrap-tpls-0.13.4.js":
            self.send_response(200)
            self.end_headers()
            file = open('public/js/ui-bootstrap-tpls-0.13.4.js', 'r')
            self.wfile.write(file.read())
        elif parsed_path.path == '/public/js/avito.js':
            self.send_response(200)
            self.end_headers()
            file = open('public/js/avito.js', 'r')
            self.wfile.write(file.read())
        elif parsed_path.path == "/users_list":
            resp_json = ""
            try:
                conn = psycopg2.connect(dbname='avito_users', user='avito', host='localhost', password='avito')
                cur = conn.cursor()
                #cur.execute("SELECT oid,name,status FROM users u, users_addresses a;")
                cur.execute("SELECT u.oid AS id,u.name AS name, u.status AS status,COUNT(a.address) AS amount_of_addresses from users u left join users_addresses a on u.oid = a.user_oid GROUP BY u.oid,u.name,u.status;")
                if cur.rowcount > 0:
                    resp_json = "["
                    for rec in cur:
                        resp_json += '{"id":"%d","name":"%s","status":"%s","amount_of_addresses":"%d"},' % (rec[0],rec[1], rec[2],rec[3])
                    resp_json = resp_json[:-1] + "]"
                    self.send_response(200)

                else:
                    self.send_response(204)
                self.send_header("Content-Type","application/json")
                self.end_headers()
                conn.commit()
                cur.close()
                conn.close()
                self.wfile.write(resp_json)
            except:
                print "Unable to connect to the database ",sys.exc_info()[0]
        elif parsed_path.path == "/addresses_list":
            resp_json = ""
            try:
                user_id = int(urlparse.parse_qs(parsed_path.query)['uid'][0])
                conn = psycopg2.connect(dbname='avito_users', user='avito', host='localhost', password='avito')
                cur = conn.cursor()
                cur.execute("SELECT oid,address FROM users_addresses WHERE user_oid = %s;", (user_id,))
                if cur.rowcount > 0:
                    resp_json = "["
                    for rec in cur:
                        resp_json += '{"id":"%d","address":"%s"},' % (rec[0],rec[1])
                    resp_json = resp_json[:-1] + "]"
                    self.send_response(200)
                else:
                    self.send_response(204)
                    resp_json = "[]"
                self.send_header("Content-Type","application/json")
                self.end_headers()
                conn.commit()
                cur.close()
                conn.close()
                print resp_json
                self.wfile.write(resp_json)
            except:
                    print "Unable to connect to the database ",sys.exc_info()[0]
        return

    def do_POST(self):
        content_len = int(self.headers.getheader('content-length', 0))
        post_body = self.rfile.read(content_len)
        data = json.loads(post_body.decode())

        if self.path == "/new":
            try:
                conn = psycopg2.connect(dbname='avito_users', user='avito', host='localhost', password='avito')
                cur = conn.cursor()
                cur.execute("INSERT INTO users(name) VALUES(%s) RETURNING oid;", (data["name"],))
                if len(data["address"]) > 0:
                    oid_of_new_user = cur.fetchone()[0]
                    cur.execute("INSERT INTO users_addresses(user_oid,address) VALUES(%s,%s);", (oid_of_new_user,data["address"]))
                conn.commit()
                cur.close()
                conn.close()
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to connect to the database ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        elif self.path == "/update":
            try:
                print data["name"],data["status"],data["id"]
                conn = psycopg2.connect(dbname='avito_users', user='avito', host='localhost', password='avito')
                cur = conn.cursor()
                cur.execute("UPDATE users SET name = %s, status = %s WHERE oid = %s;", (data["name"], data["status"], int(data["id"])))
                conn.commit()
                cur.close()
                conn.close()
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to connect to the database ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()

        elif self.path == "/new_address":
            try:
                print "inside"
                conn = psycopg2.connect(dbname='avito_users', user='avito', host='localhost', password='avito')
                cur = conn.cursor()
                cur.execute("INSERT INTO users_addresses(user_oid,address) VALUES(%s,%s);", (int(data["user_id"]), data["address"]))
                conn.commit()
                cur.close()
                conn.close()
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to connect to the database ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        elif self.path == "/delete":
            print int(data["id"])
            try:
                conn = psycopg2.connect(dbname='avito_users', user='avito', host='localhost', password='avito')
                cur = conn.cursor()
                cur.execute("DELETE FROM users WHERE oid = %s;", (int(data["id"]),))
                #cur.execute("DELETE FROM users WHERE oid = 16427;")
                conn.commit()
                cur.close()
                conn.close()
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to connect to the database ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        elif self.path == "/delete_address":
            try:
                conn = psycopg2.connect(dbname='avito_users', user='avito', host='localhost', password='avito')
                cur = conn.cursor()
                cur.execute("DELETE FROM users_addresses WHERE oid = %s;", (int(data["id"]),))
                #cur.execute("DELETE FROM users WHERE oid = 16427;")
                conn.commit()
                cur.close()
                conn.close()
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to connect to the database ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        elif self.path == "/update_address":
            print data["address"], data["id"]
            try:
                conn = psycopg2.connect(dbname='avito_users', user='avito', host='localhost', password='avito')
                cur = conn.cursor()
                cur.execute("UPDATE users_addresses SET address = %s WHERE oid = %s;", (data["address"],int(data["id"])))
                #cur.execute("DELETE FROM users WHERE oid = 16427;")
                conn.commit()
                cur.close()
                conn.close()
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to connect to the database ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()

        # Begin the response

        self.wfile.write('Client: %s\n' % str(self.client_address))
        self.wfile.write('User-agent: %s\n' % str(self.headers['user-agent']))
        self.wfile.write('Path: %s\n' % self.path)
        self.wfile.write('Content type: %s\n' % self.headers['Content-Type'])
        self.wfile.write('Form data:\n')
        # Echo back information about what was posted in the form
        return

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""


if __name__ == '__main__':
    from BaseHTTPServer import HTTPServer
    server =  ThreadedHTTPServer(('localhost', 9632), Handler)
    print 'Starting server, use <Ctrl-C> to stop'
    server.serve_forever()

