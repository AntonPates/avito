__author__ = 'anton'
import pgq, sys, urlparse

class AddressCopyUnique(pgq.RemoteConsumer):
    def __init__(self, args):
        pgq.RemoteConsumer.__init__(self, "names_app", "src_db", "dst_db", args)
    def process_remote_batch(self, db, batch_id, event_list, dst_db):
        #table = self.cf.get('table_name')
        try:
            for ev in event_list:
                self.log.info('Type %s, %s!' % (ev.type,ev.extra1))
                self.log.info('Backup %s' % ev.extra2)
                if ev.type in ['I:id','U:id','D:id']:
                    cur = dst_db.cursor()
                    if ev.type == 'I:id' and ev.extra1 == 'public.addresses':
                        self.log.info('Data %s!' % ev.ev_data)
                        address = str(urlparse.parse_qs(ev.ev_data)['address'][0])
                        cur.execute("INSERT INTO addresses(address) values(%s);",(address,))
                    elif ev.type == 'U:id' and ev.extra1 == 'public.addresses':
                        old_address = str(urlparse.parse_qs(ev.ev_extra2)['address'][0])
                        address = str(urlparse.parse_qs(ev.ev_data)['address'][0])
                        self.log.info('Address %s' % address)
                        if old_address != address:
                            cur.execute("UPDATE addresses SET amount = amount - 1 WHERE address = %s;",(old_address,))
                            cur.execute("INSERT INTO addresses(address) values(%s);",(address,))
                    elif ev.type == 'D:id' and ev.extra1 == 'public.addresses':
                        address = str(urlparse.parse_qs(ev.ev_data)['address'][0])
                        cur.execute("UPDATE addresses SET amount = amount - 1 WHERE address = %s;",(address,))
                    self.log.info(cur.query)
                    dst_db.commit()
                    cur.close()
                ev.tag_done()
        except:
            print "Smth wrong ",sys.exc_info()[0]

if __name__ == '__main__':
    script = AddressCopyUnique(sys.argv[1:])
    script.start()