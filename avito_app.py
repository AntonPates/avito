__author__ = 'anton'
# coding: utf-8
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import urlparse
import json
import psycopg2,sys
reload(sys)
sys.setdefaultencoding('UTF-8') #normal work with UTF-8

class DB:
    __dsn_str = ""
    def __init__(self, dbname, user, host, password):
        self.__dsn_str = "dbname=%s user=%s host=%s password=%s" % (dbname,user,host,password)
    def Connect(self):
        conn = psycopg2.connect(self.__dsn_str)
        cur = conn.cursor()
        return (conn,cur)
    def Disconnect(self,conn,cur):
        conn.commit()
        cur.close()
        conn.close()
class UserModel:
    __db = DB(dbname="avito", user="avito", host="localhost", password="avito")
    def add(self,data):
         conn, cur = self.__db.Connect()
         cur.execute("INSERT INTO users(name) VALUES(%s) RETURNING id;", (data["name"],))
         if len(data["address"]) > 0:
            id_of_new_user = cur.fetchone()[0]
            cur.execute("INSERT INTO addresses(user_id,address) VALUES(%s,%s);", (id_of_new_user,data["address"]))
         self.__db.Disconnect(conn,cur)

    def update(self,data):
        conn, cur = self.__db.Connect()
        cur.execute("UPDATE users SET name = %s, status = %s WHERE id = %s;", (data["name"], data["status"], int(data["id"])))
        self.__db.Disconnect(conn,cur)
    def delete(self,id):
        conn, cur = self.__db.Connect()
        cur.execute("DELETE FROM users WHERE id = %s;", (id,))
        self.__db.Disconnect(conn,cur)
    def getList(self):
        resp_json = ""
        conn, cur = self.__db.Connect()
        cur.execute("SELECT u.id AS id,u.name AS name, u.status AS status,COUNT(a.address) AS amount_of_addresses from users u left join addresses a on u.id = a.user_id GROUP BY u.id,u.name,u.status;")
        if cur.rowcount > 0:
            resp_json = "["
            for rec in cur:
                resp_json += '{"id":"%d","name":"%s","status":"%s","amount_of_addresses":"%d"},' % (rec[0],rec[1], rec[2],rec[3])
            resp_json = resp_json[:-1] + "]"
        self.__db.Disconnect(conn,cur)
        return resp_json


class AddressModel:
    __db = DB(dbname="avito", user="avito", host="localhost", password="avito")
    __dbua = DB(dbname="avito_ua", user="avito", host="localhost", password="avito")
    def add(self,data):
        conn, cur = self.__db.Connect()
        cur.execute("INSERT INTO addresses(user_id,address) VALUES(%s,%s);", (int(data["user_id"]), data["address"]))
        self.__db.Disconnect(conn,cur)
    def update(self,data):
        conn, cur = self.__db.Connect()
        cur.execute("UPDATE addresses SET address = %s WHERE id = %s;", (data["address"],int(data["id"])))
        self.__db.Disconnect(conn,cur)
    def delete(self,id):
        conn, cur = self.__db.Connect()
        cur.execute("DELETE FROM addresses WHERE id = %s;", (id,))
        self.__db.Disconnect(conn,cur)
    def getGetUsersList(self,user_id):
        resp_json = ""
        conn, cur = self.__db.Connect()
        cur.execute("SELECT id,address FROM addresses WHERE user_id = %s;", (user_id,))
        if cur.rowcount > 0:
            resp_json = "["
            for rec in cur:
                resp_json += '{"id":"%d","address":"%s"},' % (rec[0],rec[1])
            resp_json = resp_json[:-1] + "]"
        self.__db.Disconnect(conn,cur)
        return resp_json
    def getUniqueAddressesList(self):
        resp_json = ""
        conn, cur = self.__dbua.Connect()
        cur.execute("SELECT id,address,amount FROM addresses WHERE amount > 0;")
        if cur.rowcount > 0:
            resp_json = "["
            for rec in cur:
                resp_json += '{"id":"%d","address":"%s","amount":"%s"},' % (rec[0],rec[1],rec[2])
            resp_json = resp_json[:-1] + "]"
        self.__dbua.Disconnect(conn,cur)
        return resp_json


class Handler(BaseHTTPRequestHandler):
    dsn_str = "dbname=avito user=avito host=localhost password=avito"
    user_model = UserModel()
    address_model = AddressModel()
    def do_GET(self):
        #dsn_str = "dbname=avito user=avito host=localhost password=avito"
        parsed_path = urlparse.urlparse(self.path)
        if parsed_path.path == "/":
            self.send_response(200)
            self.end_headers()
            file = open('form.html', 'r')
            self.wfile.write(file.read())
        elif parsed_path.path == "/public/js/ui-bootstrap-tpls-0.13.4.js":
            self.send_response(200)
            self.end_headers()
            file = open('public/js/ui-bootstrap-tpls-0.13.4.js', 'r')
            self.wfile.write(file.read())
        elif parsed_path.path == '/public/js/avito.js':
            self.send_response(200)
            self.end_headers()
            file = open('public/js/avito.js', 'r')
            self.wfile.write(file.read())
        #Users list with amount of addresses for each user
        elif parsed_path.path == "/users_list":
            try:
                resp_json = self.user_model.getList()
                if len(resp_json) > 2:
                    self.send_response(200)
                else:
                    self.send_response(204)
                    resp_json = "{}"
                self.send_header("Content-Type","application/json")
                self.end_headers()
                self.wfile.write(resp_json)
            except:
                print "Unable to get Users List ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        #User's (uid) addresses list
        elif parsed_path.path == "/addresses_list":
            resp_json = ""
            try:
                user_id = int(urlparse.parse_qs(parsed_path.query)['uid'][0])
                resp_json = self.address_model.getGetUsersList(user_id)
                if len(resp_json) > 2:
                    self.send_response(200)
                else:
                    self.send_response(204)
                    resp_json = "{}"
                self.send_header("Content-Type","application/json")
                self.end_headers()
                self.wfile.write(resp_json)
            except:
                print "Unable to get addresses list ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        #Unique addresses list
        elif parsed_path.path == "/unique_addresses_list":
            resp_json = ""
            try:
                resp_json = self.address_model.getUniqueAddressesList()
                if len(resp_json) > 2:
                    self.send_response(200)
                else:
                    self.send_response(204)
                    resp_json = "{}"
                self.send_header("Content-Type","application/json")
                self.end_headers()
                self.wfile.write(resp_json)
            except:
                print "Unable to get Unique Addresses List ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        else:
            self.send_response(404)
            self.end_headers()
            self.wfile.write("404 - The page does not exists.")
        return

    def do_POST(self):
        content_len = int(self.headers.getheader('content-length', 0))
        post_body = self.rfile.read(content_len)
        data = json.loads(post_body.decode())
        #New user and, probably, address
        if self.path == "/new":
            try:
                self.user_model.add(data)
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to create new user record ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        #Update user record
        elif self.path == "/update":
            try:
                self.user_model.update(data)
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to update user record ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        #New user's address
        elif self.path == "/new_address":
            try:
                self.address_model.add(data)
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to create new address record ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        #Remove user record
        elif self.path == "/delete":
            print int(data["id"])
            try:
                self.user_model.delete(int(data["id"]))
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to delete User record ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        #Remove address record
        elif self.path == "/delete_address":
            try:
                self.address_model.delete(int(data["id"]))
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to delete address record ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        #Update address record
        elif self.path == "/update_address":
            print data["address"], data["id"]
            try:
                self.address_model.update(data)
                self.send_response(200)
                self.end_headers()
            except:
                print "Unable to update address record ",sys.exc_info()[0]
                self.send_response(500)
                self.end_headers()
        else:
            self.send_response(404)
            self.end_headers()
            self.wfile.write("404 - The page does not exists.")
        return

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""


if __name__ == '__main__':
    from BaseHTTPServer import HTTPServer
    server =  ThreadedHTTPServer(('', 9632), Handler)
    print 'Starting server, use <Ctrl-C> to stop'
    server.serve_forever()

