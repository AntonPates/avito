/**
 * Created by anton on 9/27/15.
 */
var avitoApp = angular.module('avitoApp',['ui.bootstrap']);
avitoApp.controller('avitoCtrl',['$scope','$http',function($scope,$http){
    $scope.name = "";
    $scope.address = "";
    $scope.users = new Array();
    $scope.addresses = new Array();
    $scope.cur_name = "";
    $scope.cur_id = 0;
    $scope.cur_status = '2';
    $scope.button_value = "Add";
    $scope.cur_mode = "Add";
    $scope.cur_address_id = 0;
    $scope.isAlertInactive = true;
    $scope.isAlertActive = false;
    $scope.totalUsers = 0;
    $scope.totalAddresses = 0;
    $scope.curUserPage = 1;
    $scope.curAddressPage = 1;
    $scope.maxSize = 7;
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
        $scope.isAlertActive = false;
        $scope.isAlertInactive = true;
        $scope.alerts.length = 0;
    };
    $scope.addAlert = function(type,message) {
        $scope.alerts.push({type: type, msg: message});
        $scope.isAlertActive = true;
        $scope.isAlertInactive = false;
    };
    $scope.sendData = function (){
        console.log("Sending data...");
        if ($scope.name.length > 0) {
            var ref = "";
            var dts = "";
            var msg = "";
            if ($scope.cur_mode == "Add") {
                ref = '/new';
                dts = '{' + '"name":"' + $scope.name.trim() + '","address":"' + $scope.address.trim() + '"}';
                msg = 'Пользователь успешно добавлен.';
            } else {
                ref = '/update';
                dts =  '{' + '"id":"' + $scope.cur_id + '","name":"' + $scope.name.trim() + '","status":"' + $scope.cur_status +'"}';
                msg = 'Данные пользователя успешно обновлены.';
            }
            $http({
                method: 'POST', data: dts
                , url: ref
            }).
                success(function (data, status, headers, config) {
                    if (status != 200) {
                        console.log("Smth wrong");
                        $scope.addAlert('danger', 'OOps, smth wrong');
                    } else {
                        $scope.address = "";
                        $scope.name = "";
                        $scope.cur_mode = "Add";
                        $scope.button_value = "Add";
                        $scope.getUsersList();
                        $scope.addAlert('success', msg);
                        $scope.getUniqueAdressesList();
                    }
                }).
                error(function (data, status, headers, config) {
                    console.log("Error callback " + status);
            });
        } else if ($scope.address.length > 0) {
            var ref = "";
            var dts = "";
            if ($scope.cur_mode == "Add" && $scope.cur_id > 0) {
                ref = '/new_address';
                dts =  '{' + '"user_id":"' + $scope.cur_id + '","address":"' + $scope.address.trim() + '"}';
                msg = 'Адрес успешно добавлен.';
            } else if ($scope.cur_id > 0) {
                ref = '/update_address';
                dts =  '{' + '"id":"' + $scope.cur_address_id + '","address":"' + $scope.address.trim() + '"}';
                msg = 'Адрес успешно обновлен.';
            } else {
                $scope.addAlert('danger', "Нельзя добавить адрес без привязки к пользователю");
                return;
            }
            $http({
                method: 'POST', data: dts, url: ref
            }).
                success(function (data, status, headers, config) {
                    if (status != 200) {
                        console.log("Smth wrong");
                        $scope.addAlert('danger', 'OOps, smth wrong');
                    } else {
                        $scope.address = "";
                        $scope.name = "";
                        $scope.cur_mode = "Add";
                        $scope.button_value = "Add";
                        $scope.getUsersList();
                        $scope.getAddressesList($scope.cur_id,$scope.cur_name,$scope.cur_status);
                        $scope.addAlert('success', msg);
                    }

                }).
                error(function (data, status, headers, config) {
                    console.log("Error callback " + status);
            });
        }
    };
    $scope.getUsersList = function(){
        $http({method: 'GET', url: '/users_list'}).
            success(function(data,status,headers,config) {
                if (status == 200){
                    $scope.users.length = 0;
                    data = eval(data);
                    var i = 0;
                    for (i=0;i<data.length;i++){
                        $scope.users.push({id:data[i].id, name:data[i].name, status:data[i].status,addresses:parseInt(data[i].amount_of_addresses)});
                    }
                    $scope.totalUsers = $scope.users.length;
                } else {
                     $scope.addAlert('danger','Unable to load users list - there is no one in the DB');
                }
            }).
            error(function(data,status,headers,config){
                console.log("Error callback: " + status);
                $scope.addAlert('danger','OOps, smth wrong. Unable to get Users List.');
            })
    };
    $scope.getAddressesList = function(user_id,name,user_status){
        $http({method: 'GET', url: '/addresses_list?uid='+user_id}).
            success(function(data,status,headers,config) {
                $scope.cur_name = name;
                $scope.cur_id = user_id;
                $scope.cur_status = user_status;
                if (status != 200) {
                    if (status == 204) {
                        $scope.addresses.length = 0;
                        console.log("There is no address for " + name);
                        $scope.addAlert('danger', "There is no address for " + name);
                    } else {
                        console.log("Smth wrong");
                        $scope.addAlert('danger','OOps, smth wrong');
                    }

                } else {
                    $scope.addresses.length = 0;
                    console.log(data);
                    data = eval(data);
                    var i = 0;
                    for (i=0;i<data.length;i++) {
                        $scope.addresses.push({
                            id: data[i].id,
                            val: data[i].address
                        });
                    }
                    $scope.totalAddresses = $scope.addresses.length;
                }
            }).
            error(function(data,status,headers,config){
                console.log("Error callback " + status);
                $scope.addAlert('danger','OOps, smth wrong. Unable to get Addresses List.');
        });
    };

     $scope.getUniqueAdressesList = function(){
        $http({method: 'GET', url: '/unique_addresses_list'}).
            success(function(data,status,headers,config) {
                $scope.button_value = "Add";
                $scope.address = "";
                $scope.name = "";
                $scope.cur_id = 0;
                $scope.cur_address_id = 0;
                $scope.cur_mode = "Add";
                $scope.cur_name = "из \"уникальной БД\"";
                if (status != 200) {
                    console.log("Status " + status);
                    if (status == 204) {
                        $scope.addresses.length = 0;
                        $scope.addAlert('danger', "There are no addresses in Unuque DB ");
                    } else {
                        console.log("Smth wrong");
                        $scope.addAlert('danger','OOps, smth wrong');
                    }

                } else {
                    $scope.addresses.length = 0;
                    //console.log(data);
                    data = eval(data);
                    var i = 0;
                    for (i=0;i<data.length;i++) {
                        $scope.addresses.push({
                            id: data[i].id,
                            val: data[i].address
                        });
                    }
                    $scope.totalAddresses = $scope.addresses.length;
                }
            }).
            error(function(data,status,headers,config){
                console.log("Error callback " + status);
                $scope.addAlert('danger','OOps, smth wrong. Unable to get Unique Addresses List.');
        });
    };

    $scope.deleteUser = function(user_id){
        $http({method: 'POST', data: '{'
            +'"id":"' + user_id + '"}'
            ,url: '/delete'}).
            success(function(data,status,headers,config) {
                if (status != 200) {
                    console.log("Smth wrong");
                    $scope.addAlert('danger','OOps, smth wrong');
                } else {
                    $scope.getUsersList();
                    $scope.getUniqueAdressesList();
                    $scope.addAlert('success','Пользователь успешно удален.');
                }
            }).
            error(function(data,status,headers,config){
                console.log("Error callback " + status + ". Unable to delete user");
        });
    };
    $scope.editAddress = function(id,address){
        $scope.button_value = "Update";
        $scope.address = address;
        $scope.name = "";
        $scope.cur_address_id = parseInt(id);
        $scope.cur_mode = "Edit";


    };
    $scope.editUser = function(id,name,status){
        $scope.button_value = "Update";
        $scope.name = name;
        $scope.address = "";
        $scope.cur_status = status;
        $scope.cur_id = parseInt(id);
        $scope.cur_mode = "Edit_user";


    };
     $scope.cancelEdit = function(){
        $scope.button_value = "Add";
        $scope.address = "";
        $scope.name = "";
        $scope.cur_address_id = 0;
        $scope.cur_mode = "Add";


    };
    $scope.start = function() {
        $scope.getUsersList();
        $scope.getUniqueAdressesList();
    };
    $scope.deleteAddress = function(address_id){
        $http({method: 'POST', data: '{'
            +'"id":"' + address_id + '"}'
            ,url: '/delete_address'}).
            success(function(data,status,headers,config) {
                if (status != 200) {
                    console.log("Smth wrong");
                    $scope.addAlert('danger','OOps, smth wrong');
                } else {
                    $scope.getUsersList();
                    $scope.getAddressesList($scope.cur_id,$scope.cur_name,$scope.cur_status);
                    $scope.addAlert('success','Адрес успешно удален.');
                }
            }).
            error(function(data,status,headers,config){
                 console.log("Error callback " + status + ". Unable to delete address");
        });
    };
    $scope.$on('$viewContentLoaded',$scope.start());
    }]);
avitoApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});